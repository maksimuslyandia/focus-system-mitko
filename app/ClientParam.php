<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientParam extends Model
{
    protected $table = 'client_params';

    public function subClientParams()
    {
        return $this->hasMany('App\SubClientParam', 'client_param_id', 'id');
    }
}
