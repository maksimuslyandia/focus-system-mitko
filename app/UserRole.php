<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $primaryKey = "id";

    protected $table = 'user_roles';

    public function getUsers()
    {
        return $this->belongsToMany('App\User', 'user_role_rules', 'user_role_id', 'user_id');
    }
}
