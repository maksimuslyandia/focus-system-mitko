<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientSearchRule extends Model
{
    protected $table = 'client_search_rules';

    public function getClientSearchParamRule()
    {
        return $this->hasMany('App\ClientSearchParamRule','id','sfera_na_deynosti');

    }
}

