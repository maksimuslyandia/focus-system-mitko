<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallState extends Model
{
    protected $table = 'call_states';
}
