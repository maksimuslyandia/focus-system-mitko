<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Apartment;

class TestController extends Controller
{
    public function convert()
    {
        $Apartments = Apartment::all();
        foreach ($Apartments as $apartment) {
//            if ($apartment->id = 13926) {
            if ($apartment->id = 13926) {
                $newApartment = Apartment::find($apartment->id);
//            $newApartment = Apartment::find(1007);
//            $newApartment->otoplenie = (isset($apartment->params[0])) ? intval($apartment->params[0]->parameter) : null;
                if (isset($apartment->params[0]) && !is_null($apartment->params[0])) {
                    $newApartment->otoplenie = $apartment->params[0]->parameter;
                }
                if (isset($apartment->params[1]) && !is_null($apartment->params[1])) {
                    $newApartment->region = $apartment->params[1]->parameter;
                }
                if (isset($apartment->params[2])) {
                    switch ($apartment->params[2]) {
                        case ('Изток'):
                            $newApartment->izlogenie_iztok = 1;
                            break;
                        case ('Запад '):
                            $newApartment->izlogenie_zapad = 1;
                            break;
                        case ('Север'):
                            $newApartment->izlogenie_sever = 1;
                            break;
                        case ('Юг'):
                            $newApartment->izlogenie_ug = 1;
                            break;
                    }
                }
                if (isset($apartment->params[3]) && !is_null($apartment->params[3])) {
                    $newApartment->vid_stroitelstvo = $apartment->params[3]->parameter;
                }
                if (isset($apartment->params[4]) && !is_null($apartment->params[4])) {
                    $newApartment->obzavegdane = $apartment->params[4]->parameter;
                }
                if (isset($apartment->params[5]) && !is_null($apartment->params[5])) {
                    $newApartment->vid_imot = $apartment->params[5]->parameter;
                }
                if (isset($apartment->params[6])) {
                    switch (isset($apartment->params[6])) {
                        case 'Няма':
                            $newApartment->prehod = 2;
                            break;
                        case 'Има':
                            $newApartment->prehod = 1;

                            break;
                        case 'Не се знае':
                            $newApartment->prehod = 3;
                            break;
                    }
                }
                if (isset($apartment->params[7])) {
                    switch (isset($apartment->params[7])) {
                        case 'Да':
                            $newApartment->izolatsia = 1;
                            break;
                        case 'Не':
                            $newApartment->izolatsia = 2;
                            break;
                    }
                }
                if (isset($apartment->params[7])) {
                    switch (isset($apartment->params[7])) {
                        case 'Има':
                            $newApartment->elevator = 1;
                            break;
                        case 'Няма':
                            $newApartment->elevator = 2;
                            break;
                    }
                }
                if (isset($apartment->params[9])) {
                    switch (isset($apartment->params[9])) {
                        case 'Да':
                            $newApartment->komisionna = 1;
                            break;
                        case 'Не':
                            $newApartment->komisionna = 2;
                            break;
                    }
                }
                if (isset($apartment->params[10]) && !is_null($apartment->params[10])) {
                    $newApartment->etag = intval($apartment->params[10]->parameter);
                }
                if (isset($apartment->params[11]) && !is_null($apartment->params[11])) {
                    $newApartment->obsho_etaga = intval($apartment->params[11]->parameter);
                }
                if (isset($apartment->params[12]) && !is_null($apartment->params[12])) {
                    $newApartment->komentar_za_etaga = $apartment->params[12]->parameter;
                }
                if (isset($apartment->params[13]) && !is_null($apartment->params[13])) {
                    $newApartment->orientir_za_address = $apartment->params[13]->parameter;
                }
                if (isset($apartment->params[14]) && !is_null($apartment->params[14])) {
                    $newApartment->opisanie_na_obzavegdane = $apartment->params[14]->parameter;
                }
                if (isset($apartment->params[15]) && !is_null($apartment->params[15])) {
                    $newApartment->opisanie_za_nastilki = $apartment->params[15]->parameter;
                }
                if (isset($apartment->params[16]) && !is_null($apartment->params[16])) {
                    $newApartment->komesionna_zabelegka = $apartment->params[16]->parameter;
                }
                if (isset($apartment->params[17]) && !is_null($apartment->params[17])) {
                    $newApartment->kvadratura = intval($apartment->params[17]->parameter);
                }
                if (isset($apartment->params[18]) && !is_null($apartment->params[18])) {
                    $newApartment->cena_eur = intval($apartment->params[18]->parameter);
                }
                if (isset($apartment->params[19]) && !is_null($apartment->params[19])) {
                    $newApartment->brokerska_belegka = $apartment->params[19]->parameter;
                }
                if (isset($apartment->params[20]) && !is_null($apartment->params[20])) {
                    $newApartment->razpredelenie = $apartment->params[20]->parameter;
                }
                $newApartment->save();
//            dd($newApartment);

            }
        }
    }

}
