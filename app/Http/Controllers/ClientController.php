<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientParam;
use App\ClientSearchParamRule;
use App\ClientSearchRule;
use App\RequestState;
use App\SubClientParam;
use App\User;
use App\Status;
use App\ClientRole;
use App\CallState;
use App\UserRole;
use App\UserClientRule;
use App\Email;
use App\Phone;
use App\ClientPhoneRule;
use App\ClientEmailRule;
use App\ClientRoleRule;
use App\ApartmentsRepresent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Promise\all;

class ClientController extends Controller
{


    public function show(Request $request, $id)
    {
        $value = $request->session()->get('key');
dd($value);
        //
    }

    public function getClients(Request $request, $status)
    {
        $Client = new Client();
        $CallStates = CallState::all();
        $clientsInfo = $Client
            ->with('statuses')
            ->with('clientRoles') //ролите на даден апартементы
            ->with('phones') //ролите на даден апартементы
            ->with('emails') //ролите на даден апартементы
            ->with('users') //ролите на даден апартементы
            ->with('calls');
            // dd($status);
        switch ($status) {
            case 'active':
                $statusId = 1;
                $clientsInfo->Where('status_id', '=', $statusId);
                break;
            case 'passive':
                $statusId = 2;
                $clientsInfo->where('status_id', '=', $statusId);
                break;
        }
        $naematel_id = 'наемател';
        $kupuvach_id = 'купувач';
        $clientsInfo->whereHas('clientRoles', function ($query) use ($naematel_id, $kupuvach_id) {
            $query->where('role_name', '=', $naematel_id);
        })
            ->orWhere('id', '=', $kupuvach_id);
        $clientsInfo = $clientsInfo
            ->orderByDesc('created_at')
            ->paginate(30);
        return view('clients.clients',
            [
                'clientsInfo' => $clientsInfo,
                'callStates' => $CallStates]
        );
    }

    public function getClient(Request $request, $clientId)
    {
        $Client = new Client();
        $clientInfo = $Client
            ->with('statuses')
            ->with('users')
            ->with('clientRoles') //ролите на даден апартементы
            ->with('phones') //ролите на даден апартементы
            ->with('calls') //ролите на даден апартементы
            ->with('emails') //ролите на даден апартементы
            ->with('apartments') //ролите на даден апартементы
            ->with('getclientSearchRules')
            ->with('likeApartments')
            ->find($clientId);

        $ClientParam = new ClientParam();
        $clientParams = $ClientParam
            ->with('subClientParams')
            ->get();

        $viewToReturn = 'clients.clientInfo';//если в ссылке не содержиться update
        if (strpos($request->url(), "update") !== false) {
            $params = [];

            foreach ($clientParams as $key => $label) {
                $params [$key]['param_name'] = $label->param_name;
                if ($clientInfo->getclientSearchRules == null) {
//                создать запись с пустыми полями в таблице поиска
                    $ClientSearchRule = new ClientSearchRule();
                    $ClientSearchRule->client_id = $clientId;
                    $ClientSearchRule->save();
                    $clientInfo = $Client
                        ->with('statuses')
                        ->with('users')
                        ->with('clientRoles') //ролите на даден апартементы
                        ->with('phones') //ролите на даден апартементы
                        ->with('calls') //ролите на даден апартементы
                        ->with('emails') //ролите на даден апартементы
                        ->with('apartments') //ролите на даден апартементы
                        ->with('getclientSearchRules')
                        ->find($clientId);
                }
                $params [$key]['label'] = $clientInfo->getclientSearchRules->toArray()[$label->column_name];
                foreach ($label->subClientParams as $k => $subClientParam) { //так как может быть не наеамтель
                    $params [$key]['selcted'] = $clientInfo->getclientSearchRules->toArray()[$label->column_name];
                    if ($clientInfo->getclientSearchRules != null) {// проверка если не наематель
                        if ($clientInfo->getclientSearchRules->toArray()[$label->column_name] == $subClientParam->sub_param_name) {
                            $params [$key]['selcted'] = $clientInfo->getclientSearchRules->toArray()[$label->column_name];
                        }
                    }
                    $params [$key]['sub_param_name'][$k] = $subClientParam->sub_param_name;
                }
                $params [$key]['param_type'] = $label->param_type;
                $params [$key]['column_name'] = $label->column_name;
            }
            $viewToReturn = 'clients.clientRedaction';
        }

        return view($viewToReturn,
            [
                'clientParams' => $clientParams,
                'params' => (isset($params)) ? $params : '',
                'clientInfo' => $clientInfo,
                'statuses' => Status::all(),
                'callStates' => CallState::all(),
                'brokers' => User::all()
            ]
        );
    }

    public
    function getUserClients()
    {
        $ApartmentsRepresent = new ApartmentsRepresent();
        $representInfo = $ApartmentsRepresent
                            ->with("client")
                            ->with("apartment")
                            ->where('represent_state_id', '=', 4)
                            ->where('user_id', "=", Auth::id())
                            ->get();
        
        $User = new User();
        
        $clientsInfo = $User
            ->with('activeClients');
        $clientsInfo = $clientsInfo
            ->find(Auth::id());
                   
        return view('clients.myClients',
            [
                'callStates' => CallState::all(),
                'myClientsInfo' => $clientsInfo,
                "representInfo"=> $representInfo
            ]
        );
    }

    public
    function saveClient(Request $request)
    {
        $request->all();
        $clientObj = new Client();
        $client = $clientObj
                        ->with('usersClients')
                        ->find($request->client_id);
        if($request->user_id !== "false") {
            if (count($client->usersClients) != 0) {
                $client->usersClients[0]->user_id = $request->user_id;
            } else {
                $userClientRules = new UserClientRule();
                $userClientRules->user_id = $request->user_id;
                $userClientRules->client_id = $request->client_id;
                $userClientRules->save();
            }
        }
        $client->first_name = $request->first_name;
        $client->last_name = $request->last_name;
        $client->status_id = $request->client_status;
        $client->push();
        // dd($request);
        $ClientPhoneRule = new ClientPhoneRule(); //вытащил все свяхи телефон - клиент
        $rules = $ClientPhoneRule->where('client_id', '=', $request->client_id)->get();

        $ruleIds = collect([]);
        $phoneIds = collect([]);
        foreach ($rules as $rule) {
            $ruleIds->push($rule->id);
            $phoneIds->push($rule->phone_id);
        }

        $deletingPhonesRules = $ClientPhoneRule->whereIn('id', $ruleIds);
        $deletingPhonesRules->delete();
        $Phone = new Phone();
        $Phone = $Phone->whereIn('id', $phoneIds);
        $Phone->delete();

        foreach ($request->phone as $phone) {
            if ($phone != null) {
                $Phone = new Phone();
                $Phone->phone = $phone;
                $Phone->save();

                $ClientPhoneRule = new ClientPhoneRule();
                $ClientPhoneRule->client_id = $request->client_id;
                $ClientPhoneRule->phone_id = $Phone->id;
                $ClientPhoneRule->push();
            }
        }

        // $newClientRoleRule = ClientRoleRule::where('client_id', $request->client_id)->get();
        // $newClientRoleRule[0]->client_id = $request->client_id;
        // $newClientRoleRule[0]->client_role_id = $request->client_roles;
        // $newClientRoleRule[0]->save();

//        $ClientSearchRule = new ClientSearchRule();
//        $ClientSearchRule->where('client_id', '=', $client->id);
//        $Client = new Client();
//        $cl = $Client
//            ->with('phones')
//            ->find($request->client_id);
////        $client = $Client->find(1001)->get();
////        $cl = $client->with('phones')->get(); //ролите на даден апартементы
//        dd($cl);

        $ClientSearchRules = ClientSearchRule::firstWhere('client_id', $request->client_id);;
//        $ClientSearchRules = $ClientSearchRules->
//        $ClientSearchRules->client_id =$request->client_id;
        $ClientSearchRules->informacia = $request->informacia;
        $ClientSearchRules->opisanie_na_tirseneto = $request->opisanie_na_tirseneto;
        $ClientSearchRules->kolko_vreme_tirsi = $request->kolko_vreme_tirsi;
        $ClientSearchRules->ogleda_mina = $request->ogleda_mina;
        $ClientSearchRules->kakvo_ima = $request->kakvo_ima;
        $ClientSearchRules->price = $request->price;
        $ClientSearchRules->apartment_id = $request->apartment_id;
        $ClientSearchRules->prichina_za_napuskane = $request->prichina_za_napuskane;
        $ClientSearchRules->sfera_na_deynosti = $request->sfera_na_deynosti;
        $ClientSearchRules->udobno_vreme_za_ogled = $request->udobno_vreme_za_ogled;
        $ClientSearchRules->imot_site = $request->imot_site;
        $ClientSearchRules->region = $request->region;
        $ClientSearchRules->obzavegdane = $request->obzavegdane;
        $ClientSearchRules->vid_imot = $request->vid_imot;
        $ClientSearchRules->raboti_s_drugi_agencii = $request->raboti_s_drugi_agencii;
        $ClientSearchRules->pushach = $request->pushach;
        $ClientSearchRules->moge_da_predplati = $request->moge_da_predplati;
        $ClientSearchRules->home_pet = $request->home_pet;
        $ClientSearchRules->siglasen_uslovia = $request->siglasen_uslovia;
        $ClientSearchRules->vzaimootnoshenia = $request->vzaimootnoshenia;
        $ClientSearchRules->push();

        $Сlient = new Client();
        $client = $Сlient->with('getclientSearchRules')->with('phones');
        $client = $client->find($request->client_id);
        return redirect()->action(
            'ClientController@getClients', ['status' => $request->client_status] 
        );
    }

    public
    function createClient()
    {
        $ClientParam = new ClientParam();
        $clientParams = $ClientParam
            ->with('subClientParams')
            ->get();

        $Status = Status::all();
        $ClientRoles = ClientRole::all();
        $UserRole = new UserRole ();
        $brokers = $UserRole->with('getUsers')
            ->where('role_name', '=', 'broker')
            ->get();

        return view('clients.clientCreation',
            [
                'statuses' => $Status,
                'clientParams' => $clientParams,
                'clientRoles' => $ClientRoles,
                "brokers" => $brokers[0]->getUsers
            ]
        );
    }

    public
    function saveNewClient(Request $request)
    {
//        dd($request->all());
        $Client = new Client();
        $Client->status_id = $request->client_status;
        $Client->first_name = $request->first_name;
        $Client->last_name = $request->last_name;
        $Client->save();

        $ClientSearchRules = new ClientSearchRule();
        $ClientSearchRules->client_id = $Client->id;
        $ClientSearchRules->informacia = $request->informacia;
        $ClientSearchRules->opisanie_na_tirseneto = $request->opisanie_na_tirseneto;
        $ClientSearchRules->kolko_vreme_tirsi = $request->kolko_vreme_tirsi;
        $ClientSearchRules->ogleda_mina = $request->ogleda_mina;
        $ClientSearchRules->kakvo_ima = $request->kakvo_ima;
        $ClientSearchRules->price = $request->price;
        $ClientSearchRules->apartment_id = $request->apartment_id;
        $ClientSearchRules->prichina_za_napuskane = $request->prichina_za_napuskane;
        $ClientSearchRules->sfera_na_deynosti = $request->sfera_na_deynosti;
        $ClientSearchRules->udobno_vreme_za_ogled = $request->udobno_vreme_za_ogled;
        $ClientSearchRules->imot_site = $request->imot_site;
        $ClientSearchRules->region = $request->region;
        $ClientSearchRules->obzavegdane = $request->obzavegdane;
        $ClientSearchRules->vid_imot = $request->vid_imot;
        $ClientSearchRules->raboti_s_drugi_agencii = $request->raboti_s_drugi_agencii;
        $ClientSearchRules->pushach = $request->pushach;
        $ClientSearchRules->moge_da_predplati = $request->moge_da_predplati;
        $ClientSearchRules->home_pet = $request->home_pet;
        $ClientSearchRules->siglasen_uslovia = $request->siglasen_uslovia;
        $ClientSearchRules->vzaimootnoshenia = $request->vzaimootnoshenia;
        $ClientSearchRules->save();


        //sub параметры клиента (multiselect)

        $newEmail = new Email(); // ** todo Проверка на пустоту
        $newEmail->email = $request->email;
        $newEmail->save();


        $newClientPhoneRule = new ClientEmailRule();
        $newClientPhoneRule->client_id = $Client->id;
        $newClientPhoneRule->email_id = $newEmail->id;
        $newClientPhoneRule->save();

        //телефоны клиента найти существующий
        $newPhone = new Phone();// ** todo Проверка на массив
        $newPhone->phone = $request->phone;
        $newPhone->save();
        $newClientPhoneRule = new ClientPhoneRule();
        $newClientPhoneRule->client_id = $Client->id;
        $newClientPhoneRule->phone_id = $newPhone->id;
        $newClientPhoneRule->save();

        //email инфо клиента
        $newClientEmailRule = new ClientEmailRule();
        $newClientEmailRule->client_id = $Client->id;
        $newClientEmailRule->email_id = $newEmail->id;
        $newClientEmailRule->save();

        //определили наемател, купувач
        $newClientRoleRule = new ClientRoleRule();
        $newClientRoleRule->client_id = $Client->id;
        $newClientRoleRule->client_role_id = $request->client_roles;
        $newClientRoleRule->save();

        //присвоили брокеру клиента
        $newUserClientRule = new UserClientRule();
        $newUserClientRule->client_id = $Client->id;
        $newUserClientRule->user_id = $request->broker_id;
        $newUserClientRule->save();
        $clientStatus = "active";
        // dd($request->client_status);
        if($request->client_status === "2"){
            $clientStatus = "passive";
        }
        return redirect()->action(
            'ClientController@getClients', ['status' => $clientStatus] 
        );
    }
}


