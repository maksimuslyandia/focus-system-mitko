<?php

namespace App\Http\Controllers;

use App\ApartmentsRepresent;
use App\RepresentState;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RepresentController extends Controller
{
    public function updateOgled(Request $request)
    {
        $date = strtotime("$request->ogledDate $request->ogledTime");
        $ApartmentsRepresent = new ApartmentsRepresent();
        $ApartmentsRepresent->apartment_id = $request->apartment_id;
        $ApartmentsRepresent->client_id = $request->client_id;
        $ApartmentsRepresent->user_id = Auth::user()->id;
        $ApartmentsRepresent->datetime = date('Y-m-d H:i:s', $date);
        $ApartmentsRepresent->save();
    }
    
    public function createNewOgeld(Request $request)
    {
        // dd($request);
        $i = 0;
        foreach($request->apartments as $key => $apartment){
            // dd( $key);
            $ApartmentsRepresent = new ApartmentsRepresent();
            $ApartmentsRepresent->apartment_id = $apartment['apartmentID'];
            $ApartmentsRepresent->client_id = $apartment['clientID'];
            $ApartmentsRepresent->user_id = Auth::user()->id;
            $ApartmentsRepresent->date = $request->dates[$i];
            $ApartmentsRepresent->time = $request->times[$i];
            $ApartmentsRepresent->represent_state_id = 4;
            $ApartmentsRepresent->save();
            $i++;
        }
        
        return redirect()->action(
            'ClientController@getUserClients'
        );    
    }

    public function loadModal(Request $request)
    {

        return view('represents.representModal',
            [
                'representStates' => RepresentState::all(),
                'idRepresent' => $request->idRepresent,
            ]
        );
    }

    public function changeRepresentState(Request $request)
    {
        $changeState = ApartmentsRepresent::find($request->idRepresent);
        $changeState->represent_comment = $request->representComment;
        $changeState->represent_state_id = $request->representStateId;
        $changeState->push();
    }

    public function getRepresents()
    {
        $myRepresents = ApartmentsRepresent::where('user_id', '=', Auth::user()->id)
            ->where('represent_state_id', '=', null)
            ->with('apartment')
            ->orderbyDesc('datetime')
            ->get();

        return view('represents.representsTable',
            [
                'myRepresents' => $myRepresents,
            ]
        );
    }
}
