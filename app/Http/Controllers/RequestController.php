<?php

namespace App\Http\Controllers;

use App\RequestTask;
use App\User;
use Illuminate\Http\Request;
use App\RequestState;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class RequestController extends Controller
{

//    todo change hard code here ↓

    public function newRequest()
    {
        $zayavki = DB::table('request_lists')->get();
        return view('requests.newrequest', [
                'zayavki' => $zayavki
            ]
        );
    }

    public function setRequest(Request $request)
    {
        $date = date("Y-m-d H:i:s");
        //        todo переделать на eloquent
        DB::table('request_tasks')->insert(
            [
                'request_list_id' => $request->request_list_id,
                'request_state_id' => 1,
                'datetime' => $date,
                'user_id' => Auth::user()->id,
                'request_comment' => $request->request_comment
            ]
        );
        $userName =User::find(Auth::user()->id)->name;
        $userEmail=User::find(Auth::user()->id)->email;

//        $Telegram =  (new TelegramController)->sendTelegramMessage($userName,$userEmail,$request->request_comment,$date);
        header("Location: /home");
        $ImageController = new ImageController();
        die();
    }

    public function changeStatus(Request $request) //ajax form for change status подтвердена, процес, готово
    {
        //        todo переделать на eloquent
        switch ($request->state_name) {
            case ('Подадена'):
                $request_state_id = 1;
                break;
            case ('Отхвърлена'):
                $request_state_id = 2;
                break;
            case ('Готово'):
                $request_state_id = 3;
                break;
        }
        $affected = DB::table('request_tasks')
            ->where('id', $request->id)
            ->update(['request_state_id' => $request_state_id]);
    }

    public function requestsBroker()
    {

//        todo переделать на eloquent
        $RequestTask = new RequestTask();
        $requestTasks = $RequestTask
            ->with('users')
            ->with('requestStates')
            ->with('requestList')
            ->orderByDesc('datetime')
            ->where('user_id', '=', Auth::user()->id)
            ->paginate(30);
        return view('requests.requests', [
            'requestStates' => RequestState::all(),
            'requestTasks' => $requestTasks
        ]);
    }

    public function requests()
    {
//        todo переделать на eloquent
        $RequestTask = new RequestTask();
        $requestTasks = $RequestTask
            ->with('users')
            ->with('requestStates')
            ->with('requestList')
            ->orderBy('datetime', 'desc')
            ->orderBy('request_state_id', 'asc');

        if (session('role') == 'broker') {
            $requestTasks = $requestTasks->where('user_id', '=', Auth::user()->id);
        }

        $requestTasks = $requestTasks->paginate(30);
        return view('requests.requests', [
            'requestStates' => RequestState::all(),
            'requestTasks' => $requestTasks
        ]);
    }

}
