<?php

namespace App\Http\Controllers;

use App\ApartmentsRepresent;
use App\RepresentState;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $myRepresents = ApartmentsRepresent::where('user_id', '=', Auth::user()->id)
            ->where('represent_state_id', '=', null)
            ->with('apartment')
            ->orderbyDesc('datetime')
            ->get();

        return view('homeSB',
            [
                'myRepresents' => $myRepresents,
            ]
        );
    }


}
