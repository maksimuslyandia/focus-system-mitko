<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;


class BrokerController extends Controller
{
    public function getBrokers()
    {
        $User = new User();
        $brokers = $User
        ->with("roles")
        ->get();
        return view('brokers.brokers',
            [
                'brokers'=>$brokers
            ]
        );
    }

    public function updateBrokerView(Request $request, $brokerID)
    {
        $User = new User();
        $broker = $User
        ->with("roles")
        ->find($brokerID);
        return view('brokers.brokerUpdate',
            [
                'broker'=>$broker
            ]
        );
    }

    public function updateBroker(Request $request)
    {
        if($request->sub_role === "Наеми"){
            $request->sub_role = null;
        }

        $broker = User::with("role")->find($request->brokerID);
        $broker->sub_role = $request -> sub_role;
        $broker->work_phone = $request -> work_phone;
        $broker->personal_phone = $request -> personal_phone;
        $broker->username = $request->username;
        $broker->role->user_role_id = intval($request->role);

        if($request->name === null){
            $broker->name = $request->username;
        } else {
            $broker->name = $request->name;
        }

        if($request->change_pass == 1 && $request->new_pass){
            $broker->password = Hash::make($request->new_pass); 
        }

        $broker->push();

        return view('brokers.brokerUpdate',
            [
                'broker'=>$broker
            ]
        );
    }
}
