<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\ApartmentModel;
use App\ClientLikeApartmentRules;
use App\ApartmentParam;
use App\CallState;
use App\Status;
use App\ClientRole;
use App\ApartmentRole;
use App\ApartmentPhoto;
use App\ApartmentParamsRule;
use App\ApartmentRoleRule;
use App\Phone;
use App\Email;
use App\Client;
use App\ClientEmailRule;
use App\ClientPhoneRule;
use App\ClientApartmentRule;
use App\ClientRoleRule;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;


class ApartmentController extends Controller
{
    public function getApartments(Request $request)
        /** Изважда всичко за Апартаменти **/
    {

        if ($request->clientId != null) {
            $Client = new Client();
            $client = $Client->find($request->clientId);
            session()->put('clientId', $request->clientId);
        } else {
            session()->forget(['clientId']);
        }

        if (isset($request->ogled)) {
            // $Client = new Client;
            // $client = $Client->find($request->clientId);
            session()->put('ogled', $request->ogled);
        } else {
            session()->forget(['ogled']);
        }

        $Apartments = new Apartment();
        $ApartmentParam = new ApartmentParam();
        $ApartmentParams = $ApartmentParam->with('subApartmentParams')->get();
        $Apartments = $Apartments
            ->with('photos') //снимки за апартамети
            ->with('calls') //Инфо за обаждания на даден апартемент (Коментар, време на обаждане, кой се обадил)
            ->with('clientsLike') //Инфо за обаждания на даден апартемент (Коментар, време на обаждане, кой се обадил)
            ->with('clients') //Инфо за клиенти на даден апартемент
            ->with('clients.phones') //Инфо за клиенти на даден апартемент
            ->with('key') //Инфо за клиенти на даден апартемент
            ->with('lock_broker')
            ->with('exclusive_contract');

        if (session('role') == 'broker') {
            $Apartments->Where('status_id', '=', 1);
        }
        
        if (Auth::user()->sub_role == 'sales') {
            $Apartments = $Apartments->whereHas('apartmentRoles', function ($query) {
                $query->where('role_name', '=', 'продаден');
                $query->orwhere('role_name', '=', 'продава се');
            });
        } elseif(Auth::user()->sub_role == null) {
            $Apartments = $Apartments->whereHas('apartmentRoles', function ($query) {
                $query->where('role_name', '=', 'отдаден');
                $query->orwhere('role_name', '=', 'отдава се');
        });
    }

        if(isset($request->sub_role)){
            if ($request->sub_role == 'sales') {
                $Apartments = $Apartments->whereHas('apartmentRoles', function ($query) {
                    $query->where('role_name', '=', 'продаден');
                    $query->orwhere('role_name', '=', 'продава се');
                });
            } else {
                    $Apartments = $Apartments->whereHas('apartmentRoles', function ($query) {
                    $query->where('role_name', '=', 'отдаден');
                    $query->orwhere('role_name', '=', 'отдава се');
                    });
            }
        }
//        $Apartments = $Apartments->orderByDesc('created_at');
        $Apartments = $Apartments->orderBy('id','DESC');
        $Apartments = $Apartments->paginate(20); //Ще направи да има по 30 апартамента на страница
        return view('apartments.apartments',
            [
                'apartmentsInfo' => $Apartments,
                'apartmentParams' => $ApartmentParams,
                'callStates' => CallState::all(),
                'client' => (isset($client)) ? $client : '',
                'statuses' => Status::all(),
            ]
        );
}

    public function getApartment(Request $request, $apartmentId)
        /** Изважда един Апартамент **/
    {
        $Apartments = new Apartment();
        $apartmentInfo = $Apartments
            ->with('statuses') //Иформация за статус
            ->with('params') //параметри които имат апартаменти
            ->with('photos') //снимки за апартамети
            ->with('apartmentParams') //общо параметри за фронт енд
            ->with('apartmentRoles') //ролите на даден апартементы
            ->with('calls')
            ->with('clients') //Инфо за обаждания на даден апартемент (Коментар, време на обаждане, кой се обадил)
            ->find($apartmentId); //ще ми извади един апартмент

        $brokerKeys = false;
        if(isset($apartmentInfo->keys)){
            $brokerKeys = User::find($apartmentInfo->keys);
        }

        $viewToReturn = 'apartments.apartmentInfo';
        if (strpos($request->url(), "update") !== false) {
            $viewToReturn = 'apartments.apartmentRedaction';
        }

        return view($viewToReturn,
            [
                'apartmentInfo' => $apartmentInfo,
                'statuses' => Status::all(),
                'clientRoles' => ClientRole::all(),
                'apartmentRoles' => ApartmentRole::all(),
                'brokers' => User::all(),
                'brokerKeys' => $brokerKeys
            ]
        );
    }

    public function saveApartment(Request $request)
    {
        $Apartments = new Apartment();
        $apartment = $Apartments->find($request->apartment_id);
        $apartment->address = $request->address;
        $apartment->status_id = $request->apartment_status;
        $apartment->otoplenie = $request->otoplenie;
        $apartment->region = $request->region;
        if (isset($request->izloginie)) {
            foreach ($request->izloginie as $islogenie) {
                if ($islogenie == 'izlogenie_iztok') {
                    $apartment->izlogenie_iztok = 1;
                }
                if ($islogenie == 'izlogenie_zapad') {
                    $apartment->izlogenie_zapad = 1;
                }
                if ($islogenie == 'izlogenie_sever') {
                    $apartment->izlogenie_sever = 1;
                }
                if ($islogenie == 'izlogenie_ug') {
                    $apartment->izlogenie_ug = 1;
                }
            }
        }

        if($request->key_broker === "false"){
            $apartment->keys = NULL;
        }else{
            $apartment->keys = $request->key_broker;
        }

        $apartment->has_key = $request->has_key;
        $apartment->vid_stroitelstvo = $request->vid_stroitelstvo;
        $apartment->obzavegdane = $request->obzavegdane;
        $apartment->vid_imot = $request->vid_imot;
        $apartment->prehod = $request->prehod;
        $apartment->izolatsia = $request->izolatsia;
        $apartment->elevator = $request->elevator;
        $apartment->komisionna = $request->komisionna;
        $apartment->etag = $request->etag;
        $apartment->obsho_etaga = $request->obsho_etaga;
        $apartment->komentar_za_etaga = $request->komentar_za_etaga;
        $apartment->orientir_za_address = $request->orientir_za_address;
        $apartment->opisanie_na_obzavegdane = $request->opisanie_na_obzavegdane;
        $apartment->opisanie_za_nastilki = $request->opisanie_za_nastilki;
        $apartment->kvadratura_rzp = $request->kvadratura_rzp;
        $apartment->kvadratura_zp = $request->kvadratura_zp;
        $apartment->finished_state = $request->finished_state;
        $apartment->year_build = $request->year_build;
        $apartment->cena_eur = $request->cena_eur;
        $apartment->komesionna_zabelegka = $request->komesionna_zabelegka;
        $apartment->brokerska_belegka = $request->brokerska_belegka;
        $apartment->razpredelenie = $request->razpredelenie;
        $apartment->offer_website = $request->offer_website;
        $apartment->offer_date = $request->offer_date;
        $apartment->price_comment = $request->price_comment;
        $apartment->exclusive_contract_broker = $request->exclusive_contract_broker;
        $apartment->has_exclusive_contract = $request->has_exclusive_contract;
        $apartment->is_locked = $request->is_locked;
        $apartment->locked_broker = $request->locked_broker;

       if(!empty($request->client)){
            $apartment->clients[0]->first_name = $request->client[0];
            $apartment->clients[0]->last_name = $request->client[1];
            $apartment->clients[0]->status_id = $request->client_status;
  
            $apartment->push();
        }
        $apartment->push();


      

	if($request->phoneID != null){

        $Phone = Phone::find($request->phoneID);
        $Phone->phone = $request->phone;
        $Phone->save();
}

        if(isset($request->clientID)){
            $newClientRoleRule = ClientRoleRule::where('client_id', $request->clientID)->get();
            $newClientRoleRule[0]->client_id = $request->clientID;
            $newClientRoleRule[0]->client_role_id = $request->client_roles;
            $newClientRoleRule[0]->save();
        }

        if($request->emailID != null){

        $newClientEmailRule = Email::find($request->emailID);
        $newClientEmailRule->email = $request->email;
        $newClientEmailRule->save();
}
        $clientNames = [];
        foreach ($request->all() as $key => $req) {
            if (Str::contains($key, "client")) {
                array_push($clientNames, $req);
            } else {
                continue;
            }
        }

 
        // foreach ($apartment->clients as $key => $client) {
        //     $names = $clientNames[$key];
        //     $client->first_name = $names[0];
        //     $client->middle_name = $names[1];
        //     $client->last_name = $names[2];
        //     $client->save();
        // }

        $ImageController = new ImageController();
        if ($request->photo != null) {
            $imageNames = $ImageController->save($request, $request->apartment_id);
            if ($imageNames != null) {
                foreach ($imageNames as $key => $imageName) {
                    $ApartmentPhoto = new ApartmentPhoto();
                    $ApartmentPhoto->photo_path = $imageName;
                    $ApartmentPhoto->apartment_id = $request->apartment_id;
                    $ApartmentPhoto->save();
                }
            }
        }

        return redirect()->action(
            'ApartmentController@getApartments'
        );
    }

    public function createApartment()
    {

        $ApartmentParams = ApartmentParam::with("subApartmentParams")
            ->get();
        return view("apartments.apartmentCreation",
            [
                // 'apartmentInfo' => $apartmentInfo,
                'statuses' => Status::all(),
                'clientRoles' => ClientRole::all(),
                'apartmentRoles' => ApartmentRole::all(),
                'apartmentParams' => $ApartmentParams,
                'brokers' => User::all()
            ]
        );
    }

    //TODO:Optimize this
    public function saveNewApartment(Request $request)
    {

        $newApartment = new Apartment();
        $newApartment->address = $request->address;
        $newApartment->status_id = $request->apartment_status;
        $newApartment->otoplenie = $request->otoplenie;
        $newApartment->region = $request->region;
        if (isset($request->izlogenie)) {
            foreach ($request->izlogenie as $islogenie) {
                if ($islogenie == 'izlogenie_iztok') {
                    $newApartment->izlogenie_iztok = 1;
                }
                if ($islogenie == 'izlogenie_zapad') {
                    $newApartment->izlogenie_zapad = 1;
                }
                if ($islogenie == 'izlogenie_sever') {
                    $newApartment->izlogenie_sever = 1;
                }
                if ($islogenie == 'izlogenie_ug') {
                    $newApartment->izlogenie_ug = 1;
                }
            }
        }
        if($request->key_broker !== "false") {
            $newApartment->keys = $request->key_broker;
        }

        $newApartment->has_key = $request->has_key;
        $newApartment->vid_stroitelstvo = $request->vid_stroitelstvo;
        $newApartment->obzavegdane = $request->obzavegdane;
        $newApartment->vid_imot = $request->vid_imot;
        $newApartment->prehod = $request->prehod;
        $newApartment->izolatsia = $request->izolatsia;
        $newApartment->elevator = $request->elevator;
        $newApartment->komisionna = $request->komisionna;
        $newApartment->etag = $request->etag;
        $newApartment->obsho_etaga = $request->obsho_etaga;
        $newApartment->komentar_za_etaga = $request->komentar_za_etaga;
        $newApartment->orientir_za_address = $request->orientir_za_address;
        $newApartment->opisanie_na_obzavegdane = $request->opisanie_na_obzavegdane;
        $newApartment->opisanie_za_nastilki = $request->opisanie_za_nastilki;
        $newApartment->cena_eur = $request->cena_eur;
        $newApartment->komesionna_zabelegka = $request->komesionna_zabelegka;
        $newApartment->brokerska_belegka = $request->brokerska_belegka;
        $newApartment->razpredelenie = $request->razpredelenie;
        $newApartment->price_comment = $request->price_comment;
        $newApartment->offer_date = $request->offer_date;
        $newApartment->offer_website = $request->offer_website;
        $newApartment->razpredelenie = $request->razpredelenie;
        $newApartment->year_build = $request->year_build;
        $newApartment->kvadratura_rzp = $request->kvadratura_rzp;
        $newApartment->kvadratura_zp = $request->kvadratura_zp;
        $newApartment->finished_state = $request->finished_state;
        $newApartment->has_exclusive_contract = $request->has_exclusive_contract;
        $newApartment->exclusive_contract_broker = $request->exclusive_contract_broker;
        $newApartment->locked_broker = $request->locked_broker;
        $newApartment->is_locked = $request->locked_broker;        
        $newApartment->save();


//        foreach ($request->parameters as $key => $param) {
//            $ApartmentParamRule = new ApartmentParamsRule();
//            $ApartmentParamRule->apartment_id = $newApartment->id;
//            $ApartmentParamRule->apartment_params_id = $key + 1;
//            $ApartmentParamRule->parameter = $param;
//            $ApartmentParamRule->save();
//        }

        $ApartmentRoleRules = new ApartmentRoleRule();
        $ApartmentRoleRules->apartment_id = $newApartment->id;
        $ApartmentRoleRules->apartment_role_id = $request->apartment_role;
        $ApartmentRoleRules->save();
        if ($request->clientID != null) {
            $clientId = $request->clientID;
        } else {
            $newClient = new Client();
            $newClient->first_name = $request->client[0];
	        $newClient->last_name = $request->client[1];
            $newClient->status_id = $request->client_status;
            $newClient->save();
            $clientId = $newClient->id;

            $newEmail = new Email();
            $newEmail->email = $request->email;
            $newEmail->save();

            $newPhone = new Phone();
            $newPhone->phone = $request->phone;
            $newPhone->save();

            $newClientPhoneRule = new ClientPhoneRule();
            $newClientPhoneRule->client_id = $clientId;
            $newClientPhoneRule->phone_id = $newPhone->id;
            $newClientPhoneRule->save();

            $newClientRoleRule = new ClientRoleRule();
            $newClientRoleRule->client_id = $clientId;
            $newClientRoleRule->client_role_id = $request->client_roles;
            $newClientRoleRule->save();

            $newClientEmailRule = new ClientEmailRule();
            $newClientEmailRule->client_id = $clientId;
            $newClientEmailRule->email_id = $newEmail->id;
            $newClientEmailRule->save();
        }
        $newClientApartmentRule = new ClientApartmentRule();
        $newClientApartmentRule->client_id = $clientId;
        $newClientApartmentRule->apartment_id = $newApartment->id;
        $newClientApartmentRule->save();

        $ImageController = new ImageController();
        if ($request->photo != null) {
            $imageNames = $ImageController->save($request, $newApartment->id);
            if ($imageNames != null) {
                foreach ($imageNames as $key => $imageName) {
                    $ApartmentPhoto = new ApartmentPhoto();
                    $ApartmentPhoto->photo_path = $imageName;
                    $ApartmentPhoto->apartment_id =$newApartment->id;
                    $ApartmentPhoto->save();
                }
            }
        }
        return redirect()->action(
            'ApartmentController@getApartments'
        );
    }


    public function updateLikeApartment(Request $request)
    {
        $ClientLikeApartmentRules = new ClientLikeApartmentRules();
        $find = $ClientLikeApartmentRules
            ->where('client_id', $request->client_id)
            ->where('apartment_id', $request->apartment_id)
            ->get();

        if ($find->isEmpty()) {
            $ClientLikeApartmentRules->client_id = $request->client_id;
            $ClientLikeApartmentRules->apartment_id = $request->apartment_id;
            $ClientLikeApartmentRules->save();
            return 'saved';
        } else {
            $ClientLikeApartmentRules
                ->where('client_id', $request->client_id)
                ->where('apartment_id', $request->apartment_id)->delete();
            return 'deleted';
        }

    }

    public function stopLikeApartment()
    {
        session()->forget(['clientId']);
        return redirect('home');
    }

    public function searchApartmetns(Request $request)
    {
        $requestData = $request->all();
        //delete all null values with theirs key предпазва от празно ввеждане или случайно маркиране
        $requestDataArr = array_filter($requestData, function ($element) {
            return !empty($element);
        });
        unset($requestDataArr['_token']);
        if (isset($requestDataArr['page']))
            unset($requestDataArr['page']);
        /** Todo create select previous search to show in checkbox & selectBox & inputs */
        if ($request->clientId != null) {
            $Client = new Client();
            $client = $Client->find($request->clientId);
            session()->put('clientId', $request->clientId);
        } else {
            session()->forget(['clientId']);
        }

        if (isset($request->ogled)) {
            // $Client = new Client;
            // $client = $Client->find($request->clientId);
            session()->put('ogled', $request->ogled);
        } else {
            session()->forget(['ogled']);
        }

        $Apartments = new Apartment();
        $ApartmentParam = new ApartmentParam();
        $ApartmentParams = $ApartmentParam->with('subApartmentParams')->get();
        $Apartments = $Apartments
            ->with('photos') //снимки за апартамети
            ->with('calls') //Инфо за обаждания на даден апартемент (Коментар, време на обаждане, кой се обадил)
            ->with('clientsLike') //Инфо за обаждания на даден апартемент (Коментар, време на обаждане, кой се обадил)
            ->with('clients') //Инфо за клиенти на даден апартемент
            ->with('key'); //Инфо за клиенти на даден апартемент
        if (isset($request->priceFrom) && isset($request->priceTo)) {
            $Apartments = $Apartments->orwhereBetween('cena_eur', [intval($request->priceFrom), intval($request->priceTo)]);
        }
        if (isset($request->idSearch)) {
            $Apartments = $Apartments->where('id', '=', $request->idSearch);
        }
        if (isset($request->region)) {
            $Apartments = $Apartments->whereIn('region', $request->region);
        }
        if (isset($request->vid_imot)) {
            $Apartments = $Apartments->whereIn('vid_imot', $request->vid_imot);
        }
        if (isset($request->otoplenie)) {
            $Apartments = $Apartments->whereIn('otoplenie', $request->otoplenie);
        }
        if (isset($request->vid_stroitelstvo)) {
            $Apartments = $Apartments->whereIn('vid_stroitelstvo', $request->vid_stroitelstvo);
        }
        if (isset($request->obzavegdane)) {
            $Apartments = $Apartments->whereIn('obzavegdane', $request->obzavegdane);
        }

        if (isset($request->status)) {
            switch ($request->status) {
                case 1:
                    $Apartments->where('status_id', '=', 1);
                    break;
                case 2:
                    $Apartments->where('status_id', '=', 2);
                    break;
            }
        }

        if (session('role') == 'broker') {
            $Apartments->Where('status_id', '=', 1);
        }

        if (Auth::user()->sub_role == 'sales') {
            $Apartments = $Apartments->whereHas('apartmentRoles', function ($query) {
                $query->where('role_name', '=', 'продаден');
                $query->orwhere('role_name', '=', 'продава се');
            });
        } elseif(Auth::user()->sub_role == null) {
            $Apartments = $Apartments->whereHas('apartmentRoles', function ($query) {
                $query->where('role_name', '=', 'отдаден');
                $query->orwhere('role_name', '=', 'отдава се');
            });
        }
        // dd($Apartments);
        $Apartments = $Apartments->orderBy('id','DESC');
        $Apartments = $Apartments->paginate(20)->appends($requestDataArr);

        return view('apartments.apartments',
            [
                'apartmentsInfo' => $Apartments,
                'apartmentParams' => $ApartmentParams,
                'callStates' => CallState::all(),
                'statuses' => Status::all(),
                'client' => (isset($client)) ? $client : '',
                'requestDataArr' => $requestDataArr,
            ]
        );
    }


}
