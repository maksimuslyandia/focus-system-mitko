<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\ApartmentModel;
use App\Call;
use App\CallState;
use App\Client;
use App\Phone;
use App\UserImotSystemTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhoneController extends Controller
{

    public function chekPhoneInfo(Request $request)
    {
        $Phone = new Phone();
        $checkPhoneResult = $Phone
            ->with('getPhoneClient')
            ->where('phone', '=', $request->phone)
            ->get();
        if (!$checkPhoneResult->isEmpty()) {
            return $checkPhoneResult->toArray();
        } else {
            return 'Няма такъв номер';
        }

    }

    public function updateCall(Request $request)
    {
        $apartment = new ApartmentModel();
        $apartment->changeCallStatus($request);
    }

    public function updateCallSysTask(Request $request)
    {
        $apartmentId = $request->apartment_id;
        $apartment = new ApartmentModel();
        $apartment->changeCallStatus($request);
        $UserImotSystemTask = UserImotSystemTask::where('apartment_id', '=', $apartmentId)
            ->where('user_id', '=', Auth::user()->id)
            ->where('status_id', '=', 1)->first();
        $UserImotSystemTask->status_id = 2;
        $UserImotSystemTask->push();
        return;
    }

    public function getCallModal(Request $request)
    {
        $Apartment = Apartment::with('callHistory.client.phones')
            ->with('clients')
            ->find($request->apartment_id);
        return view('calls.CallModal',
            [
                'apartment' => $Apartment,
            ]);

    }

//    public function getStatesComment(Request $request)
//    {
//
//        $callStates = CallState::all();
//        return view('calls.callStatesComment',
//            [
//                'apartmentId' => $request->idApartment,
//                'clientId' => $request->idClient,
//                'callStates' => $callStates,
//            ]);
//    }

}
