<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\ApartmentPhoto;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function index()
    {
        return view('images.uploadImage');
    }

    public function save($images,$apartmentId)
    {

        request()->validate([
            'photo' => 'required',
            'photo.*' => 'mimes:jpg,jpeg,png'
        ]);

        $photoPaths = collect([]);
        if ($images->hasfile('photo')) {
                foreach ($images->file('photo') as $k => $file) {
                    $extension = $file->getClientOriginalExtension();
                    $filename =$apartmentId.'/'.time().'_' . $k . '.' . $extension;
                    $photoPaths->push($filename);
                    Storage::disk('apartments')->put($filename,\File::get($file));
                }
            return $photoPaths;
        }
        return null;
    }
}
