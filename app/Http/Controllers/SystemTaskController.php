<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\CallState;
use App\User;
use App\UserImotSystemTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\Promise\all;

class SystemTaskController extends Controller
{
    public function index() //Список с брокерами
    {
        $Users = User::where('status_id', '=', 1)
            ->with('roles')
            ->whereHas('roles', function ($query) {
                $query->where('user_role_id', '=', 2);
            })->get();

        return view('systemTasks.systemTasks',
            [
                'users' => $Users,
            ]
        );
    }


    public function passiveApartments(Request $request) //вывод апартаментов которые имеют одно фото и основную инфо
    {
        $idBroker = $request->idBroker;
        $Apartment = Apartment::with(['brokerSystemTasks' => function ($query) use ($idBroker) {
            $query->where('user_id', '=', $idBroker);
            $query->where('status_id', '!=', 2);
        }])
            ->with('systemTasks')
            ->where('status_id', '=', 2)
            ->whereDoesntHave('systemTasks', function ($query) use ($idBroker) {
                $query->where('user_id', '!=', $idBroker);
            })
        ->orwherehas('systemTasks', function ($query) use ($idBroker) {
        $query->where('status_id', '=', 1);
    });
        /** todo проверку для имотов которые более недели висят */
        return view('apartments.apartmentsAll',
            [
                'apartments' => $Apartment->paginate(30)->appends(['idBroker' => $idBroker]),
            ]
        );
    }

    public function getApartmentPhotos(Request $request, $idApartment)
    {
        return view('apartments.apartmentPhotos',
            [
                'apartmentPhotos' => Apartment::where('status_id', '=', 2)
                    ->with('photos')
                    ->find($idApartment)
            ]
        );
    }



    public function sysTaskChecker(Request $request)
    {
        if ($request->marker) {
            foreach ($request->marker as $k => $apartmentId) {
                $userId = $request->brokerId;
                $UserImotSystemTask = UserImotSystemTask:: // проверка, имеет кто-то такую задачу
                where('apartment_id', '=', $apartmentId)
                    ->where('user_id', '!=', $userId)
                    ->where('status_id', '=', 1)->get();

                if ($UserImotSystemTask->isEmpty()) { //в случае если задачу не имеет другой брокер, позволить получить задачу текущему брокеру
                    $UserImotSystemTask = UserImotSystemTask:: // проверка, имеет кто-то такую задачу
                    where('apartment_id', '=', $apartmentId)
                        ->where('user_id', '=', $userId)
                        ->where('status_id', '=', 1)->get();
                    if ($UserImotSystemTask->isEmpty()) {
                        //создать новую задачу
                        $UserImotSystemTask = new UserImotSystemTask;
                        $UserImotSystemTask->user_id = $userId;
                        $UserImotSystemTask->apartment_id = $apartmentId;
                        $UserImotSystemTask->status_id = 1;
                        $UserImotSystemTask->save();
                        $arr[$k]= ['state' => 'saved', 'apartment_id' => $apartmentId];
                    } else {
                        //удалить задачу
                        $userImotSystemTask = $UserImotSystemTask[0]->id;
                        UserImotSystemTask::find($userImotSystemTask)->delete();
                        $arr[$k]= ['state' => 'deleted', 'apartment_id' => $apartmentId];
                    }
                }

            }
            return $arr;
        } else {
            $userId = $request->brokerId;
            $apartmentId = $request->apartment_id;
            $UserImotSystemTask = UserImotSystemTask:: // проверка, имеет кто-то такую задачу
            where('apartment_id', '=', $apartmentId)
                ->where('user_id', '!=', $userId)
                ->where('status_id', '=', 1)->get();

            if ($UserImotSystemTask->isEmpty()) { //в случае если задачу не имеет другой брокер, позволить получить задачу текущему брокеру
                $UserImotSystemTask = UserImotSystemTask:: // проверка, имеет кто-то такую задачу
                where('apartment_id', '=', $apartmentId)
                    ->where('user_id', '=', $userId)
                    ->where('status_id', '=', 1)->get();
                if ($UserImotSystemTask->isEmpty()) {
                    //создать новую задачу
                    $UserImotSystemTask = new UserImotSystemTask;
                    $UserImotSystemTask->user_id = $userId;
                    $UserImotSystemTask->apartment_id = $apartmentId;
                    $UserImotSystemTask->status_id = 1;
                    $UserImotSystemTask->save();
                    return ['state' => 'saved', 'apartment_id' => $apartmentId];
                } else {
                    //удалить задачу
                    $userImotSystemTask = $UserImotSystemTask[0]->id;
                    UserImotSystemTask::find($userImotSystemTask)->delete();
                    return ['state' => 'deleted', 'apartment_id' => $apartmentId];
                }
            }
        }

    }

    public function systemTasksGet()
    {
        $Apartments = Apartment::whereHas('brokerSystemTasks', function ($query) {
            $query->where('user_id', '=', Auth::user()->id);
            $query->where('status_id', '=', 1);
        })
            ->paginate(30);

        return view('systemTasks.brokerImotTask', [
                'apartments' => $Apartments,
                'callStates' => CallState::all(),
            ]
        );
    }

}
