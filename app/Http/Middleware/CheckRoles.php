<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CheckRoles
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $roles
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
            if (session('role') == $roles) {
                return $next($request);
            } else {
                /** todo разобраться есть ошибки во входе в редакции */
                return $next($request);
//                return abort(404);
            }
        }



}
