<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

//SetRole sets role and sub_role
class SetRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('role') == null) {
            $userId = Auth::user()->id;
            $User = new User();
            $userRole = $User
                ->with('roles')
                ->find($userId);
            session()->put('role', $userRole->roles[0]->role_name);
            session()->put('sub_role', $userRole->sub_role);
            session()->put('id', $userRole->id);
            // session()->put('userID', $userId);
            return redirect('home');
        }else{
            return $next($request);
        }


//
    }
}
