<?php

namespace App\Http\Middleware;

use App\IpLog;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class IpLogger
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     *
     */

    public function handle($request, Closure $next)
    {
        $request->header('User-Agent');//инфо по браузеру
        $request->ip();
        $IpLog = new IpLog();
        $IpLog->ip = $request->ip();
        $IpLog->host = gethostname();
        $IpLog-> user_id = Auth::user()->id;
        $IpLog-> request_url =$request->headers->get('referer');
        $IpLog-> device =$request->header('User-Agent');
        $IpLog->save();

        return $next($request);
    }
}
