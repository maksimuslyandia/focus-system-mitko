<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersModel extends Model
{
    public function getUserRole()
    {
        if (isset(Auth::user()->id)) {
            $userId = Auth::user()->id;
            $users = DB::table('user_role_rules')
                ->Join('user_roles', 'user_role_rules.user_role_id', '=', 'user_roles.id')
                ->Join('users', 'user_role_rules.user_id', '=', 'users.id')
                ->where('users.id', '=', $userId)
                ->pluck('role_name');
            return $users[0];
        } else {
            return route('login');
        }
    }
}
