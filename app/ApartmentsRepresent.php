<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApartmentsRepresent extends Model
{
    protected $table = 'apartment_represents';

    public function client()
    {
        return $this->hasOne('App\Client', 'id', 'client_id');
    }

    public function apartment()
    {
        return $this->hasOne('App\Apartment', 'id', 'apartment_id');
    }


}
