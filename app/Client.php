<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';

    public function statuses()
    {
        return $this->hasOne('App\Status', 'id', 'status_id');
    }

    public function activeStatuses()
    {
        return $this->hasOne('App\Status', 'id', 'status_id')
            ->where('status_name', '=', 'active');
    }

    public function clientRoles()
    {
        return $this->belongsToMany('App\ClientRole', 'client_role_rules', 'client_id', 'client_role_id');
    }

    public function calls()
    {
        $calls = $this->hasMany('App\Call')
            ->with('clients')
            ->with('users')
            ->with('callStates')->orderByDesc('datetime');
        return $calls;
    }

    public function phones()
    {
        return $this->belongsToMany('App\Phone', 'client_phone_rules', 'client_id', 'phone_id');
    }

    public function emails()
    {
        return $this->belongsToMany('App\Email', 'client_email_rules', 'client_id', 'email_id');
    }

    public function apartments()
    {
        return $this->belongsToMany('App\Apartment', 'client_apartment_rule', 'client_id', 'apartment_id')
            ->with('calls');
    }

    public function getclientSearchRules()
    {
        return $this->hasOne('App\ClientSearchRule', 'client_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'users_clients_rules', 'client_id', 'user_id');
    }

    public function usersClients()
    {
        $userClients = $this->hasMany('App\UserClientRule');

        return $userClients;
    }

    public function likeApartments()
    {
        return $this->belongsToMany('App\Apartment', 'client_like_apartment_rules', 'client_id', 'apartment_id')
            ->with('params')
            ->with('calls')
            ->with('photos')
            ->with('apartmentParams')
            ->with('clients');
    }
}
