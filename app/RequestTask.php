<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestTask extends Model
{
    protected $table = 'request_tasks';

    public function users(){
        return $this->hasOne('App\User','id','user_id');
    }

    public function requestStates(){
        return $this->hasOne('App\RequestState','id','request_state_id');
    }

    public function requestList(){
        return $this->hasOne('App\RequestList','id','request_list_id');
    }
}
