<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    protected $table = 'calls';

    public function clients()
    {
        return $this->hasOne('App\Client','id','client_id')
            ->with('phones')
            ->with('emails');
    }

    public function users()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    public function callStates()
    {
        return $this->hasOne('App\CallState', 'id', 'call_state_id');
    }
    /////////////////////
     public function client()
        {
            return $this->hasOne('App\Client','id','client_id');
        }
}
