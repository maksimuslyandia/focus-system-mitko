<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClientRule extends Model
{
    protected $table = 'users_clients_rules';
}
