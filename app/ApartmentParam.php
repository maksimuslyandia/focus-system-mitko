<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApartmentParam extends Model
{
    
    protected $primaryKey = "id";
    protected $table = 'apartment_params';

    public function apartmentParamsRules(){
        return $this->belongsTo('App\ApartmentParamsRule','id','apartment_params_id');
    }

    public function subApartmentParams(){
        return $this->hasMany('App\SubApartmentParams','apartment_params_id','id');
    }
}
