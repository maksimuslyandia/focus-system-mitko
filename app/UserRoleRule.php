<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoleRule extends Model
{
    protected $primaryKey = "id";
    protected $fillable = [
        'user_role_id', 'user_id'
    ];
    protected $table = 'user_role_rules';
}
