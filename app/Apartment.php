<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Apartment extends Model
{
    protected $primaryKey = "id";


    protected $table = 'apartments';

    public function photos()
    {
        return $this->hasMany('App\ApartmentPhoto');
    }

    public function photo()
    {
        return $this->hasMany('App\ApartmentPhoto')->take(1);
    }

    public function key()
    {
        return $this->hasOne('App\User','id','keys');
    }

    public function exclusive_contract()
    {
        return $this->hasOne('App\User','id','exclusive_contract_broker');
    }

    public function lock_broker()
    {
        return $this->hasOne('App\User','id','locked_broker');
    }

    public function getApartmentPhotoes($idApartment)
    {
        return $this->hasMany('App\ApartmentPhoto')
            ->where('apartment_id', '=', $idApartment);
    }

    public function statuses()
    {
        return $this->hasOne('App\Status', 'id', 'status_id');
    }

    public function params()
    {
        return $this->hasMany('App\ApartmentParamsRule')->with('apartmentParams')->orderBy('apartment_params_id');
    }

    public function calls()
    {
        $calls = $this->hasMany('App\Call')
            ->with('clients')
            ->with('users')
            ->with('callStates')->orderByDesc('datetime');
        return $calls;
    }

    public function clients()
    {
        $clients = $this->belongsToMany('App\Client', 'client_apartment_rule', 'apartment_id', 'client_id')
//            ->with('emails')
//            ->with('phones')
//            ->with('apartments')
//            ->with('clientRoles')
        ;
        return $clients;
    }

    public function apartmentParams()
    {
        $params = $this->belongsToMany('App\ApartmentParam', 'apartment_params_rules', 'apartment_id', 'apartment_params_id')
            ->with("subApartmentParams")
            ->with("apartmentParamsRules");
        return $params;
    }

    public function test(){
        return $this->belongsTo('App\ApartmentRoleRule');
    }

    public function apartmentRoles()
    {
        return $this->belongsToMany(
            'App\ApartmentRole',
            'apartment_role_rules',
            'apartment_id',
            'apartment_role_id');
    }

    public function clientsLike()
    {
        return $this->belongsToMany('App\Client', 'client_like_apartment_rules', 'apartment_id', 'client_id')
            ->where('client_id', '=', session('clientId'));
    }

    public function systemTasks()
    {
        return $this->hasMany('App\UserImotSystemTask');
    }

    public function brokerSystemTasks()
    {
        return $this->hasMany('App\UserImotSystemTask');
    }

    /////////////////////////////////////
    public function callHistory()
    {
        return  $this->hasMany('App\Call');
    }


}
