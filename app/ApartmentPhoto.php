<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApartmentPhoto extends Model
{
    protected $table = 'apartment_photos';

}
