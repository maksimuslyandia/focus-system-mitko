<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApartmentModel extends Model
{
    public function changeCallStatus($callInfo)
    {
//        dd($callInfo['call_comment']);
       $max = DB::table('calls')->insert([
            'call_state_id' => isset($callInfo->call_state_id) ? $callInfo->call_state_id:3,
            'call_comment' => $callInfo->call_comment,
            'user_id' => Auth::user()->id,
            'client_id' => $callInfo->client_id,
            'apartment_id' => $callInfo->apartment_id,
            'datetime' => date('Y-m-d H:i:s'),
        ]);
//       dd($max);
//        return 'ok';
    }

}
