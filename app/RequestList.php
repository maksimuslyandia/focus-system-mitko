<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestList extends Model
{
    protected $table = 'request_lists';

    public function requests()
    {
        return $this->belongsTo('App\RequestTask','request_list_id','id');
    }
}
