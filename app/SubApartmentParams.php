<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubApartmentParams extends Model
{
    protected $table = 'sub_apartment_params';
}
