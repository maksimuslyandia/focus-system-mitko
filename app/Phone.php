<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $table='phones';

    public function getPhoneClient(){
        $clients = $this->belongsToMany('App\Client', 'client_phone_rules', 'phone_id', 'client_id');
        return $clients;
    }
}
