<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    protected $table = 'users';
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name', 'email', 'password', 'status_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function clients()
    {
        $calls = $this->belongsToMany('App\Client', 'users_clients_rules', 'user_id', 'client_id')
            ->with('emails')
            ->with('phones')
            ->with('clientRoles');
        return $calls;
    }

    public function activeClients()
    {
        $calls = $this->belongsToMany('App\Client', 'users_clients_rules', 'user_id', 'client_id')
            ->with('emails')
            ->with('phones')
            ->with('calls')
            ->with('clientRoles')
            ->where('status_id', '=', '1');
        return $calls;
    }
    
    public function role(){
        $roles = $this->hasOne('App\UserRoleRule');
        return $roles;
    }

    public function roles()
    {
        $roles = $this->belongsToMany('App\UserRole', 'user_role_rules', 'user_id', 'user_role_id');
        return $roles;
    }

}
