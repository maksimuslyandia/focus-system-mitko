<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserClientSystemTask extends Model
{
    protected $table = 'user_client_system_tasks';
}
