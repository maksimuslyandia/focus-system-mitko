<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ApartmentParamsRule extends Model
{
    protected $table = 'apartment_params_rules';

    public function apartmentParams(){
        return $this->hasOne('App\ApartmentParam','id','apartment_params_id')
        ->with('subApartmentParams');
    }
}
