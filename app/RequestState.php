<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestState extends Model
{
    protected $table = 'request_states';

    //
    public function requests()
    {
        return $this->belongsTo('App\RequestTask', 'request_state_id', 'id');
    }
}
