<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubClientParam extends Model
{
    protected $table = 'sub_client_params';
}
