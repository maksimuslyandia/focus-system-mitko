@extends('layouts.appSB')

@section('content')

    <script>
        $(function () {
            $("#success-alert").hide();
            $("#p_prldr").hide().fadeOut('slow');
            $('#saveRepresentData').on('click', function () {
                let representComment = $('#representComment').val();
                let representStateId = $('#representStateId').val();
                let idRepresent = $('#representStateId').attr('name');
                if (representComment !== '' && representStateId !== '') {
                    $.ajax({
                        method: "POST",
                        url: "/changerepresentstate",
                        type: "POST",
                        data: {
                            "_token": $('meta[name="csrf-token"]').attr('content'),
                            "representComment": representComment,
                            "representStateId": representStateId,
                            "idRepresent": idRepresent,
                        },
                        success: function (response) {
                            $("#myModal").modal('hide');
                        }
                    })
                        .done(function (msg) {
                            $("#p_prldr").hide().fadeOut('slow');
                            $("#max").append(msg); //дебагер ответа dd();
                            getRepresentsTable();
                        });
                } else {
                    alert('Запиши коментар и избери състояние')
                }
            });
            /*вытаскивает номер ячекйи кроме превого ряда*/
            $("div").on("click", "td", function () {
                var row = $("tr");
                [].forEach.call(row, function (elem) {
                    elem.addEventListener('click', function (el) {
                        let idRepresent = parseInt(this.children[0].innerHTML, 10); //нажатый элемент в таблице
                        if (this.children[0].innerHTML !== '#') { //игнорирование первого ряда
                            $("#p_prldr").show().fadeIn('slow');
                            $.get("/changestate/" + idRepresent)
                                .done(function (data) {
                                    $("#p_prldr").hide().fadeOut('slow');
                                    // let rerpesentData = JSON.parse(data);
                                    // let clientId = rerpesentData.client_id;
                                    $("#myModal").modal();
                                    $("#modal-body").html(data);
                                });
                        }
                    })
                });
            });
        });

        function getRepresentsTable() {
            $.get("/represents/table")
                .done(function (data) {
                    $("#represents").html(data);
                });
        }

    </script>
    <div id="max"></div>
    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>

    <!-- Content Row -->
    <div class="row">
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Earnings (Monthly)
                                (DEMO)
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">40,000 BGN</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Earnings (Annual)
                                (DEMO)
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">215,000 BGN</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tasks (DEMO)</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-info" role="progressbar" style="width: 50%"
                                             aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Calls (DEMO)
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">18</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(!$myRepresents->isEmpty())
        <div class="row">
            <div class="col-sm-4">
                <div id="represents">
                    <script>getRepresentsTable()</script>
                </div>

            </div>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Статус на огледа</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div id="modal-body" class="modal-body">

                    </div>
                    <div class="modal-footer">

                        <button id="saveRepresentData" type="button" class="btn btn-success">Запази
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                    </div>
                </div>

            </div>
        </div>
    @endif
    <!-- Modal -->
@endsection


