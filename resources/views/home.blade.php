@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@foreach($apartmentsInfo as $id => $apartment)
    <tbody>

    <tr id="row{{$id}}"
        @if(count($apartment->calls) == 0)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         if(empty($apartment->calls)){
        class="table-light"
        @else
            @switch($apartment->calls[0]->call_state_id)
                @case(1)
                class="table-success"
                @break
                @case(2)
                class="table-warning"
                @case(3)
                class="table-danger"
                @break
                @default
            @endswitch
        @endif
        >
        <th scope="row">{{$id}}</th>   <!--Apartment ID-->
        <th scope="row">
            @foreach($apartment->clients as $client)
                {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}<br> <!--Names-->
            @endforeach
        </th>
        <th scope="row">{{$apartment->address}}</th>
        <th scope="row"><img src={{empty($apartment->img) ? "Няма" : $apartment->img[0]->photo_path}}></th> <!--Images-->
        <th scope="row">
            @foreach($apartment->params as $param)
                @if($param->apartment_params_id == 1)
                    {{$param->parameter}}
                @endif
            @endforeach
        </th>
        <th scope="row">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal">Info</button>
            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal">Redact</button>
        </th>
    </tr>
    </tbody>
@endforeach
