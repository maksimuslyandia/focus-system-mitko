@extends('layouts.appSB')

@section('content')
    <div class="container">
dd($zayavki)
        <form action="{{URL::to('/rent/'.session('role').'/setrequest')}}" method="post">
            @csrf
            <div class="form-group">
                <h2>Създаване на нова заявка</h2>

                <div class="input-group mb-3 pt-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Относно</span>
                    </div>
                    <select name="request_list_id" id="inputState" class="form-control">
                        @foreach($zayavki as $k=> $zayavka)
                            <option name="request_name" value="{{$zayavka->id}}">{{$zayavka->request_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Описание</span>
                    </div>
                    <textarea name="request_comment" rows="10" class="form-control" placeholder="Опиши подробно заявка"
                              aria-label="With textarea"></textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
