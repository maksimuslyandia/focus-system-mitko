@extends('layouts.appSB')

@section('content')
{{--    <script>--}}
{{--        $(function () {--}}
{{--            $("select").change(function () {--}}
{{--                let state_name = this.value;--}}
{{--                let broker_request_id = this.id;--}}
{{--                $.ajax({--}}
{{--                    method: "POST",--}}
{{--                    url: "/admin/changeStatus",--}}
{{--                    type: "POST",--}}
{{--                    data: {--}}
{{--                        "_token": $('meta[name="csrf-token"]').attr('content'),--}}
{{--                        "state_name": state_name,--}}
{{--                        "id": broker_request_id--}}
{{--                    },--}}
{{--                    success: function (response) {--}}
{{--                        // $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {--}}
{{--                        //     $("#success-alert").slideUp(500);--}}
{{--                        // });--}}
{{--                    }--}}
{{--                })--}}
{{--                    .done(function (msg) {--}}
{{--                        //переберает данные обычного массива--}}
{{--                        /* for (let [key, value] of Object.entries(msg)) {--}}
{{--                             console.log(`${key}: ${value}`);--}}
{{--                         }*/--}}

{{--                        $("#alerts").append(msg); //дебагер ответа dd();--}}
{{--                        $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {--}}
{{--                            $("#success-alert").slideUp(500);--}}
{{--                        });--}}
{{--                        //todo найти способ поменять данную часть на автоматику, речь идет об выборке--}}
{{--                        switch (state_name) {--}}
{{--                            case 'Подадена':--}}
{{--                                $("#row" + broker_request_id).attr('class', 'table-danger');--}}
{{--                                break;--}}
{{--                            case 'Процес':--}}
{{--                                $("#row" + broker_request_id).attr('class', 'table-warning');--}}
{{--                                break;--}}
{{--                            default:--}}
{{--                                $("#row" + broker_request_id).attr('class', 'table-success');--}}
{{--                        }--}}
{{--                        // $("#row"+broker_request_id).attr('class','table-success');--}}
{{--                        // setTimeout(function (){--}}
{{--                        //     window.location.replace("requests")}, 2000);--}}
{{--                    });--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
    @dd('fdfd')
    <div id="content">
        {{$requestsInfo->links()}}
        <div class="alert alert-success" disabled id="success-alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong>Success! </strong> Успешно променен статус
        </div>
        <div id="alerts"></div>
        <table class="table table-sm">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th width="10%" scope="col">Статус</th>
                <th scope="col">Брокер</th>
                <th width="60%" scope="col">Коментар</th>
                <th width="20%" scope="col">Дата</th>
            </tr>
            </thead>
            <tbody>
            @foreach($requestsInfo as $request)
                <tr id="row{{$request->id}}"
                    @switch($request->request_state_id)
                    @case(1)
                    class="table-danger"
                    @break
                    @case(2)
                    class="table-warning"
                    @break
                    @case(3)
                    class="table-success"
                    @break
                    @endswitch
                >
                    <td scope="row">
                        {{$request->id}}
                    </td>
                    <th scope="row">
                        @if(session('role')=='broker')
                            {{$request->state_name}}
                        @else
                            <div class="form-group">
                                <select class="form-control" id="{{$request->id}}">
                                    @foreach($requestState as $state)
                                        @if($state->id == $request->request_state_id)
                                            <option selected>{{$state->state_name}}</option>
                                        @else
                                            <option>{{$state->state_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        @endif
                    </th>
                    <td>{{$request->name}}</td>
                    <td>{{$request->request_comment}}</td>
                    <td>{{$request->datetime}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        $("#success-alert").hide();
    </script>
@endsection
