@extends('layouts.appSB')
@section('content')
    <script>
        // Shorthand for $( document ).ready()
        $(function () {
            console.log("ready!");

            $("#phoneBtn").click(function () {
                $("#addPhone").append("<input value=\"\"  name=\"phone[]\" type=\"text\"\n" +
                    "                       class=\" mt-2 form-control\" id=\"phone\" placeholder=\"Телефон\">");
            });
        });
    </script>
    <form action="
    @if(session('role') == "admin")
    {{URL::to('/rent/admin/clients/update')}}
    @else
    {{URL::to('/rent/broker/clients/update')}}
    @endif" method="post" enctype="multipart/form-data">
        @csrf
        <h3 class="mt-5">За наемателя</h3>
        <hr>
        <div class="row">
            <div class="col-sm-6" @if(session('role')=='broker') style="display: none;"@endif>
                <label for="client_status">Статус на собственика</label>
                <select name="client_status" class="custom-select mr-sm-2">
                    @foreach($statuses as $status)
                        @if($status->id == $clientInfo->status_id)
                            <option value="{{$status->id}}" selected>{{$status->status_name}}</option>
                        @else
                            <option value="{{$status->id}}">{{$status->status_name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col-sm-6" @if(session('role')=='broker') style="display: none;"@endif>
                <label for="client_roles" @if(session('role')=='broker') style="display: none;"@endif>Роля на
                    собственика</label>
                <select name="client_roles" class="custom-select mr-sm-2" id="client_roles" disabled>
                    @foreach($clientInfo->clientRoles as $role)
                        @if(!array($clientInfo->clientRols) && ($clientInfo->clientRols->id == $role->id))
                            <option value="{{$role->id}}" selected>{{$role->role_name}}</option>
                        @else
                            <option value="{{$role->id}}">{{$role->role_name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <label for="first_name">Име</label>
                <input value="{{$clientInfo->first_name}}" required name="first_name"
                       type="text"
                       class="form-control" id="first_name"
                       >
            </div>

           <div class="col-sm-6">
                <label for="last_name">Фамилия</label>
                <input value="{{$clientInfo->last_name}}" name="last_name" type="text"
                       class="form-control" id="last_name">
            </div>
        </div>

        {{--            todo добавить возможность добавлени инфо--}}

        <div class="row mt-3" @if(session('role')=='broker') style="display: none;"@endif>
            <div class="col-sm-6" id="addPhone">
                <label for="phone">Телефон</label>
                {{--                <div class="input-group mb-3">--}}
                <div class="input-group-prepend">
                    <button class="btn btn-outline-secondary" id="phoneBtn" type="button">Add Phone</button>
                </div>
                @foreach($clientInfo->phones as $k=> $phone)
                    <input value="{{$phone->phone}}" @if($k==0)required @endif name="phone[]" type="text"
                           class="form-control mt-2" id="phone" placeholder="Телефон">
                @endforeach
                {{--                </div>--}}

            </div>
            <div class="col-sm-6">
                <label for="email"> Email </label>
                @if(!$clientInfo->emails->isEmpty())
                    <input disabled value="{{$clientInfo->emails[0]->email}}" required name="email"
                           type="text"
                           class="form-control"
                           id="email" placeholder="Email">
                @else
                    <input value="Няма" required name="email" type="text"
                           class="form-control"
                           id="email" placeholder="Email">
                @endif
            </div>
        </div>
        <!--Broker-->
        @if(session('role') == "admin")
            <div col-sm-12>
                <label for="email"> Избери Брокер </label>
                <select id="user_id" name="user_id" class="custom-select mr-sm-2">
                    @foreach($brokers as $broker)
                        @if(count($clientInfo->users) != 0)
                             @if($clientInfo->users[0]->id === $broker->id)
                                    <option value="{{$broker->id}}" selected>{{$broker->name}} ({{$broker->username}})</option>
                                @else
                                    <option value="{{$broker->id}}">{{$broker->name}} ({{$broker->username}})</option>
                                @endif
                        @else
                            <option value="{{$broker->id}}">{{$broker->name}} ({{$broker->username}})</option>
                        @endif
                    @endforeach
                </select>
            </div>
        @endif
        <input value="{{$clientInfo->id}}" name="client_id" style="display: none;">
        <hr>
        <h3 class="mt-3">Търси</h3>
        <div class="row mt-2">
            @foreach($params as $param)
                @if($param['param_type']=='select')
                    <div class="input-group input-group-sm mb-3 col-sm-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text"
                                  id="span{{$param['column_name']}}">{{$param['param_name']}}</span>
                        </div>
                        <select class="custom-select" name="{{$param['column_name']}}" id="{{$param['column_name']}}">
                            <option value="">Choose...</option>
                            @if(isset($param['selcted']))
                                <option selected value="{{$param['selcted']}}"> {{$param['selcted']}}</option>
                            @endif
                            @foreach($param['sub_param_name'] as $subParam)
                                <option value="{{$subParam}}">{{$subParam}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="row mt-2">
            {{--            @dd($params)--}}
            @foreach($params as $param)
                @if($param['param_type']=='multiselect')
                    <div class="input-group input-group-sm mb-3 col-sm-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text"
                                  id="span{{$param['column_name']}}">{{$param['param_name']}}</span>
                        </div>
                        <select class="custom-select" name="{{$param['column_name']}}" id="{{$param['column_name']}}">
                            <option value="">Choose...</option>
                            @if(isset($param['selcted']))
                                <option selected value="{{$param['selcted']}}"> {{$param['selcted']}}</option>
                            @endif
                            @foreach($param['sub_param_name'] as $subParam)
                                <option value="{{$subParam}}">{{$subParam}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="row mt-2">
            {{--            @dd($params)--}}
            @foreach($params as $param)
                @if($param['param_type']=='text')
                    <div class="input-group input-group-sm mb-3 col-sm-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text"
                                  id="{{$param['column_name']}}">{{$param['param_name']}}</span>
                        </div>
                        <textarea class="form-control" name="{{$param['column_name']}}"
                                  aria-label="With textarea">{{$param['label']}}</textarea>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="row mt-2">
            @foreach($clientParams as $param)
                @if($param['param_type']=='number')
                    <div class="input-group input-group-sm mb-3 col-sm-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text">{{$param->param_name}}</span>
                        </div>
                        <input value="" type="number" class="form-control" name="{{$param->column_name}}"
                               aria-describedby="inputGroup-sizing-sm">
                    </div>
                @endif
            @endforeach
        </div>
        <button class="btn btn-primary" type="submit">Запази</button>
    </form>
@endsection
