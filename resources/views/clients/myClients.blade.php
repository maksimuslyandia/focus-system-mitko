@extends('layouts.appSB')
@section('content')

    <script>
        (function () {
            $("#success-alert").hide();
        })();

        function calls(idModal, clienId, idApartment, call_comment) {
            var call_status = $('#call_status' + idApartment).val();
            call_comment = $('#' + call_comment).val();
            $.ajax({
                method: "POST",
                url: "/updatecall",
                type: "POST",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "call_state_id": call_status,
                    "call_comment": call_comment,
                    "client_id": clienId,
                    "apartment_id": null,
                },
                success: function (response) {
                    $("#max").append(response); //дебагер ответа dd();
                }
            })
                .done(function (msg) {
                    switch (call_status) {
                        case "1":
                            $("#row" + clienId).attr('class', 'table-success');
                            break;
                        case "2":
                            $("#row" + clienId).attr('class', 'table-warning');
                            break;
                        default:
                            $("#row" + clienId).attr('class', 'table-danger');
                            break;
                    }
                    // $("#max").append(msg); //дебагер ответа dd();
                    $('#' + idModal).modal('hide');
                    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                        $("#success-alert").slideUp(500);
                    });
                    location.reload();
                });
        }


        function ogled(idModal, clienId, idApartment) {
            var ogledDate = $('#ogledDate' + idApartment).val();
            var ogledTime = $('#ogledTime' + idApartment).val();
            if (ogledTime != '' && ogledDate != '' && new Date(ogledDate + ' ' + ogledTime) > Date.now()) {
                $.ajax({
                    method: "POST",
                    url: "/createNewOgeld",
                    type: "POST",
                    data: {
                        "_token": $('meta[name="csrf-token"]').attr('content'),
                        "ogledDate": ogledDate,
                        "ogledTime": ogledTime,
                        "client_id": clienId,
                        "apartment_id": idApartment,
                        "representStateId": representStateId
                    },
                    success: function (response) {
                        // $("#max").append(response); //дебагер ответа dd();
                    }
                })
                    .done(function (msg) {
                        console.log(msg);
                    });
            } else {
                alert('Попълни данни и провери дали дата не е минала')
            }
        }
    </script>
    <div id='max'></div>
    <h3>Моите клиенти</h3>
    @if($myClientsInfo !=null && !$myClientsInfo->activeClients->isEmpty())
        <table class="table table-sm">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Имена</th>
                <th scope="col">Функции</th>
            </tr>
            </thead>
            <tbody>
            @foreach($myClientsInfo->activeClients as $client)
                <tr id="row{{$client->id}}"
                    @if(count($client->calls) != 0 && strtotime(date('Y-m-d 00:00:00'))<strtotime($client->calls[0]->datetime))
                    @switch($client->calls[0]->call_state_id)
                    @case(1)
                    class="table-success"
                    @break
                    @case(2)
                    class="table-warning"
                    @break
                    @case(3)
                    class="table-danger"
                    @break
                    @endswitch
                    @endif
                ><td>{{$client->id}}</td>
                    <td>{{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</td>
                    <td>
                        <a href="/rent/broker/clients/get/{{$client->id}}" class="btn btn-primary btn-sm active"
                           role="button" aria-pressed="true"><i class="fas fa-info"></i></a>
                        <a href="/rent/admin/clients/update/{{$client->id}}" class="btn btn-warning btn-sm active"
                           role="button" aria-pressed="true"><i class="fas fa-edit"></i> </a>
                        <button type="button" class="btn btn-sm btn-success" data-toggle="modal"
                                data-target="#contact{{$client->id}}">
                            <i class="fas fa-mobile-alt"></i>
                        </button>

                        <a href="/rent/admin/apartments/search/{{$client->id}}" class="btn btn-info btn-sm active"
                           role="button" aria-pressed="true"><i class="fas fa-search"></i></a>
                        <!-- Modal -->
                        <div class="modal fade" id="contact{{$client->id}}" tabindex="-1" role="dialog"
                             aria-labelledby="contact{{$client->id}}" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"
                                            id="contact{{$client->id}}">
                                            {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}<br>
                                           ID Клиента: {{$client->id}}
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <table>
                                            <thead>
                                            <tr>
                                                <th scope="col">Names</th>
                                                <th scope="col">Emails</th>
                                                <th scope="col">Phones</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    @if(count($client->phones) != 0)
                                                        @foreach($client->phones as $phone)
                                                            {{ $phone->phone}}<br>
                                                        @endforeach
                                                    @else
                                                        Няма
                                                    @endif
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Оглед</label>
                                            <select class="form-control" id="call_status{{$client->id}}">
                                                <option selected value="">Избери</option>
                                                @foreach($callStates as $state)
                                                    <option value="{{$state->id}}">{{$state->call_status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Коментар към разговор</label>
                                            <textarea class="form-control" id="call_comment{{$client->id}}"
                                                      rows="3"></textarea>
                                        </div>
{{--                                        @dd($client)--}}
                                        <table class="table table-sm">
                                            <thead>
                                            <tr>
                                                <th scope="col">Call Статус</th>
                                                <th scope="col">Коментар</th>
                                                <th scope="col">Дата</th>
                                            </tr>
                                            </thead>
                                            <tbody id="calls{{$client->id}}">
                                            @foreach($client->calls as $key => $call)
                                                @if($key<5)
                                                    <tr
                                                        @switch($call->call_state_id)
                                                        @case(1)
                                                        class="table-success"
                                                        @break
                                                        @case(2)
                                                        class="table-warning"
                                                        @case(3)
                                                        class="table-danger"
                                                        @break
                                                        @default
                                                        class="table-light"
                                                        @endswitch
                                                    >
                                                        <td>{{$call->callStates->call_status}}</td>
                                                        {{--                                                <td><b>Client ID:</b>{{$call->clients->id}}<br>--}}
                                                        {{--                                                    {{$call->clients->first_name}} {{$call->clients->middle_name}} {{$call->clients->last_name}}--}}
                                                        {{--                                                </td>--}}
                                                        <td>{{$call->call_comment}}</td>
                                                        <td>{{$call->datetime}}</td>

                                                    </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>

                                        <button type="button"
                                                onclick='calls("contact{{$client->id}}","{{$client->id}}","{{$client->id}}","call_comment{{$client->id}}",)'
                                                class="btn btn-primary">
                                            Save changes
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="row">
            <div class="col-sm-6">
                <div class="alert alert-dark">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <span>Съжелявам нещо нямаш още клиенти, изпий кафе от най-хубава кафемашина и почвай да печелиш!</span><br>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="alert alert-dark">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <span>В другия случай, ако още не ти е одобрена заявката, обърни се към системени администратор за подробностите</span>
                </div>
            </div>
        </div>
    @endif
{{--    <a href="/rent/admin/apartments/ogled/true" class="btn btn-success btn-sm active"--}}
{{--                           role="button" aria-pressed="true">Добави  предварителен оглед</a>--}}
{{--    <h3 class="mt-3">Предварителни огледи</h3>--}}
{{--    <div class="table-responsive">--}}
{{--        <table class="table">--}}
{{--            <thead>--}}
{{--                <tr>--}}
{{--                    <th scope="col">ID на наемател</th>--}}
{{--                    <th scope="col">Имена на наемател</th>--}}
{{--                    <th scope="col">ID на апартамент</th>--}}
{{--                    <th scope="col">Адрес на апартамент</th>--}}
{{--                    <th scope="col">Дата на оглед</th>--}}
{{--                    <th scope="col">Час на оглед</th>--}}
{{--                    <th scope="col">Функции</th>--}}

{{--                </tr>--}}
{{--            </thead>--}}
{{--            <tbody>--}}
{{--                <tr>--}}
{{--                    @foreach($representInfo as $info)--}}
{{--                        <tr $representInfo>--}}
{{--                            <td >{{$info->client->id}}</td>--}}
{{--                            <td >{{$info->client->first_name}} {{$info->client->middle_name}} {{$info->client->last_name}}</td>--}}
{{--                            <td >{{$info->apartment->id}}</td>--}}
{{--                            <td >{{$info->apartment->address}}</td>--}}
{{--                            <td >{{$info->date}}</td>--}}
{{--                            <td >{{$info->time}}</td>--}}
{{--                            <td>--}}
{{--                            <a href="/rent/{{session('role')}}/apartments/get/{{$info->apartment->id}}" role="button"--}}
{{--                                class="btn btn-primary btn-sm active" data-toggle="tooltip" data-placement="top"--}}
{{--                                title="Информация"><i class="fas fa-info"></i></a>--}}
{{--                            <a href="/rent/{{session('role')}}/apartments/update/{{$info->apartment->id}}" role="button"--}}
{{--                                class="btn btn-warning btn-sm active" data-toggle="tooltip" data-placement="top"--}}
{{--                                title="Редактирай"><i class="fas fa-edit"></i></a>--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                    @endforeach--}}
{{--                </tr>--}}
{{--            </tbody>--}}
{{--        </table>--}}
{{--    </div>--}}
@endsection
 