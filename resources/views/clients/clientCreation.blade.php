@extends('layouts.appSB')
@section('content')

    <form action="{{URL::to('/rent/admin/clients/create')}}" method="post" enctype="multipart/form-data">
        @csrf
        <h3 class="mt-5">За клиента</h3>
        <hr>
        <div class="row mt-2">
            <div class="col-sm-4">
                <label for="client_status">Статус на клиента</label>
                <select required id="client_status" name="client_status" class="custom-select mr-sm-2">
                    <option value="">Избери</option>
                    @foreach($statuses as $status)
                        <option value="{{$status->id}}">{{$status->status_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4">
                <label for="client_roles">Роля на клиента</label>
                <select required name="client_roles" class="custom-select mr-sm-2" id="client_roles">
                    <option value="">Избери</option>
                    @foreach($clientRoles as $role)
                        <option value="{{$role->id}}">{{$role->role_name}}</option>
                    @endforeach
                </select>
            </div>
{{--            @dd($brokers)--}}
            <div class="col-sm-4">
                <label for="client_roles">Брокер</label>
                <select required name="broker_id" class="custom-select mr-sm-2" id="client_roles">
                    <option value="">Избери</option>
                    @foreach($brokers as $broker)
                        <option value="{{$broker->id}}">{{$broker->name}} ({{$broker->username}})</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-sm-6">
                <label for="first_name">Име</label>
                <input value="" required name="first_name"
                       type="text"
                       class="form-control" id="first_name">
            </div>
             <div class="col-sm-6">
                <label for="last_name">Фамилия</label>
                <input value="" name="last_name" type="text"
                       class="form-control" id="last_name">
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-sm-4">
                <label for="phone">Телефон</label>
                <input value="" required name="phone" type="text"
                       class="form-control" id="phone" placeholder="Телефон">
            </div>
            <div class="col-sm-4">
                <label for="email">Email</label>
                <input value="" name="email" type="text"
                       class="form-control"
                       id="email" placeholder="Email">
            </div>
        </div>

        <h3 class="mt-4">Търси</h3>
        <div class="row mt-2">
            @foreach($clientParams as $param)
                @if($param->param_type=='select')
                    <div class="input-group input-group-sm mb-3 col-sm-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="span{{$param->column_name}}">{{$param->param_name}}</span>
                        </div>
                        <select class="custom-select" name="{{$param->column_name}}" id="{{$param->column_name}}">
                            <option value="">Choose...</option>
                            @foreach($param->subClientParams as $subParam)
                                <option value="{{$subParam->sub_param_name}}">{{$subParam->sub_param_name}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="row mt-2">
            @foreach($clientParams as $param)
                @if($param->param_type=='multiselect')
                    <div class="input-group input-group-sm mb-3 col-sm-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="span{{$param->column_name}}">{{$param->param_name}}</span>
                        </div>
                        <select class="custom-select" name="{{$param->column_name}}" id="{{$param->id}}">
                            <option value="">Choose...</option>
                            @foreach($param->subClientParams as $subParam)
                                <option value="{{$subParam->sub_param_name}}">{{$subParam->sub_param_name}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="row mt-2">
            @foreach($clientParams as $param)
                @if($param->param_type=='text')
                    <div class="input-group input-group-sm mb-3 col-sm-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="span{{$param->column_name}}">{{$param->param_name}}</span>
                        </div>
                        <textarea class="form-control" name="{{$param->column_name}}" aria-label="With textarea"></textarea>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="row mt-2">

            @foreach($clientParams as $param)
                @if($param->param_type=='number')
                    <div class="input-group input-group-sm mb-3 col-sm-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="span{{$param->id}}">{{$param->param_name}}</span>
                        </div>
                        <input value="" type="number" class="form-control" name="{{$param->column_name}}" id="{{$param->id}}"
                               aria-describedby="inputGroup-sizing-sm">
                    </div>
                @endif
            @endforeach
        </div>

        <button class="btn btn-primary m-4" type="submit">Добави</button>
    </form>

@endsection
