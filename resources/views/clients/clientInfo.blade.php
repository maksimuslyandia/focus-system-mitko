@extends('layouts.appSB')
@section('content')
    <script>
        function calls(idModal, clienId, idApartment, call_comment) {
            var call_status = $('#call_status' + idApartment).val();
            call_comment = $('#' + call_comment).val();
            $.ajax({
                method: "POST",
                url: "/updatecall",
                type: "POST",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "call_state_id": call_status,
                    "call_comment": call_comment,
                    "client_id": clienId,
                    "apartment_id": idApartment,
                },
                success: function (response) {
                    $("#max").append(response); //дебагер ответа dd();
                }
            })
                .done(function (msg) {
                    switch (call_status) {
                        case "1":
                            $("#row" + idApartment).attr('class', 'table-success');
                            break;
                        case "2":
                            $("#row" + idApartment).attr('class', 'table-warning');
                            break;
                        default:
                            $("#row" + idApartment).attr('class', 'table-danger');
                            break;
                    }
                    // $("#max").append(msg); //дебагер ответа dd();
                    $('#' + idModal).modal('hide');
                    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                        $("#success-alert").slideUp(500);
                    });
                });
        }

        function ogled(idModal, clienId, idApartment) {
            var ogledDate = $('#ogledDate' + idApartment).val();
            var ogledTime = $('#ogledTime' + idApartment).val();
            if (ogledTime != '' && ogledDate != '' && new Date(ogledDate + ' ' + ogledTime) > Date.now()) {
                $.ajax({
                    method: "POST",
                    url: "/updateogled",
                    type: "POST",
                    data: {
                        "_token": $('meta[name="csrf-token"]').attr('content'),
                        "ogledDate": ogledDate,
                        "ogledTime": ogledTime,
                        "client_id": clienId,
                        "apartment_id": idApartment,
                    },
                    success: function (response) {
                        // $("#max").append(response); //дебагер ответа dd();
                    }
                })
                    .done(function (msg) {
                        // switch (call_status) {
                        //     case "1":
                        //         $("#row" + idApartment).attr('class', 'table-success');
                        //         break;
                        //     case "2":
                        //         $("#row" + idApartment).attr('class', 'table-warning');
                        //         break;
                        //     default:
                        //         $("#row" + idApartment).attr('class', 'table-danger');
                        //         break;
                        // }
                        $("#max").append(msg); //дебагер ответа dd();
                        $('#' + idModal).modal('hide');
                    });
            } else {
                alert('Попълни данни и провери дали дата не е минала')
            }
        }
    </script>
    <div id="max"></div>

    <h3>Инфо за клиент {{$clientInfo->id}}
        @foreach($clientInfo->clientRoles as $role)
            | <sup>{{$role->role_name}}</sup>
        @endforeach
    </h3>
    <h5>
        @if(session('role') == 'broker')
            <a class="btn btn-sm btn-outline-warning m-3" href="/rent/broker/clients/update/{{$clientInfo->id}}"
               target="_blank" role="button">Редактирай</a>
        @else
            Брокер:
            @foreach($clientInfo->users as $user)
                {{$user->name}} ({{$user->email}})
            @endforeach
            <a class="btn btn-sm btn-outline-warning m-3" href="/rent/admin/clients/update/{{$clientInfo->id}}"
               target="_blank" role="button">Редактирай</a>
        @endif
    </h5>
    <div class="row">
        <div class="col-sm-6">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Имена</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <th scope="row">{{$clientInfo->statuses->status_name}}</th>
                    <td>{{$clientInfo->first_name}} {{$clientInfo->middle_name}} {{$clientInfo->last_name}}</td>

                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-6">

            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Статус</th>
                    <th scope="col">Коментар</th>
                    <th scope="col">Дата</th>
                </tr>
                </thead>
                <tbody>
                @if($clientInfo->calls->isEmpty())
                    <tr>
                        <td colspan="5">Няма нито едно обаждане на този човек</td>
                    </tr>
                @else
                    @foreach($clientInfo->calls as $k => $call)
                        <tr>
                            <th scope="row">{{$k+1}}</th>
                            <th scope="row">{{$call->callStates->call_status}}</th>
                            <th scope="row">{{$call->call_comment}}</th>
                            <th scope="row">{{$call->datetime}}</th>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        @if(!$clientInfo->apartments->isEmpty())
            <div class="col-sm-6">
                <h4>С кои имоти разполага</h4>
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th scope="col">Имот ID</th>
                        <th scope="col">address</th>
                        <th scope="col">Go to apartment</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clientInfo->apartments as $k => $apartment)
                        <tr>
                            <th scope="row">{{$apartment->id}}</th>
                            <th scope="row">{{$apartment->address}}</th>
                            <td><a href="/rent/{{session('role')}}/apartment/get/{{$apartment->id}}"
                                   class="btn btn-primary btn-sm" role="button">Go to This
                                    Apartment</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
    @if($clientInfo->getclientSearchRules!=null)
        <hr>
        <h3>Какво търси?</h3>
        <div class="row">
            <div class="col-sm-6">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th scope="col" width="35%">Име на атрибут</th>
                        <th scope="col">Подробности</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Клиент ID</td>
                        <td>{{$clientInfo->getclientSearchRules->client_id}}</td>
                    </tr>
                    <tr>
                        <td>Информация</td>
                        <td>{{$clientInfo->getclientSearchRules->informacia}}</td>
                    </tr>
                    <tr>
                        <td>Описание на търсене</td>
                        <td>{{$clientInfo->getclientSearchRules->opisanie_na_tirseneto}}</td>
                    </tr>
                    <tr>
                        <td>Колко врме търси</td>
                        <td>{{$clientInfo->getclientSearchRules->kolko_vreme_tirsi}}</td>
                    </tr>
                    <tr>
                        <td>Огледа мина</td>
                        <td>{{$clientInfo->getclientSearchRules->ogleda_mina}}</td>
                    </tr>
                    <tr>
                        <td>Има Мебли/техника</td>
                        <td>{{$clientInfo->getclientSearchRules->kakvo_ima}}</td>
                    </tr>
                    <tr>
                        <td>Цена</td>
                        <td>{{$clientInfo->getclientSearchRules->price}}</td>
                    </tr>
                    <tr>
                        <td>ID апартамент</td>
                        <td>{{$clientInfo->getclientSearchRules->apartment_id}}</td>
                    </tr>
                    <tr>
                        <td>Причина за напускане</td>
                        <td>{{$clientInfo->getclientSearchRules->prichina_za_napuskane}}</td>
                    </tr>
                    <tr>
                        <td>Сфера на дейности</td>
                        <td>{{$clientInfo->getclientSearchRules->sfera_na_deynosti}}</td>
                    </tr>
                    <tr>
                        <td>Удобно време за оглед</td>
                        <td>{{$clientInfo->getclientSearchRules->udobno_vreme_za_ogled}}</td>
                    </tr>
                    <tr>
                        <td>Сайт от който се обажда</td>
                        <td>{{$clientInfo->getclientSearchRules->imot_site}}</td>
                    </tr>
                    <tr>
                    <tr>
                        <td>Район</td>
                        <td>{{$clientInfo->getclientSearchRules->region}}</td>
                    </tr>
                    <tr>
                    <tr>
                        <td>Обзавеждане</td>
                        <td>{{$clientInfo->getclientSearchRules->obzavegdane}}</td>
                    </tr>
                    <tr>
                    <tr>
                        <td>Вид имот</td>
                        <td>{{$clientInfo->getclientSearchRules->vid_imot}}</td>
                    </tr>
                    <tr>
                        <td>Работи с други агенции</td>
                        <td>{{$clientInfo->getclientSearchRules->raboti_s_drugi_agencii}}</td>
                    </tr>
                    <tr>
                        <td>Пушач</td>
                        <td>{{$clientInfo->getclientSearchRules->pushach}}</td>
                    </tr>
                    <tr>
                        <td>Може да предплати</td>
                        <td>{{$clientInfo->getclientSearchRules->moge_da_predplati}}</td>
                    </tr>
                    <tr>
                        <td>Домашен любимец</td>
                        <td>{{$clientInfo->getclientSearchRules->home_pet}}</td>
                    </tr>
                    <tr>
                        <td>Съгласен с нашите условия</td>
                        <td>{{$clientInfo->getclientSearchRules->siglasen_uslovia}}</td>
                    </tr>
                    <tr>
                        <td>Взаимоотношение</td>
                        <td>{{$clientInfo->getclientSearchRules->vzaimootnoshenia}}</td>
                    </tr>
                    <tr>
                        <td>Създаден</td>
                        <td>{{$clientInfo->getclientSearchRules->created_at}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    @if(!$clientInfo->likeApartments->isEmpty())
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-sm">
                    <thead>
                    <tr>
                        <th scope="col">ID на апартамент</th>
                        <th scope="col">Имена</th>
                        <th scope="col">Адрес</th>
                        <th scope="col">Снимки</th>
                        <th scope="col">Цена EUR</th>
                        <th scope="col">Функции</th>

                    </tr>
                    </thead>
                    @foreach($clientInfo->likeApartments as $id => $apartment)
                        <tbody>
                        <tr id="row{{$apartment->id}}"
                            @if(count($apartment->calls) != 0 && strtotime(date('Y-m-d H:i:s',strtotime("-1 days")))<strtotime($apartment->calls[0]->datetime))
                            @switch($apartment->calls[0]->call_state_id)
                            @case(1)
                            class="table-success"
                            @break
                            @case(2)
                            class="table-warning"
                            @break
                            @case(3)
                            class="table-danger"
                            @break
                            @endswitch
                            @endif
                        >
                            <td scope="row">{{$apartment->id}}</td>   <!--Apartment ID-->
                            <td scope="row">
                                @foreach($apartment->clients as $client)
                                    {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}<br>
                                    <!--Names-->
                                @endforeach
                            </td>
                            <td scope="row">{{$apartment->address}}</td>
                            <td scope="row"><img alt="Снимката липсва" width="150px"
                                                 src={{($apartment->photos->isEmpty()) ? "" : asset('storage/apartments')."/".$apartment->photos[0]->photo_path}}>
                            </td>
                            <!--Images-->
                            <td scope="row">
                                @foreach($apartment->params as $param)
                                    @if($param->apartment_params_id == 19)
                                        {{$param->parameter}}
                                    @endif
                                @endforeach
                            </td>
                            <td scope="row">

                                {{--                                Огледи--}}
                                <button type="button" class="btn btn-sm btn-success" data-toggle="modal"
                                        data-target="#ogled{{$apartment->id}}">
                                    <i class="fas fa-business-time"></i>
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="ogled{{$apartment->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="ogled{{$apartment->id}}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title"
                                                    id="ogled{{$apartment->id}}">
                                                    <h5>Време за оглед на {{$apartment->id}}</h5>
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">Оглед</label>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Email address</label>
                                                        <p><input type="date" class="form-control"
                                                                  value="{{date('Y-m-d')}}"
                                                                  id="ogledDate{{$apartment->id}}"></p>
                                                        <p><input type="time" class="form-control"
                                                                  value="{{date('H:i')}}"
                                                                  id="ogledTime{{$apartment->id}}"></p>
                                                        <small id="emailHelp" class="form-text text-muted"> Попълни дата
                                                            и време за оглед.</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Close
                                                </button>
                                                <button type="button"
                                                        onclick='ogled("ogled{{$apartment->id}}","{{$client->id}}","{{$apartment->id}}")'
                                                        class="btn btn-primary">
                                                    Save changes
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <button type="button" class="btn btn-sm btn-success" data-toggle="modal"
                                        data-target="#contact{{$apartment->id}}">
                                    <i class="fas fa-mobile-alt"></i>
                                </button>
                                <!-- Modal -->
                                <div class="modal fade" id="contact{{$apartment->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="contact{{$apartment->id}}" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title"
                                                    id="contact{{$apartment->id}}">
                                                    @foreach($apartment->clients as $client)
                                                        {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}
                                                        <br>
                                                    @endforeach
                                                    <h5>ID апартамента: {{$apartment->id}}</h5>
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Names</th>
                                                        <th scope="col">Emails</th>
                                                        <th scope="col">Phones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(count($apartment->clients) != 0)
                                                        @foreach($apartment->clients as $client)
                                                            <tr>
                                                                <td>
                                                                    {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}
                                                                </td>
                                                                <td>
                                                                    @if(count($client->emails) != 0)
                                                                        @foreach($apartment->clients as $client)
                                                                            {{$client->email}}<br>
                                                                        @endforeach
                                                                    @else
                                                                        Няма
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(count($client->phones) != 0)
                                                                        @foreach($client->phones as $phone)
                                                                            {{ $phone->phone}}<br>
                                                                        @endforeach
                                                                    @else
                                                                        Няма
                                                                    @endif
                                                                </td>
                                                                @endforeach
                                                            </tr>
                                                            @endif
                                                    </tbody>
                                                </table>
                                                <div class="form-group">
                                                    <label for="exampleFormControlSelect1">Оглед</label>
                                                    <select class="form-control" id="call_status{{$apartment->id}}">
                                                        <option selected value="">Избери</option>
                                                        @foreach($callStates as $state)
                                                            <option
                                                                value="{{$state->id}}">{{$state->call_status}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1">Коментар към
                                                        разговор</label>
                                                    <textarea class="form-control" id="call_comment{{$apartment->id}}"
                                                              rows="3"></textarea>
                                                </div>
                                                <table class="table table-sm">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">Call Статус</th>
                                                        <th scope="col">Коментар</th>
                                                        <th scope="col">Дата</th>
                                                        <th scope="col">Брокер</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($apartment->calls as $key => $call)
                                                        @if($key<5)
                                                            <tr
                                                                @switch($call->call_state_id)
                                                                @case(1)
                                                                class="table-success"
                                                                @break
                                                                @case(2)
                                                                class="table-warning"
                                                                @case(3)
                                                                class="table-danger"
                                                                @break
                                                                @default
                                                                class="table-light"
                                                                @endswitch
                                                            >
                                                                <td>{{$call->callStates->call_status}}</td>
                                                                {{--                                                <td><b>Client ID:</b>{{$call->clients->id}}<br>--}}
                                                                {{--                                                    {{$call->clients->first_name}} {{$call->clients->middle_name}} {{$call->clients->last_name}}--}}
                                                                {{--                                                </td>--}}
                                                                <td>{{$call->call_comment}}</td>
                                                                <td>{{$call->datetime}}</td>
                                                                <td>{{$call->users->name}}<br>({{$call->users->email}})
                                                                </td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    Close
                                                </button>

                                                <button type="button"
                                                        onclick='calls("contact{{$apartment->id}}","{{$client->id}}","{{$apartment->id}}","call_comment{{$apartment->id}}",)'
                                                        class="btn btn-primary">
                                                    Save changes
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <a href="/rent/{{session('role')}}/apartment/get/{{$apartment->id}}" role="button"
                                   class="btn btn-sm btn-primary"><i class="fas fa-info"></i></a>
                                <a href="/rent/{{session('role')}}/apartments/update/{{$apartment->id}}" role="button"
                                   class="btn btn-sm btn-warning"> <i class="fas fa-edit"></i> </a>
                            </td>

                        </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    @endif
@endsection
