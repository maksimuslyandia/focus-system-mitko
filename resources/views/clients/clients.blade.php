@extends('layouts.appSB')
@section('content')
    <div id="max"></div>
    {{$clientsInfo->links()}}
    <table class="table table-sm">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Имена</th>
            <th scope="col">Брокер</th>
            <th scope="col">Роля</th>

            <th scope="col">Функции</th>
        </tr>
        </thead>
        @foreach($clientsInfo as $client)

            <tbody>
            <tr id="row{{$client->id}}"
                @if(count($client->calls) != 0 && strtotime(date('Y-m-d 00:00:00'))<strtotime($client->calls[0]->datetime))
                @switch($client->calls[0]->call_state_id)
                @case(1)
                class="table-success"
                @break
                @case(2)
                class="table-warning"
                @break
                @case(3)
                class="table-danger"
                @break
                @endswitch
                @endif
            >
                <td scope="row">{{$client->id}}</td>
                <td scope="row">
                    {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}<br> <!--Names-->
                </td>
                <td>
                    @foreach($client->users as $user)
                        {{$user->name}} ({{$user->email}})<br>
                    @endforeach
                </td>
                <td>
                    @foreach($client->clientRoles as $role)
                        {{$role->role_name}}
                    @endforeach
                </td>
                <td scope="row">
                    <a href="/rent/{{session('role')}}/clients/get/{{$client->id}}" role="button"
                       class="btn btn-sm btn-primary">Информация</a>
                    <a href="/rent/{{session('role')}}/clients/update/{{$client->id}}" role="button"
                       class="btn btn-sm btn-primary">Редакция</a>
                    <button type="button" class="btn btn-sm btn-primary" data-toggle="modal"
                            data-target="#contact{{$client->id}}">Контакт
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="contact{{$client->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="contact{{$client->id}}" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="contact{{$client->id}}"> {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table>
                                        <thead>
                                        <tr>
                                            <th scope="col">Emails</th>
                                            <th scope="col">Phones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                @if(count($client->emails) != 0)
                                                    @foreach($client->emails as $email)
                                                        {{$email->email}}<br>
                                                    @endforeach
                                                @else
                                                    Няма
                                                @endif
                                            </td>
                                            <td>
                                                @if(count($client->phones) != 0)
                                                    @foreach($client->phones as $phone)
                                                        {{ $phone->phone}}<br>
                                                    @endforeach
                                                @else
                                                    Няма
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Оглед</label>
                                        <select class="form-control" id="exampleFormControlSelect1">
                                            <option selected value="">Избери</option>
                                            @foreach($callStates as $state)
                                                <option value="{{$state->id}}">{{$state->call_status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Коментар към разговор</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1"
                                                  rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <a type="button" class="btn btn-primary">Save changes</a>
                                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
        @endforeach
    </table>

    <!-- Modal -->

@endsection
