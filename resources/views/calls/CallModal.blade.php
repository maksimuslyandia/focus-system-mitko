{{--@dd($apartment)--}}

<script>
    function calls(clienId, idApartment) {
        var call_status = $('#call_status' + idApartment).val();
        call_comment = $('#call_comment').val();
        alert(call_comment);
        $.ajax({
            beforeSend: function () {
                $("#p_prldr").show().fadeIn('slow');
            },
            complete: function () {
                $('#p_prldr').hide().fadeOut('slow');
            },
            method: "POST",
            url: "/updatecall",
            type: "POST",
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "call_state_id": call_status,
                "call_comment": 'През сис.задача' + call_comment,
                "client_id": clienId,
                "apartment_id": idApartment,
            },
            success: function (response) {
                $("#max").append(response); //дебагер ответа dd();
            }
        })
            .done(function (msg) {
                switch (call_status) {
                    case "1":
                        $("#row" + idApartment).attr('class', 'table-success');
                        break;
                    case "2":
                        $("#row" + idApartment).attr('class', 'table-warning');
                        break;
                    default:
                        $("#row" + idApartment).attr('class', 'table-danger');
                        break;
                }
                $('#contactModal').modal('hide');
                $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                    $("#success-alert").slideUp(500);
                });
            });
    }

    function showPhone(e, phone, idClient, idApartment) {
        $('.phone-button').html('покажи');
        $(e).html(phone);
        $.get("/rent/phone/get", {idClient: idClient, idApartment: idApartment}) //получили основные данные по таблице
            .done(function (data) {
                $("#comment_blok").html(data); //сунули полученый контент
                $("#p_prldr").hide().fadeOut('slow'); //вырубили прелоадер
            });
    }

</script>

<div class="modal-header">
    <h5 class="modal-title" id="contact{{$apartment->id}}">
        <h5>ID апартамента: {{$apartment->id}}</h5>
    </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">

    <table class="table table-sm">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Имена</th>
            <th scope="col">Телефони</th>
        </tr>
        </thead>
        <tbody>
        @foreach($apartment->clients as $client)
            <tr>
                <th scope="row"></th>
                <td>{{$client->first_name}} {{$client->middle_name}} {{$client->last_name}} </td>
                <td>
                    @foreach($client->phones as $phone)
                        <button type="button"
                                onclick="showPhone(this,'{{$phone->phone}}','{{$client->id}}','{{$apartment->id}}')"
                                class="btn m-2 phone-button btn-outline-success">
                            Покажи
                        </button>
                        <br>
                    @endforeach
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    <div id="comment_blok"></div>
    <div id="callHistory">
        <table class="table table-sm">
            <thead>
            <tr>
                <th scope="col">Call Статус</th>
                <th scope="col">Коментар</th>
                <th scope="col">Дата</th>
                <th scope="col">Брокер</th>
            </tr>
            </thead>
            <tbody>
            @foreach($apartment->calls as $key => $call)
                @if($key<5)
                    <tr
                        @switch($call->call_state_id)
                        @case(1)
                        class="table-success"
                        @break
                        @case(2)
                        class="table-warning"
                        @case(3)
                        class="table-danger"
                        @break
                        @default
                        class="table-light"
                        @endswitch
                    >
                        <td>{{$call->callStates->call_status}}</td>
                        <td>{{$call->call_comment}}</td>
                        <td>{{$call->datetime}}</td>
                        <td>{{$call->users->name}}<br>({{$call->users->email}})</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>

    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
    </button>
    <div id="saveCall">

    </div>
</div>
