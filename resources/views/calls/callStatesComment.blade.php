<div class="form-group">
    <label for="exampleFormControlSelect1">Оглед</label>
    <select class="form-control" id="call_status">
        <option selected value="">Избери</option>
        @foreach($callStates as $state)
            <option value="{{$state->id}}">{{$state->call_status}}</option>
        @endforeach
    </select>
</div>


<div class="form-group">
    <label for="call_comment">Пиши коментар за контакт със 885010146</label>
    <textarea class="form-control" id="call_comment" rows="3"></textarea>
</div>
