{{--@extends('layouts.appSB')--}}
{{--@section('content')--}}
{{--    <div class="container-fluid">--}}
{{--        <h3>Show a file-select field which allows multiple files:</h3>--}}
{{--        <form name="max" action="/uploadimage"  method="post" enctype="multipart/form-data">--}}
{{--            <label for="myfile">Select files:</label>--}}
{{--            <input type="file" id="myfile" name="images[]" multiple><br><br>--}}
{{--            <input type="submit">--}}
{{--        </form>--}}
{{--    </div>--}}
{{--    --}}
{{--    --}}
{{--    --}}
{{--@endsection--}}


<html lang="en" class="">
<head>
    <meta charset="UTF-8">
    <title>Laravel Multiple File Upload Tutorial Example From Scratch</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
</head>
<body>
<div class="container">
    <div class="row justify-content-center">
        <div class="card">
            <div class="card-header">Laravel Upload File Example</div>
            <div class="card-body">
                <form action="/save" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <input type="file" class="form-control-file" name="photo[]" id="file" multiple="">
{{--                        <img src="{{asset('storage/1587901941_0.jpg')}}" alt="..." class="img-thumbnail">--}}
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</html>
