<h4>Огледи</h4>
<table id="representsTable" class="table table-sm table-hover">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Адрес</th>
        <th scope="col">Кога</th>
    </tr>
    </thead>
    <tbody>
    @foreach($myRepresents as $k => $represent)
        <tr>
            <th scope="row">{{$represent->id}}</th>
            <td>{{$represent->apartment->address}}</td>
            <td>{{$represent->datetime}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
