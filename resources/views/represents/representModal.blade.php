<div class="form-group">
    <label for="representStateId">Example multiple select</label>
    <select name="{{$idRepresent}}" class="form-control" id="representStateId">
        <option value="">Избери</option>
        @foreach($representStates as $state)
            <option value="{{$state->id}}">{{$state->represent_name}}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="representComment">Дай коментар за оглед</label>
    <textarea class="form-control" id="representComment" rows="3"></textarea>
</div>
