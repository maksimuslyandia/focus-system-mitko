@extends('layouts.appSB')
@section('content')
    <script>
        //получение контента апартаментов
        $(function () {
            $("#success-alert").hide();
            $("#p_prldr").hide().fadeOut('slow');
            $("#brokersList").change(function () {
                $("#p_prldr").show().fadeIn('slow');
                let idBroker = parseInt($("#brokersList").val());
                $.get("/rent/admin/newsystemtask/get/apartments", {idBroker: idBroker}) //получили основные данные по таблице
                    .done(function (data) {
                        // $("#max").append(data); //дебагер ответа dd();
                        $("#apartments").html(data); //сунули полученый контент
                        $("#p_prldr").hide().fadeOut('slow'); //вырубили прелоадер
                    });
            });
        });
        //photo galary
        // $(document).on('click', '#photo', function (event) {
        //     var page = $(this).attr('name');
        //     alert('photo');
        //     $.get("/rent/admin/newsystemtask/get/apartment/photos/13791") //получили основные данные по таблице
        //         .done(function (data) {
        //             $("#max").html(data); //сунули полученый контент
        //             $("#p_prldr").hide().fadeOut('slow'); //вырубили прелоадер
        //         });
        // });

        /***Ajax pagination***/
        $(document).on('click', '.pagination a', function (event) {
            $("#p_prldr").show().fadeIn('slow');
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            fetch_data(page);
        });

        function fetch_data(page) {
            let idBroker = parseInt($("#brokersList").val());
            $.get("/rent/admin/newsystemtask/get/apartments?page=" + page, {idBroker: idBroker}) //получили основные данные по таблице
                .done(function (data) {
                    // $("#max").append(data); //дебагер ответа dd();
                    $("#apartments").html(data); //сунули полученый контент
                    $("#p_prldr").hide().fadeOut('slow'); //вырубили прелоадер
                });
        }

        /***END Ajax pagination***/


    </script>
    <div id="max"></div>
    {{--@dd($users)--}}
    <div class="row">
        <div class="col-sm-4">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="brokersList">Options</label>
                </div>
                <select class="custom-select" id="brokersList">
                    <option selected>Choose...</option>
                    @foreach($users as $user)
                        <option value="{{$user->id}}">{{$user->name}} ({{$user->email}})</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="apartments" class="col-sm-12">
        </div>
    </div>

@endsection
