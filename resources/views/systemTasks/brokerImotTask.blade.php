@extends('layouts.appSB')
@section('content')
    <script>
        function calls(idModal, clienId, idApartment, call_comment) {

            var call_status = $('#call_status' + idApartment).val();
            call_comment = $('#' + call_comment).val();
            if (call_comment !== '' || call_status !== '') {

                $.ajax({
                    beforeSend: function () {
                        $('#p_prldr').show().fadeIn;

                    },
                    complete: function () {
                    },
                    method: "POST",
                    url: "/updatecallsystask",
                    type: "POST",
                    data: {
                        "_token": $('meta[name="csrf-token"]').attr('content'),
                        "call_state_id": call_status,
                        "call_comment": 'През системна задача ' + call_comment,
                        "client_id": clienId,
                        "apartment_id": idApartment,
                    },
                    success: function (response) {
                        $("#max").append(response); //дебагер ответа dd();
                    }
                })
                    .done(function (msg) {
                        switch (call_status) {
                            case "1":
                                $("#row" + idApartment).attr('class', 'table-success');
                                break;
                            case "2":
                                $("#row" + idApartment).attr('class', 'table-warning');
                                break;
                            default:
                                $("#row" + idApartment).attr('class', 'table-danger');
                                break;
                        }
                        $("#max").append(msg); //дебагер ответа dd();
                        $('#' + idModal).modal('hide');
                        $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                            $("#success-alert").slideUp(500);
                        });
                    });
                setTimeout(function(){ location.reload(); }, 1000);
                $('#p_prldr').hide().fadeOut;

            } else {
                alert('Пълни данни')
            }
        }
    </script>
    <div id="max"></div>
    {{$apartments->links()}}
    <table class="table table-sm">
        <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">Address</th>
            <th scope="col">Функции</th>

        </tr>
        </thead>
        <tbody>
        @foreach($apartments as $apartment)
            <tr>
                <th scope="row">{{$apartment->id}}</th>
                <td>{{$apartment->address}}</td>
                <td>
                </td>
                <td>
                                        <a href="/rent/{{session('role')}}/apartments/get/{{$apartment->id}}" role="button"
                                           class="btn btn-sm btn-primary"><i class="fas fa-info"></i></a>
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal"
                            data-target="#contact{{$apartment->id}}">
                        <i class="fas fa-mobile-alt"></i>
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="contact{{$apartment->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="contact{{$apartment->id}}" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="contact{{$apartment->id}}">
                                        @foreach($apartment->clients as $client)
                                            {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}<br>
                                        @endforeach
                                        <h5>ID апартамента: {{$apartment->id}}</h5>
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table>
                                        <thead>
                                        <tr>
                                            <th scope="col">Names</th>
                                            <th scope="col">Emails</th>
                                            <th scope="col">Phones</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($apartment->clients) != 0)
                                            @foreach($apartment->clients as $client)
                                                <tr>
                                                    <td>
                                                        {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}
                                                    </td>
                                                    <td>
                                                        @if(count($client->emails) != 0)
                                                            @foreach($apartment->clients as $client)
                                                                {{$client->email}}<br>
                                                            @endforeach
                                                        @else
                                                            Няма
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(count($client->phones) != 0)
                                                            @foreach($client->phones as $phone)
                                                                {{ $phone->phone}}<br>
                                                            @endforeach
                                                        @else
                                                            Няма
                                                        @endif
                                                    </td>
                                                    @endforeach
                                                </tr>
                                                @endif
                                        </tbody>
                                    </table>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Оглед</label>
                                        <select class="form-control" id="call_status{{$apartment->id}}">
                                            <option selected value="">Избери</option>
                                            @foreach($callStates as $state)
                                                <option value="{{$state->id}}">{{$state->call_status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Коментар към разговор</label>
                                        <textarea class="form-control" id="call_comment{{$apartment->id}}"
                                                  rows="3"></textarea>
                                    </div>
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">Call Статус</th>
                                            <th scope="col">Коментар</th>
                                            <th scope="col">Дата</th>
                                            <th scope="col">Брокер</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($apartment->calls as $key => $call)
                                            @if($key<5)
                                                <tr
                                                    @switch($call->call_state_id)
                                                    @case(1)
                                                    class="table-success"
                                                    @break
                                                    @case(2)
                                                    class="table-warning"
                                                    @case(3)
                                                    class="table-danger"
                                                    @break
                                                    @default
                                                    class="table-light"
                                                    @endswitch
                                                >

                                                    <td>{{$call->callStates->call_status}}</td>
                                                    {{--                                                <td><b>Client ID:</b>{{$call->clients->id}}<br>--}}
                                                    {{--                                                    {{$call->clients->first_name}} {{$call->clients->middle_name}} {{$call->clients->last_name}}--}}
                                                    {{--                                                </td>--}}
                                                    <td>{{$call->call_comment}}</td>
                                                    <td>{{$call->datetime}}</td>
                                                    <td>{{$call->users->name}}<br>({{$call->users->email}})</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                    </button>

                                    <button type="button"
                                            onclick='calls("contact{{$apartment->id}}","{{$client->id}}","{{$apartment->id}}","call_comment{{$apartment->id}}",)'
                                            class="btn btn-primary">
                                        Save changes
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{$apartments->links()}}





@endsection
