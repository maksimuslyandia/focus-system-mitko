<script>
    $(function () {
        $("#success-alert").hide();
    });

    function checkPhone() {
        let phone = $('#phone').val();
        if(phone[0]==='0' ){
            phone= phone.slice(1);
        }else if(phone.includes('+359')){
            phone= phone.slice(4);
        }

        $.ajax({
            method: "POST",
            url: "/checkphone",
            type: "POST",
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "phone": phone,
            },
            success: function (response) {
                // $("#max").append(response); //дебагер ответа dd();
            }
        })
            .done(function (msg) {
                if (Array.isArray(msg)) {
                    msg.forEach((item, index) => {
                        item['get_phone_client'].forEach((subitem, subindex) => {
                            $("#phoneInfo").html('<a href="/rent/{{session('role')}}/clients/get/'+subitem['id']+'" class="btn btn-primary btn-sm" role="button">Go to This Client</a>');
                        });
                    });
                }else {
                    $("#phoneInfo").html("<h2>Няма номер в система</h2>");
                }
            });
    }
</script>
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/home') }}">
            {{--            {{ config('app.name', 'Laravel') }}--}}
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>
            <div id="phoneContent" class="row">
                <div class="input-group mb-3">
                    <input type="text" id="phone" class="form-control"
                           aria-label="Recipient's username" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button type="submit" onclick="checkPhone()" class="btn btn-outline-secondary"
                                data-toggle="modal" data-target="#exampleModal">
                            Провери телефон
                        </button>
                    </div>
                </div>
            </div>


            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Инфо за този номер</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="phoneInfo"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="" data-dismiss="modal">Close
                            </button>
                            {{--                            <button type="button" class="btn btn-primary">Save changes</button>--}}
                        </div>
                    </div>
                </div>
            </div>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
