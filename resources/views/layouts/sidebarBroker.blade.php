<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/home">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Broker <sup>2</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="/home">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Клиенти
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
{{--    <li class="nav-item">--}}
{{--        <a class="nav-link" href="/home">--}}
{{--            <i class="fas fa-fw fa-chart-area"></i>--}}
{{--            <span>Моите задачи</span></a>--}}
{{--    </li>--}}
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
           aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Създай нов</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                {{--                <h6 class="collapse-header">Custom Components:</h6>--}}
                <a class="collapse-item" href="/rent/broker/newrequest">Заявка</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
           aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Информация</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
{{--                <h6 class="collapse-header text-success">Клиенти:</h6>--}}
{{--                <a class="collapse-item" href="/rent/broker/clients/all/get">Всички</a>--}}
{{--                <a class="collapse-item" href="/rent/broker/clients/active/get">Активни Клиенти</a>--}}
{{--                <a class="collapse-item" href="/rent/broker/clients/passive/get">Не активни</a>--}}
                @if(session('sub_role') != null)
                    <a class="collapse-item" href="/rent/broker/myclients/get">Купувачи</a>
                @else
                    <a class="collapse-item" href="/rent/broker/myclients/get">Наематели</a>
                @endif
{{--                <h6 class="collapse-header text-success">Имоти:</h6>--}}
{{--                <a class="collapse-item" href="/ren t/broker/apartments/all/get">Всички</a>--}}
                <a class="collapse-item" href="/rent/broker/apartments/get">Имоти</a>
                <a class="collapse-item" href="/rent/broker/systemtasks/apartments/get">Системни задачи</a>
{{--                <a class="collapse-item" href="/rent/broker/apartments/passive/get">Не активни </a>--}}
                <a class="collapse-item" href="/rent/broker/requests/get">Заявки</a>
            </div>Ф
        </div>
    </li>


    <!-- Divider -->


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
