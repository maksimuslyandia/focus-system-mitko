@extends('layouts.appSB')
@section('content')
    <button class="btn btn-primary" name="ownerExists"> Съществуващ собственик</button>
    <button class="btn btn-success" name="ownerDoesntExists" disabled> Несъществуващ собственик</button>
    <script src="{{ asset('lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script src="{{ asset('lib/bootstrap-filestyle/bootstrap-filestyle.min.js') }}" defer></script>
    <form action="{{URL::to('/rent/admin/apartments/create')}}" method="post" enctype="multipart/form-data">
        @csrf
        <h3 class="mt-5">За собственика</h3>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Статус на Собственикa</span>
                </div>
                <!-- <label for="client_status">Статус на Собственикът</label> -->
                <select name="client_status" class="custom-select mr-sm-2" required>
                    <option value="">Избери</option>
                    @foreach($statuses as $status)
                        <option value="{{$status->id}}">{{$status->status_name}}</option>
                    @endforeach

                </select>
            </div>
            <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Роля на собственика</span>
                </div>
                <!-- <label for="client_roles">Роля на собственикът</label> -->
                <select name="client_roles" class="custom-select mr-sm-2" id="client_roles" required>
                    <option value="">Избери</option>
                    @foreach($clientRoles as $key =>  $role)
                        @if($key == 0 || $key == 3)
                            else
                            <option value="{{$role->id}}">{{$role->role_name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Име</span>
                </div>
                <!-- <label for="client[]">Име</label> -->
                <input value="" required name="client[]"
                       type="text"
                       class="form-control" id="first_name"
                       placeholder="Въведи Име">
            </div>
            <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Фамилия</span>
                </div>
                <!-- <label for="client[]">Фамилия</label> -->
                <input value="" name="client[]" type="text"
                       class="form-control" id="last_name"
                       placeholder="Въведи Име">
            </div>
        </div>
        {{--            todo добавить возможность добавлени инфо--}}
        <div class="row mt-3">
            <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Телефон</span>
                </div>
                <!-- <label for="phone">Телефон</label> -->
                <input value="" required name="phone" type="text"
                       class="form-control" id="phone" placeholder="Телефон">
            </div>
            <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Email</span>
                </div>
                <!-- <label for="email"> Email </label> -->
                <input value="" name="email" type="text"
                       class="form-control"
                       id="email" placeholder=" Въведи Email">
            </div>
            <div class="col-sm-6 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">ID На Клиент</span>
                </div>
                <!-- <label for="clientID"> ID На Клиент </label> -->
                <input disabled value="" required name="clientID" type="number"
                       class="form-control"
                       placeholder="Въведи ID на клиент">
            </div>
        </div>

        <h3 class="mt-5">За Имот</h3>
        <hr>
        <div class="row mt-4">
            <div class="col-sm-6 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Дата на офертата</span>
                </div>
                <!-- <label for="offer_date">Дата на офертата</label> -->
                <input type="text" class="form-control" placeholder="Дата" name="offer_date" id="offer_date">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>

            <div class="col-sm-6 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Взета от сайт</span>
                </div>
                <!-- <label for="offer_website">Взета от сайт</label> -->
                <input type="text" class="form-control" placeholder="Дата" name="offer_website">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-sm-2" @if(session('role')=='broker') style="display: none;"@endif>
                    <div class="custom-control custom-switch mt-3" style="">
                        <input type="hidden" name="is_locked" value="0"/>
                        <input type="checkbox" class="custom-control-input" id="is_locked" name="is_locked" value="1" />
                        <label style=" onColor: warning;"  class="custom-control-label" for="is_locked"><i style=" color: orange;" class="fas fa-lg fa-lock"></i> Заключен апартамент </label>
                    </div>
            </div>
            <div class="col-sm-4" @if(session('role')=='broker') style="display: none;"@endif>
                <div class="input-group-prepend">
                    <span class="input-group-text">Избери брокер, който е заключил апартамента</span>
                </div>
                <select class="custom-select mr-sm-2" id="locked_broker" name="locked_broker" disabled>
                    <option value="0">Не е избран</option>
                    @foreach($brokers as $broker)
                            <option value="{{$broker->id}}">{{$broker->username}} {{$broker->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-sm-2" @if(session('role')=='broker') style="display: none;"@endif>
                <div class="custom-control custom-switch mt-3" style="">
                    <input type="hidden" name="has_exclusive_contract" value="0"/>
                    <input type="checkbox" class="custom-control-input" id="has_exclusive_contract" name="has_exclusive_contract" value="1"/>
                    <label style=" onColor: warning;"  class="custom-control-label" for="has_exclusive_contract"><i style=" color: orange;" class="fas fa-lg fa-star"></i> Ексклузивен договор</label>
                </div>
            </div>
            <div class="col-sm-4" @if(session('role')=='broker') style="display: none;"@endif>
                <div class="input-group-prepend">
                        <span class="input-group-text">Избери брокер, който е взел ексклузивен договор</span>
                </div>
                <select name="exclusive_contract_broker" class="custom-select mr-sm-2" id="exclusive_contract_broker" disabled>
                    <option value="0">Не е избран</option>
                    @foreach($brokers as $broker)
                        <option value="{{$broker->id}}">{{$broker->username}} {{$broker->name}}</option>
                    @endforeach
                </select>
            </div>  
            <div class="col-sm-2 mb-3" >
                <div class="custom-control custom-switch" style="padding-top:13%;margin-left:60%;">
                    <input type="hidden" name="has_key" value="0" />
                    <input name="has_key" type="checkbox" class="custom-control-input" id="has_key" value="1">
                    <label style=" onColor: warning;"  class="custom-control-label" for="has_key"><i style=" color: orange;" class="fas fa-key fa-lg" ></i>Ключ</label>
                </div>
            </div>
            
            <div class="col-sm-4">
                <div class="input-group-prepend">
                    <span class="input-group-text">Избери брокер, който е взел ключа</span>
                </div>
                <!-- <label for="client_roles">Избери брокер, който е взел ключа</label> -->
                <select name="key_broker" class="custom-select mr-sm-2" id="key_broker" disabled>
                    <option value="false">Не е избран</option>
                    @foreach($brokers as $broker)
                            <option value="{{$broker->id}}">{{$broker->username}} {{$broker->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="row mt-3">
            <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Статус на имота</span>
                </div>
                <!-- <label for="apartment_status">Статус на имота</label> -->
                <select name="apartment_status" class="custom-select mr-sm-2" id="apartment_status" required>
                    <option value="">Избери</option>
                    @foreach($statuses as $status)
                        <option value="{{$status->id}}">{{$status->status_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Роля на имота</span>
                </div>
                <!-- <label for="apartment_role">Роля на имота</label> -->
                <select name="apartment_role" class="custom-select mr-sm-2" id="apartment_role" required>
                    <option value="">Choose...</option>
                    @foreach($apartmentRoles as $role)
                        <option value="{{$role->id}}">{{$role->role_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="mt-3 row">
            <div class="col-sm-12">
                <div class="input-group-prepend">
                    <span class="input-group-text">Адрес</span>
                </div>
                <!-- <label for="address">Адрес</label> -->
                <input required value="" name="address" type="text" class="form-control" placeholder="Въведи Адрес" id="address">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Отопление</span>
                </div>
                <!-- <label for="otoplenie">Отопление</label> -->
                <select name="otoplenie" class="custom-select mr-sm-2">
                    <option value="">Избери</option>
                    <option value="Топлофикация">Топлофикация</option>
                    <option value="Ток">Ток</option>
                    <option value="Газификация">Газификация</option>
                    <option value="Локално">Локално</option>
                    <option value="Няма">Няма</option>
                </select>
            </div>
         
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Район</span>
                </div>
                <!-- <label for="region">Район</label> -->
                <select name="region" class="custom-select mr-sm-2">
                    <option value="">Избери</option>
                    <option value="Гагарин">Гагарин</option>
                    <option value="Западен">Западен</option>
                    <option value="Захарна фабрика П">Захарна фабрика П</option>
                    <option value="Изгрев П">Изгрев П</option>
                    <option value="Индустриална зона - изток">Индустриална зона - изток</option>
                    <option value="Индустриална зона - север">Индустриална зона - север</option>
                    <option value="Индустриална зона - Тракия">Индустриална зона - Тракия</option>
                    <option value="Институт по овощарство">Институт по овощарство</option>
                    <option value="Каменица 1">Каменица 1</option>
                    <option value="Каменица 2">Каменица 2</option>
                    <option value="Коматево">Коматево</option>
                    <option value="Коматевски възел">Коматевски възел</option>
                    <option value="Кършияка">Кършияка</option>
                    <option value="Кючук Париж">Кючук Париж</option>
                    <option value="Мараша">Мараша</option>
                    <option value="Марица Север">Марица Север</option>
                    <option value="Младежки хълм">Младежки хълм</option>
                    <option value="Остромила">Остромила</option>
                    <option value="Прослав">Прослав</option>
                    <option value="Саут Сити">Саут Сити</option>
                    <option value="Старият град">Старият град</option>
                    <option value="Столипиново">Столипиново</option>
                    <option value="Сточна гара П">Сточна гара П</option>
                    <option value="Съдийски">Съдийски</option>
                    <option value="Тракия">Тракия</option>
                    <option value="Филипово">Филипово</option>
                    <option value="Христо Ботев">Христо Ботев</option>
                    <option value="Христо Смирненски">Христо Смирненски</option>
                    <option value="Централна гара П">Централна гара П</option>
                    <option value="Център П">Център П</option>
                    <option value="Шекер махала">Шекер махала</option>
                    <option value="Южен">Южен</option>
                    <option value="Въстанически">Въстанически</option>
                    <option value="Индустриална зона - запад">Индустриална зона - запад</option>
                    <option value="Индустриална зона - юг">Индустриална зона - юг</option>
                    <option value="Гаганица">Гаганица</option>
                    <option value="Беломорски">Беломорски</option>
                </select>
            </div>
          
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Изложение</span>
                </div>
                <!-- <label for="izlogenie">Изложение</label> -->
                <select name="izlogenie[]" class="custom-select mr-sm-2" multiple>
                    <option value="">Избери</option>
                    <option value="izlogenie_iztok">Изток</option>
                    <option value="izlogenie_zapad ">Запад </option>
                    <option value="izlogenie_sever">Север</option>
                    <option value="izlogenie_ug">Юг</option>
                </select>
            </div>
          
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Вид строителство</span>
                </div>
                <!-- <label for="vid_stroitelstvo">Вид строителство</label> -->
                <select name="vid_stroitelstvo" class="custom-select mr-sm-2">
                    <option value="">Избери</option>
                    <option value="ЕПК">ЕПК</option>
                    <option value="Тухла">Тухла</option>
                    <option value="Ново">Ново</option>
                    <option value="Панел">Панел</option>
                    <option value="Гредоред">Гредоред</option>
                    <option value="Неизвестно">Неизвестно</option>
                </select>
            </div>
         
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Обзавеждане</span>
                </div>
                <!-- <label for="obzavegdane">Обзавеждане</label> -->
                <select name="obzavegdane" class="custom-select mr-sm-2">
                    <option value="">Избери</option>
                    <option value="Обзаведен">Обзаведен</option>
                    <option value="Необзаведен">Необзаведен</option>
                    <option value="Полуобзаведен">Полуобзаведен</option>
                    <option value="Може обзаведен">Може обзаведен</option>
                    <option value="Може необзаведен">Може необзаведен</option>
                </select>
            </div>
         
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Степен на завършеност</span>
                </div>
                <!-- <label for="finished_state">Степен на завършеност</label> -->
                <select name="finished_state" class="custom-select mr-sm-2">
                    <option value="13">Пред акт 14</option>
                    <option value="14">Акт 14</option>
                    <option value="15">Акт 15</option>
                    <option value="16">Акт 16</option>
                </select>
            </div>
          
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Вид имот</span>
                </div>
                <!-- <label for="vid_imot">Вид имот</label> -->
                <select name="vid_imot" class="custom-select mr-sm-2">
                    <option value="">Избери</option>
                    <option value="Мезонет">Мезонет</option>
                    <option value="Къща">Къща</option>
                    <option value="Едностаен">Едностаен</option>
                    <option value="Двустаен">Двустаен</option>
                    <option value="Тристаен">Тристаен</option>
                    <option value="Четиристайни">Четиристайни</option>
                    <option value="Сграда">Сграда</option>
                    <option value="Многостаен">Многостаен</option>
                    <option value="стая">стая</option>
                    <option value="офис">офис</option>
                    <option value="магазин">магазин</option>
                    <option value="Заведение">Заведение</option>
                    <option value="Гараж">Гараж</option>
                    <option value="Ателие">Ателие</option>
                    <option value="склад">склад</option>
                    <option value="хотел">хотел</option>
                    <option value="пром. имот">пром. имот</option>
                    <option value="паркомясто">паркомясто</option>
                    <option value="етаж къща">етаж къща</option>
                    <option value="Студио">Студио</option>
                    <option value="Парцел">Парцел</option>
                    <option value="Сграда">Сграда</option>                
                
		</select>
            </div>
          
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Преход</span>
                </div>
                <!-- <label for="prehod">Преход</label> -->
                <select name="prehod" class="custom-select mr-sm-2">
                    <option value="">Избери</option>
                    <option value="3">Не се знае</option>
                    <option value="2">Няма</option>
                    <option value="1">Има</option>
                </select>
            </div>
         
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Изолация</span>
                </div>
                <!-- <label for="izolatsia">Изолация</label> -->
                <select name="izolatsia" class="custom-select mr-sm-2">
                    <option value="">Избери</option>
                    <option value="1">да</option>
                    <option value="2">Не</option>
                </select>
            </div>
         
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Асансьор</span>
                </div>
                <!-- <label for="elevator">Асансьор</label> -->
                <select name="elevator" class="custom-select mr-sm-2">
                    <option value="">Избери</option>
                    <option value="1 ">Има </option>
                    <option value="2">Няма</option>
                </select>
            </div>
          
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Комисионна</span>
                </div>
                <!-- <label for="komisionna">Комисионна</label> -->
                <select name="komisionna" class="custom-select mr-sm-2">
                    <option value="">Избери</option>
                    <option value="1">Да</option>
                    <option value="2">Не</option>
                </select>
            </div>
          
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Етаж</span>
                </div>
                <!-- <label for="etag">Етаж</label> -->
                <input value="" name="etag" type="number" class="form-control" placeholder="">
            </div>
         
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Общо етажа</span>
                </div>
                <!-- <label for="obsho_etaga">Общо етажа</label> -->
                <input value="" name="obsho_etaga" type="number" class="form-control" placeholder="">
            </div>
         
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Коментар за етажа</span>
                </div>
                <!-- <label for="komentar_za_etaga">Коментар за етажа</label> -->
                <textarea value="" name="komentar_za_etaga" type="text" class="form-control" placeholder=""></textarea>
            </div>
          
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Ориентир за адреса</span>
                </div>
                <!-- <label for="orientir_za_address">Ориентир за адреса</label> -->
                <textarea value="" name="orientir_za_address" type="text" class="form-control" placeholder=""></textarea>
            </div>
            
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Описание на обзавеждането</span>
                </div>
                <!-- <label for="opisanie_na_obzavegdane">Описание на обзавеждането</label> -->
                <textarea value="" name="opisanie_na_obzavegdane" type="text" class="form-control" placeholder=""></textarea>
            </div>
            
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Описание на настилките</span>
                </div>
                <!-- <label for="opisanie_za_nastilki">Описание на настилките</label> -->
                <textarea value="" name="opisanie_za_nastilki" type="text" class="form-control" placeholder=""></textarea>
            </div>
            
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Квадратура РЗП</span>
                </div>
                <!-- <label for="kvadratura_zp">Квадратура РЗП</label> -->
                <input value="" name="kvadratura_rzp" type="number" class="form-control" placeholder="">
            </div>
            
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Квадратура ЗП</span>
                </div>
                <!-- <label for="kvadratura_rzp">Квадратура ЗП</label> -->
                <input value="" name="kvadratura_zp" type="number" class="form-control" placeholder="">
            </div>
            
            @if(session('sub_role') !== null || session('sub_role') === "multi_admin")
                <div class="col-sm-4 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Година на строеж</span>
                    </div>
                    <!-- <label for="year_build">Година на строеж</label> -->
                    <input value="" name="year_build" type="number" class="form-control" placeholder="">
                </div>
            @endif
            
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Цена EUR</span>
                </div>
                <!-- <label for="cena_eur">Цена EUR</label> -->
                <input value="" name="cena_eur" type="number" class="form-control" placeholder="">
            </div>
            @if(session('sub_role') === null || session('sub_role') === "multi_admin")
                <div class="col-sm-4 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Коментар по цената в левове</span>
                    </div>
                    <!-- <label for="price_comment">Коментар по цената в левове</label> -->
                    <textarea name="price_comment" type="text" class="form-control"></textarea>
                </div>
            @endif 

            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Комисионна забележка</span>
                </div>
                <!-- <label for="komesionna_zabelegka">Комисионна забележка</label> -->
                <textarea value="" name="komesionna_zabelegka" type="text" class="form-control" placeholder=""></textarea>
            </div>
           
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Брокерска бележка</span>
                </div>
                <!-- <label for="brokerska_belegka">Брокерска бележка</label> -->
                <textarea value="" name="brokerska_belegka" type="text" class="form-control" placeholder=""></textarea>
            </div>

            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Разпределение</span>
                </div>
                <!-- <label for="razpredelenie">Разпределение</label> -->
                <textarea value="" name="razpredelenie" type="text" class="form-control" placeholder=""></textarea>
            </div>
        </div>
{{--        <div class="row">--}}
{{--            @foreach($apartmentParams as $param)--}}
{{--                @if($param->param_type == "select")--}}
{{--                    <div class="col-sm-4 mt-2">--}}
{{--                        <label for="parameters[]">{{$param->param_name}}</label>--}}
{{--                        <select name="parameters[]" class="custom-select mr-sm-2">--}}
{{--                            <option value="">Избери</option>--}}
{{--                            @foreach($param->subApartmentParams as $key => $subParam)--}}
{{--                                <option value="{{$subParam->sub_param_name}}">{{$subParam->sub_param_name}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                @elseif($param->param_type == "number")--}}
{{--                    <div class="col-sm-4 mt-2">--}}
{{--                        <label for="parameters[]">{{$param->param_name}}</label>--}}
{{--                        <input value="" name="parameters[]" type="{{$param->param_type}}"--}}
{{--                               class="form-control"--}}
{{--                               placeholder="">--}}
{{--                    </div>--}}
{{--                @elseif($param->param_type == "text")--}}
{{--                    <div class="col-sm-4 mt-2">--}}
{{--                        <label for="parameters[]">{{$param->param_name}}</label>--}}
{{--                        <textarea value="" name="parameters[]" type="{{$param->param_type}}"--}}
{{--                                  class="form-control"--}}
{{--                                  placeholder=""></textarea>--}}
{{--                    </div>--}}
{{--                @endif--}}
{{--            @endforeach--}}
{{--        </div>--}}
            <div class="row">
                <div class="col-sm-6 mt-3">
                    <div class="form-group"> 
                        <input type="file" data-badge="true" data-btnClass="btn-info" class="filestyle" name="photo[]" id="file" multiple>
                    </div>
                </div>
            </div>
        <button class="btn btn-primary m-4" type="submit"> Създай</button>
    </form>

    <script>
        $(document).ready(function () {
            $('#offer_date').datepicker({
                format: "dd/mm/yyyy"
            });
        });

        $('#has_key').click(function(){
            if($('#has_key').is(":checked")){
                $("#key_broker").prop("disabled", false)
            }else{
                $("#key_broker").prop("disabled", true)
            }
        })

        $('#has_exclusive_contract').click(function(){
            if($('#has_exclusive_contract').is(":checked")){
                $("#exclusive_contract_broker").prop("disabled", false)
            }else{
                $("#exclusive_contract_broker").prop("disabled", true)
            }
        })

        $('#is_locked').click(function(){
            if($('#is_locked').is(":checked")){
                $("#locked_broker").prop("disabled", false)
            }else{
                $("#locked_broker").prop("disabled", true)
            }
        })

        $("button[name='ownerExists']").on("click", function(event){
            $("select[name='client_status']").attr("disabled", true);
            $("select[name='client_roles']").attr("disabled", true);
            $("input[name='client[]']").attr("disabled", true);
            $("input[name='phone']").attr("disabled", true);
            $("input[name='email']").attr("disabled", true);
            $("input[name='clientID']").attr("disabled", false);
            $("button[name='ownerDoesntExists']").attr("disabled", false);
            $("button[name='ownerExists']").attr("disabled", true);
        });
        $("button[name='ownerDoesntExists']").on("click", function(event){
            $("select[name='client_status']").attr("disabled", false);
            $("select[name='client_roles']").attr("disabled", false);
            $("input[name='client[]']").attr("disabled", false);
            $("input[name='phone']").attr("disabled", false);
            $("input[name='email']").attr("disabled", false);
            $("input[name='clientID']").attr("disabled", true);
            $("button[name='ownerDoesntExists']").attr("disabled", true);
            $("button[name='ownerExists']").attr("disabled", false);
        })
    </script>
@endsection
