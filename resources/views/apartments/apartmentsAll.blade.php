{{$apartments->links()}}
<script>
    {{--        @dd($apartments->toJson());--}}
    function checkApSysTask(idApartment) {
        let brokerId = $("#brokersList").val();
        $.ajax({
            beforeSend: function () {
                $("#p_prldr").show().fadeIn('slow');
            },
            complete: function () {
            },
            method: "post",
            url: "/rent/admin/newsystemtask/checker",
            type: "POST",
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "apartment_id": idApartment,
                "brokerId": brokerId,
            },
            success: function (response) {
                // $("#max").append(response['state']); //дебагер ответа dd();
                switch (response['state']) {
                    case('deleted'):
                        $("#CheckBtn" + response['apartment_id']).attr('class', 'm-1 btn btn-sm btn-outline-success');
                        break;
                    case('saved'):
                        $("#CheckBtn" + response['apartment_id']).attr('class', 'm-1 btn btn-sm btn-success');
                        break;
                }
                //ошыбка,состояние нет, состояние да
                // $("#max").append(response); //дебагер ответа dd();
                $("#p_prldr").hide().fadeOut('slow');
            }
        })
    }

    function markAll(markers) {

        let brokerId = $("#brokersList").val();
        let obj = markers.data;
        var arr = [];
        for (var k in obj) {
            arr.push(obj[k].id);
        }
        // console.log(JSON.stringify(arr));
        $.ajax({
            beforeSend: function () {
                $('#p_prldr').show();
            },
            complete: function () {
                $('#p_prldr').hide();
            },
            method: "POST",
            url: "/rent/admin/newsystemtask/checker",
            type: "POST",
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "marker": arr,
                "brokerId": brokerId,
            },
            success: function (response) {
                $("#p_prldr").hide().fadeOut('slow');
                for (var k in response) {
                    switch (response[k]['state']) {
                        case('deleted'):
                            $("#CheckBtn" + response[k]['apartment_id']).attr('class', 'm-1 btn btn-sm btn-outline-success');
                            break;
                        case("saved"):
                            $("#CheckBtn" + response[k].apartment_id).attr('class', 'm-1 btn btn-sm btn-success');
                            break;
                    }
                }
            }
        })
            .done(function (msg) {


            });
    }
</script>
<div id="max"></div>
<table class="table table-sm">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">Address</th>
        <th scope="col">Картинка</th>
        <th scope="col">
            <button type="button" onclick="markAll({{$apartments->toJson()}})"
                    class="btn btn-sm btn-outline-success">Check All (Още не работи) ({{$apartments->count()}})
            </button>
        </th>
    </tr>
    </thead>
    <tbody>
    @foreach($apartments as $apartment)
        <tr>
            <th scope="row">{{$apartment->id}}</th>
            <td>{{$apartment->address}}</td>
            <td><img id="photo" width="150px"
                     src={{($apartment->photos->isEmpty()) ? "" : asset('storage/apartments')."/".$apartment->photos[0]->photo_path}}>
            </td>
            <td>
                @if(!$apartment->brokerSystemTasks->isEmpty())
                    <button id="CheckBtn{{$apartment->id}}" type="button" onclick="checkApSysTask('{{$apartment->id}}')"
                            class="m-1 btn btn-sm btn-success">
                        <i class="fas fa-check-square"></i>
                    </button>
                @else
                    <button id="CheckBtn{{$apartment->id}}" type="button" onclick="checkApSysTask('{{$apartment->id}}')"
                            class="m-1 btn btn-sm btn-outline-success">
                        <i class="fas fa-check-square"></i>
                    </button>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{$apartments->links()}}
