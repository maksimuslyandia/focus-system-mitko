@extends('layouts.appSB')
@section('content')
    <script src="{{ asset('lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script src="{{ asset('lib/bootstrap-filestyle/bootstrap-filestyle.min.js') }}" defer></script>

    <script src="{{asset('lib/lightbox/ekko-lightbox.min.js')}}" defer></script>
    <link href="{{asset('lib/lightbox/ekko-lightbox.css')}}" rel="stylesheet" type="text/css">

    <script src="{{asset('lib/image-picker/image-picker.min.js')}}" defer></script>
    <link href="{{asset('lib/image-picker/image-picker.css')}}" rel="stylesheet" type="text/css">

    <form action="
@if(session('role') == "admin")
    {{URL::to('/rent/admin/apartments/update')}}
    @else
    {{URL::to('/rent/broker/apartments/update')}}
    @endif" method="post" enctype="multipart/form-data">
        @csrf
        <h3 class="mt-5">За собственика</h3>
        @foreach($apartmentInfo->clients as $key => $client)
            <hr>
            <div class="row">
                <div class="col-sm-6" @if(session('role')=='broker') style="display: none;"@endif>
                    <div class="input-group-prepend">
                        <span class="input-group-text">Статус на Собственикa</span>
                    </div>
                    <!-- <label for="client_status">Статус на Собственикa </label> -->
                    <select name="client_status" class="custom-select mr-sm-2">
                        @foreach($statuses as $status)
                            @if($status->id == $client->status_id)
                                <option value="{{$status->id}}" selected>{{$status->status_name}}</option>
                            @else
                                <option value="{{$status->id}}">{{$status->status_name}}</option>
                            @endif
                        @endforeach

                    </select>
                </div>
                <div class="col-sm-6" @if(session('role')=='broker') style="display: none;"@endif>
                    <div class="input-group-prepend">
                        <span class="input-group-text">Роля на собственика</span>
                    </div>
                    <!-- <label for="client_roles">Роля на собственика</label> -->
                    <select name="client_roles" class="custom-select mr-sm-2" id="client_roles">
                        @foreach($clientRoles as $role)
                            @if($client->clientRoles[0]->id == $role->id)
                                <option value="{{$role->id}}" selected>{{$role->role_name}}</option>
                            @else
                                <option value="{{$role->id}}">{{$role->role_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Име</span>
                    </div>
                    <!-- <label for="client{{$key}}">Име</label> -->
                    <input value="{{$client->first_name}}" name="client[]"
                           type="text"
                           required
                           class="form-control" id="first_name"
                           >
                </div>
         
                <div class="col-sm-6">
                    <div  class="input-group-prepend">
                        <span class="input-group-text">Фамилия</span>
                    </div>
                    <!-- <label for="client{{$key}}[]">Фамилия</label> -->
                    <input  value="{{$client->last_name}}" name="client[]" type="text"
                            class="form-control" id="last_name">
                    <input @if(session('role') == 'broker') disabled @endif value="{{$client->id}}"
                            required name="clientID" type="number"
                            class="form-control" id="clientID" style="visibility: hidden;" placeholder="Телефон ">
                </div>
            </div>

            {{--            todo добавить возможность добавлени инфо--}}

            <div class="row mt-3">
                <div class="col-sm-6" @if(session('role')=='broker') style="display: none;"@endif>
                    <div class="input-group-prepend">
                        <span class="input-group-text">Телефон</span>
                    </div>
                    <!-- <label for="phone">Телефон</label> -->
                    <input value="{{count($client->phones) != 0 ? $client->phones[0]->phone : 'Няма'}}"
                           required name="phone" type="text"
                           class="form-control" id="phone" placeholder="Телефон ">
                    
                    <input value="{{count($client->phones) != 0 ? $client->phones[0]->id : 'Няма'}}" name="phoneID" type="text"
                        class="form-control"
                        id="emailID" type="hidden" style="visibility: hidden;" placeholder="Email">
                </div>
                <div class="col-sm-6" @if(session('role')=='broker') style="display: none;"@endif>
                    <div class="input-group-prepend">
                        <span class="input-group-text">Email</span>
                    </div>
                    <!-- <label for="email"> Email </label> -->
                    @if(!$client->emails->isEmpty())
                        <input  value="{{$client->emails[0]->email}}" name="email" type="text"
                               class="form-control"
                               id="email" placeholder="Email">

                        <input  value="{{$client->emails[0]->id}}" name="emailID" type="text"
                                class="form-control"
                                id="emailID" type="hidden" style="visibility: hidden;" placeholder="Email">
                    @else
                        <input  value="" name="email" type="text"
                               class="form-control"
                               id="email" placeholder="Email">
                    @endif
                </div>
            </div>
        @endforeach
        <h3 class="mt-5">За Имот</h3>
        <hr>
        <div class="@if(session('role') == 'admin') mt-3 row @endif">
            <div class="col-sm-2" @if(session('role')=='broker') style="display: none;"@endif>
                <div class="custom-control custom-switch mt-3" style="">
                    <input type="hidden" name="is_locked" value="0"/>
                    <input type="checkbox" class="custom-control-input" id="is_locked" name="is_locked" value="1"  @if($apartmentInfo->is_locked === 1) checked @endif />
                    <label style=" onColor: warning;"  class="custom-control-label" for="is_locked"><i style=" color: orange;" class="fas fa-lg fa-lock"></i> Заключен апартамент </label>
                </div>
            </div>
            
            <div class="col-sm-4" @if(session('role')=='broker') style="display: none;"@endif>
                <div class="input-group-prepend">
                    <span class="input-group-text">Избери брокер, който е заключил апартамента</span>
                </div>
                <select class="custom-select mr-sm-2" id="locked_broker" name="locked_broker" @if($apartmentInfo->is_locked === 0) disabled @endif>
                    <option value="0">Не е избран</option>
                    @foreach($brokers as $broker)
                        @if(isset($apartmentInfo->locked_broker))
                            @if($apartmentInfo->locked_broker === $broker->id)
                                <option value="{{$broker->id}}" selected>{{$broker->username}} {{$broker->name}}</option>
                            @else
                                <option value="{{$broker->id}}">{{$broker->username}} {{$broker->name}}</option>
                            @endif
                        @else
                            <option value="{{$broker->id}}">{{$broker->username}} {{$broker->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <!--Key-->
        <div class="@if(session('role') == 'admin') mt-3 row @endif">
            <div class="col-sm-2" @if(session('role')=='broker') style="display: none;"@endif>
                <div class="custom-control custom-switch mt-3" style="">
                    <input type="hidden" name="has_exclusive_contract" value="0"/>
                    <input type="checkbox" class="custom-control-input" id="has_exclusive_contract" name="has_exclusive_contract" value="1"  @if($apartmentInfo->has_exclusive_contract === 1) checked @endif />
                    <label style=" onColor: warning;"  class="custom-control-label" for="has_exclusive_contract"><i style=" color: orange;" class="fas fa-lg fa-star"></i> Ексклузивен договор</label>
                </div>
            </div>
            
            <div class="col-sm-4" @if(session('role')=='broker') style="display: none;"@endif>
                <div class="input-group-prepend">
                        <span class="input-group-text">Избери брокер, който е взел ексклузивен договор</span>
                </div>
                <select name="exclusive_contract_broker" class="custom-select mr-sm-2" id="exclusive_contract_broker" @if($apartmentInfo->has_exclusive_contract === 0) disabled @endif>
                    <option value="0">Не е избран</option>
                    @foreach($brokers as $broker)
                        @if(isset($apartmentInfo->exclusive_contract))
                            @if($apartmentInfo->exclusive_contract_broker === $broker->id)
                                <option value="{{$broker->id}}" selected>{{$broker->username}} {{$broker->name}}</option>
                            @else
                                <option value="{{$broker->id}}">{{$broker->username}} {{$broker->name}}</option>
                            @endif
                        @else
                            <option value="{{$broker->id}}">{{$broker->username}} {{$broker->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
           <div class="col-sm-2" @if(session('role')=='broker') style="display: none;"@endif>
                <div class="custom-control custom-switch" style="padding-top:13%;margin-left:60%;">
                    <input type="hidden" name="has_key" value="0" />
                    <input name="has_key" type="checkbox" class="custom-control-input" id="has_key" value="1"  @if($apartmentInfo->has_key === 1) checked @endif>
                    <label style=" onColor: warning;"  class="custom-control-label" for="has_key"><i style=" color: orange;" class="fas fa-key fa-lg" ></i>Ключ</label>
                </div>
            </div>

            <div class="col-sm-4" @if(session('role')=='broker') style="display: none;"@endif>
            <div class="input-group-prepend">
                <span class="input-group-text">Избери брокер, който е взел ключа</span>
            </div>
                <!-- <label for="client_roles">Избери брокер, който е взел ключа</label> -->
                <select name="key_broker" class="custom-select mr-sm-2" id="key_broker" @if($apartmentInfo->has_key === 0) disabled @endif>
                    <option value="false">Не е избран</option>
                    @foreach($brokers as $broker)
                        @if(isset($apartmentInfo->keys))
                            @if($apartmentInfo->keys === $broker->id)
                                <option value="{{$broker->id}}" selected>
                                {{$broker->username}} {{$broker->name}}  
                                </option>
                            @else
                                <option value="{{$broker->id}}">
                                    {{$broker->username}} {{$broker->name}}
                                </option>
                            @endif
                        @else
                            <option value="{{$broker->id}}">
                                {{$broker->username}} {{$broker->name}}
                            </option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        @if(session('role')!='broker')
            <div class=" @if(session('role') == 'admin') mt-3 row @endif">
                <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Дата на офертата</span>
                </div>
                <!-- <label for="offer_date">Дата на офертата</label> -->
                <input type="text" class="form-control" placeholder="Дата" name="offer_date" id="offer_date" value="{{$apartmentInfo->offer_date}}">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
                </div>

                <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Взета от сайт</span>
                </div>
                    <!-- <label for="offer_website">Взета от сайт</label> -->
                    <input type="text" class="form-control" placeholder="сайт" name="offer_website" value="{{$apartmentInfo->offer_website}}">
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
        @endif
        <!-- <div class="col-sm-4 mt-2">
                <label for="offer_date">Дата на офертата</label>
                <textarea name="offer_date" type="text" class="form-control" id="offer_date">{{$apartmentInfo->offer_date}}</textarea>
        </div> -->

        <div class="@if(session('role') == 'admin') mt-3 row @endif">
            <div class="col-sm-6" @if(session('role')=='broker') style="display: none;"@endif>
            <div class="input-group-prepend">
                <span class="input-group-text">Статус на имота</span>
            </div>
                <!-- <label for="apartment_status">Статус на имота</label> -->
                <select name="apartment_status" class="custom-select mr-sm-2" id="apartment_status">
                    @foreach($statuses as $status)
                        @if($status->id == $apartmentInfo->statuses->id)
                            <option selected value="{{$status->id}}">{{$status->status_name}}</option>
                        @else
                            <option value="{{$status->id}}">{{$status->status_name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="col-sm-6" @if(session('role')=='broker') style="display: none;"@endif>
                <div class="input-group-prepend">
                    <span class="input-group-text">Роля на имота</span>
                </div>
                <!-- <label for="apartment_role">Роля на имота</label> -->
                <select name="apartment_role" class="custom-select mr-sm-2" id="apartment_role">
                    @foreach($apartmentRoles as $role)
                        @if(count($apartmentInfo->apartmentRoles) != 0)
                            @if($role->id == $apartmentInfo->apartmentRoles[0]->id)
                                <option selected value="{{$role->id}}">{{$role->role_name}}</option>
                            @else
                                <option value="{{$role->id}}">{{$role->role_name}}</option>
                            @endif
                        @else
                            <option value="{{$role->role_name}}">{{$role->role_name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row @if(session('role') == 'admin')mt-3 @endif">
            <div class="col-sm-12">
            <div class="input-group-prepend">
                    <span class="input-group-text">Адрес</span>
                </div>
                <!-- <label for="address">Адрес</label> -->
                <input required value="{{$apartmentInfo->address}}" name="address" type="text" class="form-control"
                       id="address">
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-4" mt-2="">
                 <div class="input-group-prepend">
                    <span class="input-group-text">Отопление</span>
                </div>
                <!-- <label for="otoplenie">Отопление</label> -->
                <select name="otoplenie" class="custom-select mr-sm-2">
                    <option value="">избери</option>
                    <option value="Топлофикация"@if(isset($apartmentInfo->otoplenie)&& $apartmentInfo->otoplenie=='Топлофикация')selected @endif>Топлофикация</option>
                    <option value="Ток"@if(isset($apartmentInfo->otoplenie)&& $apartmentInfo->otoplenie=='Ток')selected @endif>Ток</option>
                    <option value="Газификация"@if(isset($apartmentInfo->otoplenie)&& $apartmentInfo->otoplenie=='Газификация')selected @endif>Газификация</option>
                    <option value="Локално"@if(isset($apartmentInfo->otoplenie)&& $apartmentInfo->otoplenie=='Локално')selected @endif>Локално</option>
                    <option value="Няма"@if(isset($apartmentInfo->otoplenie)&& $apartmentInfo->otoplenie=='Няма')selected @endif>Няма</option>
                </select>
            </div>
            <div class="col-sm-4" mt-2="">
                <div class="input-group-prepend">
                    <span class="input-group-text">Район</span>
                </div>
                <!-- <label for="region">Район</label> -->
                <select name="region" class="custom-select mr-sm-2">
                    <option value="">избери</option>
                    <option value="Гагарин"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Гагарин')selected @endif>Гагарин</option>
                    <option value="Западен"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Западен')selected @endif>Западен</option>
                    <option value="Захарна фабрика П"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Захарна фабрика П')selected @endif>Захарна фабрика П</option>
                    <option value="Изгрев П"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Изгрев П')selected @endif>Изгрев П</option>
                    <option value="Индустриална зона - изток"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Индустриална зона - изток')selected @endif>Индустриална зона - изток</option>
                    <option value="Индустриална зона - север"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Индустриална зона - север')selected @endif>Индустриална зона - север</option>
                    <option value="Индустриална зона - Тракия"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Индустриална зона - Тракия')selected @endif>Индустриална зона - Тракия</option>
                    <option value="Институт по овощарство"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Институт по овощарство')selected @endif>Институт по овощарство</option>
                    <option value="Каменица 1"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Каменица 1')selected @endif>Каменица 1</option>
                    <option value="Каменица 2"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Каменица 2')selected @endif>Каменица 2</option>
                    <option value="Коматево"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Коматево')selected @endif>Коматево</option>
                    <option value="Коматевски възел"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Коматевски възел')selected @endif>Коматевски възел</option>
                    <option value="Кършияка"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Кършияка')selected @endif>Кършияка</option>
                    <option value="Кючук Париж"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Кючук Париж')selected @endif>Кючук Париж</option>
                    <option value="Мараша"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Мараша')selected @endif>Мараша</option>
                    <option value="Марица Север"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Марица Север')selected @endif>Марица Север</option>
                    <option value="Младежки хълм"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Младежки хълм')selected @endif>Младежки хълм</option>
                    <option value="Остромила"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Остромила')selected @endif>Остромила</option>
                    <option value="Прослав"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Прослав')selected @endif>Прослав</option>
                    <option value="Саут Сити"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Саут Сити')selected @endif>Саут Сити</option>
                    <option value="Старият град"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Старият град')selected @endif>Старият град</option>
                    <option value="Столипиново"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Столипиново')selected @endif>Столипиново</option>
                    <option value="Сточна гара П"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Сточна гара П')selected @endif>Сточна гара П</option>
                    <option value="Съдийски"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Съдийски')selected @endif>Съдийски</option>
                    <option value="Тракия"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Тракия')selected @endif>Тракия</option>
                    <option value="Филипово"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Филипово')selected @endif>Филипово</option>
                    <option value="Христо Ботев"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Христо Ботев')selected @endif>Христо Ботев</option>
                    <option value="Христо Смирненски"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Христо Смирненски')selected @endif>Христо Смирненски</option>
                    <option value="Централна гара П"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Централна гара П')selected @endif>Централна гара П</option>
                    <option value="Център П"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Център П')selected @endif>Център П</option>
                    <option value="Шекер махала"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Шекер махала')selected @endif>Шекер махала</option>
                    <option value="Южен"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Южен')selected @endif>Южен</option>
                    <option value="Въстанически"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Въстанически')selected @endif>Въстанически</option>
                    <option value="Индустриална зона - запад"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Индустриална зона - запад')selected @endif>Индустриална зона - запад</option>
                    <option value="Индустриална зона - юг"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Индустриална зона - юг')selected @endif>Индустриална зона - юг</option>
                    <option value="Гаганица"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Гаганица')selected @endif>Гаганица</option>
                    <option value="Беломорски"@if(isset($apartmentInfo->region)&& $apartmentInfo->region=='Беломорски')selected @endif>Беломорски</option>
                </select>
            </div>
            <div class="col-sm-4" mt-2="">
                <div class="input-group-prepend">
                    <span class="input-group-text">Изложение</span>
                </div>
                <!-- <label for="izloginie">Изложение</label> -->
                <select name="izloginie[]" class="custom-select mr-sm-2" multiple>
                    <option value="">избери</option>
                    <option value="izlogenie_iztok"  @if(isset($apartmentInfo->izlogenie_iztok)&& $apartmentInfo->izlogenie_iztok==1)selected @endif>Изток</option>
                    <option value="izlogenie_zapad" @if(isset($apartmentInfo->izlogenie_zapad)&& $apartmentInfo->izlogenie_zapad==1)selected @endif>Запад</option>
                    <option value="izlogenie_sever" @if(isset($apartmentInfo->izlogenie_sever)&& $apartmentInfo->izlogenie_sever==1)selected @endif>Север</option>
                    <option value="izlogenie_ug" @if(isset($apartmentInfo->izlogenie_ug)&& $apartmentInfo->izlogenie_ug==1)selected @endif>Юг</option>
                </select>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Вид строителство</span>
                </div>
                <!-- <label for="vid_stroitelstvo">Вид строителство</label> -->
                <select name="vid_stroitelstvo" class="custom-select mr-sm-2">
                    <option value="">избери</option>
                    <option value="ЕПК"@if(isset($apartmentInfo->vid_stroitelstvo)&& $apartmentInfo->vid_stroitelstvo=='ЕПК')selected @endif>ЕПК</option>
                    <option value="Тухла"@if(isset($apartmentInfo->vid_stroitelstvo)&& $apartmentInfo->vid_stroitelstvo=='Тухла')selected @endif>Тухла</option>
                    <option value="Ново"@if(isset($apartmentInfo->vid_stroitelstvo)&& $apartmentInfo->vid_stroitelstvo=='Ново')selected @endif>Ново</option>
                    <option value="Панел"@if(isset($apartmentInfo->vid_stroitelstvo)&& $apartmentInfo->vid_stroitelstvo=='Панел')selected @endif>Панел</option>
                    <option value="Гредоред"@if(isset($apartmentInfo->vid_stroitelstvo)&& $apartmentInfo->vid_stroitelstvo=='Гредоред')selected @endif>Гредоред</option>
                    <option value="Неизвестно"@if(isset($apartmentInfo->vid_stroitelstvo)&& $apartmentInfo->vid_stroitelstvo=='Неизвестно')selected @endif>Неизвестно</option>
                </select>
            </div>
            <div class="col-sm-4 mt-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Обзавеждане</span>
            </div>
                <!-- <label for="obzavegdane">Обзавеждане</label> -->
                <select name="obzavegdane" class="custom-select mr-sm-2">
                    <option value="">избери</option>
                    <option value="Обзаведен"@if(isset($apartmentInfo->obzavegdane)&& $apartmentInfo->obzavegdane=='Обзаведен')selected @endif>Обзаведен</option>
                    <option value="Необзаведен"@if(isset($apartmentInfo->obzavegdane)&& $apartmentInfo->obzavegdane=='Необзаведен')selected @endif>Необзаведен</option>
                    <option value="Полуобзаведен"@if(isset($apartmentInfo->obzavegdane)&& $apartmentInfo->obzavegdane=='Полуобзаведен')selected @endif>Полуобзаведен</option>
                    <option value="Може обзаведен"@if(isset($apartmentInfo->obzavegdane)&& $apartmentInfo->obzavegdane=='Може обзаведен')selected @endif>Може обзаведен</option>
                    <option value="Може необзаведен"@if(isset($apartmentInfo->obzavegdane)&& $apartmentInfo->obzavegdane=='Може необзаведен')selected @endif>Може необзаведен</option>
                </select>
            </div>
            @if(session('sub_role') === "sales" || ($apartmentInfo->apartmentRoles[0]->role_name === "продава се" || $apartmentInfo->apartmentRoles[0]->role_name === "продаден"))
                <div class="col-sm-4 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Степен на завършеност</span>
                    </div>
                    <!-- <label for="finished_state">Степен на завършеност</label> -->
                    <select name="finished_state" class="custom-select mr-sm-2">
                        <option value="13"@if(isset($apartmentInfo->finished_state)&& $apartmentInfo->finished_state===13)selected @endif>Пред акт 14</option>
                        <option value="14"@if(isset($apartmentInfo->finished_state)&& $apartmentInfo->finished_state===14)selected @endif>Акт 14</option>
                        <option value="15"@if(isset($apartmentInfo->finished_state)&& $apartmentInfo->finished_state===15)selected @endif>Акт 15</option>
                        <option value="16"@if(isset($apartmentInfo->finished_state)&& $apartmentInfo->finished_state===16)selected @endif>Акт 16</option>
                    </select>
                </div>
            @else
                <input type="hidden" name="finished_state" value="{{$apartmentInfo->finished_state}}"/>
            @endif
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Вид имот</span>
                </div>
                <!-- <label for="vid_imot">Вид имот</label> -->
                <select name="vid_imot" class="custom-select mr-sm-2">
                    <option value="">избери</option>
                    <option value="Мезонет"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Мезонет')selected @endif>Мезонет</option>
                    <option value="Къща"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Къща')selected @endif>Къща</option>
                    <option value="Едностаен"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Едностаен')selected @endif>Едностаен</option>
                    <option value="Двустаен"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Двустаен')selected @endif>Двустаен</option>
                    <option value="Тристаен"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Тристаен')selected @endif>Тристаен</option>
                    <option value="Четиристайни"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Четиристайни')selected @endif>Четиристайни</option>
                    <option value="Многостаен"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Многостаен')selected @endif>Многостаен</option>
                    <option value="стая"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='стая')selected @endif>стая</option>
                    <option value="офис"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='офис')selected @endif>офис</option>
                    <option value="магазин"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='магазин')selected @endif>магазин</option>
                    <option value="Заведение"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Заведение')selected @endif>Заведение</option>
                    <option value="Гараж"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Гараж')selected @endif>Гараж</option>
                    <option value="Ателие"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Ателие')selected @endif>Ателие</option>
                    <option value="склад"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='склад')selected @endif>склад</option>
                    <option value="хотел"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='хотел')selected @endif>хотел</option>
                    <option value="пром. имот"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='пром. имот')selected @endif>пром. имот</option>
                    <option value="паркомясто"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='паркомясто')selected @endif>паркомясто</option>
                    <option value="етаж къща"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='етаж къща')selected @endif>етаж къща</option>
                    <option value="Студио"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Студио')selected @endif>Студио</option>
                    <option value="Парцел"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Парцел')selected @endif>Парцел</option>
                    <option value="Сграда"@if(isset($apartmentInfo->vid_imot)&& $apartmentInfo->vid_imot=='Сграда')selected @endif>Сграда</option>                
                </select>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Преход</span>
                </div>
                <!-- <label for="prehod">Преход</label> -->
                <select name="prehod" class="custom-select mr-sm-2">
                    <option value="">избери</option>
                    <option value="3"@if(isset($apartmentInfo->prehod)&& $apartmentInfo->prehod==3)selected @endif>Не се знае</option>
                    <option value="2"@if(isset($apartmentInfo->prehod)&& $apartmentInfo->prehod==2)selected @endif>Няма</option>
                    <option value="1"@if(isset($apartmentInfo->prehod)&& $apartmentInfo->prehod==1)selected @endif>Има</option>
                </select>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Изолация</span>
                </div>
                <!-- <label for="izolatsia">Изолация</label> -->
                <select name="izolatsia" class="custom-select mr-sm-2">
                    <option value="">избери</option>
                    <option value="1"@if(isset($apartmentInfo->izolatsia)&& $apartmentInfo->izolatsia==1)selected @endif>да</option>
                    <option value="2"@if(isset($apartmentInfo->izolatsia)&& $apartmentInfo->izolatsia==2)selected @endif>Не</option>
                </select>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Асансьор</span>
                </div>
                <!-- <label for="elevator">Асансьор</label> -->
                <select name="elevator" class="custom-select mr-sm-2">
                    <option value="">избери</option>
                    <option value="1"@if(isset($apartmentInfo->elevator)&& $apartmentInfo->elevator==1)selected @endif>Има</option>
                    <option value="2"@if(isset($apartmentInfo->elevator)&& $apartmentInfo->elevator==2)selected @endif>Няма</option>
                </select>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Комисионна</span>
                </div>
                <!-- <label for="komisionna">Комисионна</label> -->
                <select name="komisionna" class="custom-select mr-sm-2">
                    <option value="">избери</option>
                    <option value="1"@if(isset($apartmentInfo->komisionna)&& $apartmentInfo->komisionna==1)selected @endif>Да</option>
                    <option value="2"@if(isset($apartmentInfo->komisionna)&& $apartmentInfo->komisionna==2)selected @endif>Не</option>
                </select>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Етаж</span>
                </div>
                <!-- <label for="etag">Етаж</label> -->
                <input value="{{$apartmentInfo->etag}}" name="etag" type="number" class="form-control">
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Общо етажа</span>
                </div>
                <!-- <label for="obsho_etaga">Общо етажа</label> -->
                <input value="{{$apartmentInfo->obsho_etaga}}" name="obsho_etaga" type="number" class="form-control">
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Коментар за етажа</span>
                </div>
                <!-- <label for="komentar_za_etaga">Коментар за етажа</label> -->
                <textarea name="komentar_za_etaga" type="text" class="form-control">{{$apartmentInfo->komentar_za_etaga}}</textarea>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Ориентир за адреса</span>
                </div>
                <!-- <label for="orientir_za_address">Ориентир за адреса</label> -->
                <textarea name="orientir_za_address" type="text" class="form-control">{{$apartmentInfo->orientir_za_address}}</textarea>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Описание на обзавеждането</span>
                </div>
                <!-- <label for="opisanie_na_obzavegdane">Описание на обзавеждането</label> -->
                <textarea name="opisanie_na_obzavegdane" type="text" class="form-control">{{$apartmentInfo->opisanie_na_obzavegdane}}</textarea>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Описание на настилките</span>
                </div>
                <!-- <label for="opisanie_za_nastilki">Описание на настилките</label> -->
                <textarea name="opisanie_za_nastilki" type="text" class="form-control">{{$apartmentInfo->opisanie_za_nastilki}}</textarea>
            </div>
            @if(session('sub_role') === "sales" || ($apartmentInfo->apartmentRoles[0]->role_name === "продава се" || $apartmentInfo->apartmentRoles[0]->role_name === "продаден"))
                <div class="col-sm-4 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Квадратура РЗП</span>
                    </div>
                    <!-- <label for="kvadratura_rzp">Квадратура РЗП</label> -->
                    <input value="{{$apartmentInfo->kvadratura_rzp}}" name="kvadratura_rzp" type="number" class="form-control">
                </div>
                
                <div class="col-sm-4 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Квадратура ЗП</span>
                    </div>
                    <input value="{{$apartmentInfo->kvadratura_zp}}" name="kvadratura_zp" type="number" class="form-control">
                </div>
                <!-- <label for="kvadratura_zp">Квадратура ЗП</label> -->
            @else

            <div class="col-sm-4 mt-3">
                <input type="hidden" name="kvadratura_rzp" value="{{$apartmentInfo->kvadratura_rzp}}"/>
                <div class="input-group-prepend">
                    <span class="input-group-text">Квадратура</span>
                </div>
                <input value="{{$apartmentInfo->kvadratura_zp}}" name="kvadratura_zp" type="number" class="form-control">
            </div>
                <!-- <label for="kvadratura_zp">Квадратура ЗП</label> -->
            @endif

            @if(session('sub_role') === "sales" || ($apartmentInfo->apartmentRoles[0]->role_name === "продава се" || $apartmentInfo->apartmentRoles[0]->role_name === "продаден"))               
                <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Година на строеж</span>
                </div>
                    <!-- <label for="year_build">Година на строеж</label> -->
                    <input value="{{$apartmentInfo->year_build}}" name="year_build" type="number" class="form-control" >
                </div>
            @else 
                <input type="hidden" name="year_build" value="{{$apartmentInfo->year_build}}"/>
            @endif
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Цена EUR</span>
                </div>
                <!-- <label for="cena_eur">Цена EUR</label> -->
                <input value="{{$apartmentInfo->cena_eur}}" name="cena_eur" type="number" class="form-control">
            </div>
            @if(session('sub_role') === null ||($apartmentInfo->apartmentRoles[0]->role_name === "отдава се" || $apartmentInfo->apartmentRoles[0]->role_name === "отдаден"))
                <div class="col-sm-4 mt-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Коментар по цената в левове</span>
                    </div>
                    <!-- <label for="price_comment">Коментар по цената в левове</label> -->
                    <textarea name="price_comment" type="text" class="form-control">{{$apartmentInfo->price_comment}}</textarea>
                </div>
            @else 
                <input type="hidden" name="price_comment" value="{{$apartmentInfo->price_comment}}"/>
            @endif
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Комисионна забележка</span>
                </div>
                <!-- <label for="komesionna_zabelegka">Комисионна забележка</label> -->
                <textarea name="komesionna_zabelegka" type="text" class="form-control">{{$apartmentInfo->komesionna_zabelegka}}</textarea>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Брокерска бележка</span>
                </div>
                <!-- <label for="brokerska_belegka">Брокерска бележка</label> -->
                <textarea name="brokerska_belegka" type="text" class="form-control">{{$apartmentInfo->brokerska_belegka}}</textarea>
            </div>
            <div class="col-sm-4 mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Разпределение</span>
                </div>
                <!-- <label for="razpredelenie">Разпределение</label> -->
                <textarea name="razpredelenie" type="text" class="form-control">{{$apartmentInfo->razpredelenie}}</textarea>
            </div>  
            <input value="{{$apartmentInfo->id}}" name="apartment_id" style="display: none;">
          
        </div>
        @if(session('role')!='broker')
            <div class="row">
                <div class="col-sm-6 mt-3">
                    <div class="form-group"> 
                        <input type="file" data-badge="true" data-btnClass="btn-info" class="filestyle" name="photo[]" id="file" multiple>
                    </div>
                </div>
            </div>
        @endif
       
        <button class="btn btn-primary " type="submit" >Запази</button>

          <!-- <div class="col-sm-12 mt-3"> -->
   
        <!-- </div> -->
    </form>


    <!-- <script>
        $(document).on('click', '[data-toggle="lightbox"]', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script> -->
    <script>
        $(document).ready(function () {
            $('#offer_date').datepicker({
                format: "dd/mm/yyyy"
            });
        });

        $('#has_key').click(function(){
            if($('#has_key').is(":checked")){
                $("#key_broker").prop("disabled", false)
            }else{
                $("#key_broker").prop("disabled", true)
            }
        })
        
        $('#has_exclusive_contract').click(function(){
            if($('#has_exclusive_contract').is(":checked")){
                $("#exclusive_contract_broker").prop("disabled", false)
            }else{
                $("#exclusive_contract_broker").prop("disabled", true)
            }
        })

        $('#is_locked').click(function(){
            if($('#is_locked').is(":checked")){
                $("#locked_broker").prop("disabled", false)
            }else{
                $("#locked_broker").prop("disabled", true)
            }
        })
        // $('#checkbox1').click(function() {
        //     if (!$(this).is(':checked')) {
        //         var ans = confirm("Are you sure?");
        //         $('#textbox1').val(ans);
        //     }
        // });
        // function checkPhone() {
        //     let phone = $('#phone').val();

        //     $.ajax({
        //         method: "POST",
        //         url: "/checkphone",
        //         type: "POST",
        //         data: {
        //             "_token": $('meta[name="csrf-token"]').attr('content'),
        //             "phone": phone,
        //         },
        //         success: function (response) {
        //             // $("#max").append(response); //дебагер ответа dd();
        //         }
        //     })
        //         .done(function (msg) {
        //             if (Array.isArray(msg)) {
        //                 msg.forEach((item, index) => {
        //                     // document.getElementById("phoneInfo").innerHTML += index + ":" + item['phone'] + "<br>";
        //                     // $("#phoneInfo").appendTo("<p>"+ item+"</p>" ); //дебагер ответа dd();
        //                     item['get_phone_client'].forEach((subitem, subindex) => {
        //                         $("#phoneInfo").html('<a href="/rent/admin/clients/get/'+subitem['id']+'" class="btn btn-primary btn-sm" role="button">Go to This Client</a>');
        //                     });
        //                 });
        //             }else {
        //                 $("#phoneInfo").html("<h2>Няма номер в система</h2>");
        //             }
        //             // $("#phoneInfo").html(msg);
        //         });
        // }
    </script>

@endsection
