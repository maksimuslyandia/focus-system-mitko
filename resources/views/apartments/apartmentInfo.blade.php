@extends('layouts.appSB')
@section('content')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        $(".address").on("click", function () {
            // alert($(this).html());
            let id = this.id;
            let elem = this;
            $(elem).html($(elem).val());
            if ($(elem).val() === '') {
                $(elem).html('Няма Адрес');
            }
            setTimeout(function () {
                $(elem).html('Покажи адрес')
            }, 4000);
        });

        // Initialize and add the map
        function initMap() {
            // The location of Uluru
            var uluru = {lat: -25.344, lng: 131.036};
            // The map, centered at Uluru
            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 4, center: uluru});
            // The marker, positioned at Uluru
            var marker = new google.maps.Marker({position: uluru, map: map});
        }
    </script>
    
    <script src="{{asset('lib/lightbox/ekko-lightbox.min.js')}}" defer></script>
    
    <link href="{{asset('lib/lightbox/ekko-lightbox.css')}}" rel="stylesheet" type="text/css">

    <h3>Инфо за апартамент {{$apartmentInfo->id}}
        @foreach($apartmentInfo->apartmentRoles as $role)
            | <sup>{{$role->role_name}}</sup>
        @endforeach
        <a class="btn btn-sm btn-outline-warning m-3" href="/rent/admin/apartments/update/{{$apartmentInfo->id}}"
           target="_blank" role="button">Редактирай</a>
    </h3>


    <div class="row">
        <div class="col-sm-6 mb-4">
            {{--        @dd($apartmentInfo->clients)--}}
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имена на клиента</th>
                    <th scope="col">Притежава апартаменти</th>
                </tr>
                </thead>
                <tbody>
                @foreach($apartmentInfo->clients as $key => $client)
                    {{--                    @dd($apartmentInfo)--}}
                    <tr>
                        <th scope="row"><a href="/rent/broker/clients/get/{{$client->id}}" type="button"
                                           class="btn btn-sm btn-outline-primary">Към Клиент: {{$client->id}}</a></th>
                        <td>{{$client->first_name}} {{$client->middle_name}} {{$client->last_name}} </td>
                        <td><a href="#" class="btn btn-secondary btn-sm active m-1" role="button" aria-pressed="true">Този
                                имот</a><br>
                            @foreach($client->apartments as $k => $apartment)
                                @if($apartment->id !=$apartmentInfo->id)
                                    <a href="/rent/admin/apartments/get/{{$apartment->id}}"
                                       class="btn btn-secondary m-1 btn-sm active" role="button"
                                       aria-pressed="true">{{$apartment->id}} (Адрес: {{$apartment->address}})</a>
                                @endif
                            @endforeach </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">Артибут</th>
                    <th scope="col">Параметр</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Id апартамента</td>
                    <td>{{$apartmentInfo->id}}</td>
                </tr>
                <tr>
                    <td>Адрес</td>

                    <td>{{$apartmentInfo->address}}</td>
                </tr>
                <tr>
                    <td>Атрибути</td>
                    <td>
                        @if($apartmentInfo->status_id ==1)
                            <span style=" color: green;"
                                  data-toggle="tooltip" data-placement="top"
                                  title="Активен имот">
                            <i class="fas fa-power-off fa-lg"></i>
                        </span>
                        @else
                            <span style=" color: red;"
                                  data-toggle="tooltip" data-placement="top"
                                  title="Не активен имот">
                            <i class="fas fa-power-off fa-lg"></i>
                        </span>
                        @endif
                        @if(isset($apartment->key))
                            <span style=" color: orange;"
                                  data-toggle="tooltip" data-placement="top"
                                  title="Ключ при  {{$apartment->key->username}} ({{$apartment->key->name}})">
                             <i class="fas fa-key fa-lg"></i>
                            </span>
                        @endif
                        @if($apartment->has_exclusive_contract === 1)
                            <span style=" color: orange;"
                                data-toggle="tooltip" data-placement="top"
                                data-toggle="tooltip" data-placement="top"
                                @if(isset($apartment->exclusive_contract))
                                    title="Ексклузивен договор взет от {{$apartment->exclusive_contract->username}} ({{$apartment->exclusive_contract->name}})">
                                @else
                                    title="Екслклузивният договор не е взет от никой брокер">
                                @endif
                                    <i class="fas fa fa-star fa-lg"></i>
                            </span>
                        @endif
                    @if($apartment->is_locked === 1)
                        <span style=" color: orange;"
                              data-toggle="tooltip" data-placement="top"
                              data-toggle="tooltip" data-placement="top"
                              @if(isset($apartment->lock_broker))
                                title="Апартаментът е заключен от {{$apartment->lock_broker->username}} ({{$apartment->lock_broker->name}})">
                              @else
                                title="Апартаментът е заключен">
                              @endif
                                <i class="fas fas fa fa-lock fa-lg"></i>
                        </span>
                    @endif
                        @if($apartmentInfo->top ==1)
                            <span style=" color: Tomato;" data-toggle="tooltip" data-placement="top"
                                  title="Топ Оферта">
                                  <i class="fas fa-arrow-up fa-lg"></i>
                        </span>
                        @endif
                        @if($apartmentInfo->parking ==1)
                            <span style=" color: blue;" data-toggle="tooltip" data-placement="top"
                                  title="Гараж">
                           <i class="fas fa-parking fa-lg"></i>
                        </span>
                        @endif
                        @if($apartmentInfo->air_conditioner ==1)
                            <span style=" color: deepskyblue;" data-toggle="tooltip" data-placement="top"
                                  title="Климатик">
                            <i class="fas fa-fan fa-lg"></i>
                        </span>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Цена Евро</td>
                    <td>{{$apartmentInfo->cena_eur}}</td>
                </tr>
                <tr>
                    <td>Асансьор</td>
                    <td>@if($apartmentInfo->elevator ==1)
                            Има
                        @else
                            Няма
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Отопление</td>
                    <td>{{$apartmentInfo->otoplenie}}</td>
                </tr>
                <tr>
                    <td>Район</td>
                    <td>{{$apartmentInfo->region}}</td>
                </tr>
                <tr>
                    <td>Отопление</td>
                    <td>{{$apartmentInfo->otoplenie}}</td>
                </tr>
                <tr>
                    <td>Вид строитество</td>
                    <td>{{$apartmentInfo->vid_stroitelstvo}}</td>
                </tr>
                <tr>
                    <td>Обзавеждане</td>
                    <td>{{$apartmentInfo->obzavegdane}}</td>
                </tr>
                <tr>
                    <td>Описанеи на обзавеждане</td>
                    <td>{{$apartmentInfo->opisanie_na_obzavegdane}}</td>
                </tr>
                <tr>
                    <td>Описание за настилки</td>
                    <td>{{$apartmentInfo->opisanie_za_nastilki}}</td>
                </tr>
                <tr>
                    <td>Вид имот</td>
                    <td>{{$apartmentInfo->vid_imot}}</td>
                </tr>

                <tr>
                    <td>Преход</td>
                    <td>
                        @switch($apartmentInfo->prehod)
                            @case(1)
                            <span>Има</span>
                            @break
                            @case(2)
                            <span>Няма</span>
                            @break
                            @case(3)
                            <span>Не се знае</span>
                            @break
                        @endswitch
                    </td>
                </tr>
                <tr>
                    <td>Изолация</td>
                    @if($apartmentInfo->izolatsia == 2)
                        <td>Не</td>
                    @else
                        <td>Да</td>
                    @endif
                </tr>
                <tr>
                    <td>Комисионна</td>
                    @if($apartmentInfo->izolatsia == 2)
                        <td>Не</td>
                    @else
                        <td>Да</td>
                    @endif                </tr>
                <tr>
                    <td>Комисионна забележка</td>
                    <td>{{$apartmentInfo->komesionna_zabelegka}}</td>
                </tr>
                <tr>
                    <td>Етаж</td>
                    <td>{{$apartmentInfo->etag}}</td>
                </tr>
                <tr>
                    <td>Общо етажа</td>
                    <td>{{$apartmentInfo->obsho_etaga}}</td>
                </tr>
                <tr>
                    <td>Коментар за етажа</td>
                    <td>{{$apartmentInfo->komentar_za_etaga}}</td>
                </tr>
                <tr>
                    <td>Ориентир за адрес</td>
                    <td>{{$apartmentInfo->orientir_za_address}}</td>
                </tr>
                <tr>
                    <td>Брокерска бележка</td>
                    <td>{{$apartmentInfo->brokerska_belegka}}</td>
                </tr>
                <tr>
                    <td>Разпределение</td>
                    <td>{{$apartmentInfo->razpredelenie}}</td>
                </tr>
            
                @if(session('sub_role') === "sales" || ($apartmentInfo->apartmentRoles[0]->role_name === "продава се" || $apartmentInfo->apartmentRoles[0]->role_name === "продаден"))
                    <tr>
                        <td>Квадратура ЗП</td>
                        <td>{{$apartmentInfo->kvadratura_zp}}</td>
                    </tr>
                    <tr>
                        <td>Квадратура РЗП</td>
                        <td>{{$apartmentInfo->kvadratura_rzp}}</td>
                    </tr>
                @else
                    <tr>
                        <td>Квадратура</td>
                        <td>{{$apartmentInfo->kvadratura_zp}}</td>
                    </tr>
                @endif
                @if(session('role') == 'admin')
                    <tr>
                        <td>Дата на качена оферта</td>
                        <td>{{$apartmentInfo->offer_date}}</td>
                    </tr>
                    <tr>
                        <td>Сайт на качена оферта</td>
                        <td>{{$apartmentInfo->offer_website}}</td>
                    </tr>
                @endif
                @if(session('sub_role') === "sales" || ($apartmentInfo->apartmentRoles[0]->role_name === "продава се" || $apartmentInfo->apartmentRoles[0]->role_name === "продаден"))
                    <tr>
                        <td>Степен на завършеност</td>
                        @if($apartmentInfo->finished_state === 13)
                            <td>Пред акт 14</td>
                        @else
                            <td>Aкт {{$apartmentInfo->finished_state }}</td>
                        @endif
                    </tr>
                @endif
                @if(session('sub_role') === "sales" || ($apartmentInfo->apartmentRoles[0]->role_name === "продава се" || $apartmentInfo->apartmentRoles[0]->role_name === "продаден"))               
                    <td>Година на строеж</td>
                    <td>{{$apartmentInfo->year_build}}</td>
                @endif
                <tr>
                    <td>Изложение</td>
                    <td>@if($apartmentInfo->izlogenie_iztok==1)
                            Изток<br>
                        @endif
                        @if($apartmentInfo->izlogenie_zapad==1)
                            Запад<br>
                        @endif
                        @if($apartmentInfo->izlogenie_sever==1)
                            Север<br>
                        @endif
                        @if($apartmentInfo->izlogenie_ug==1)
                            Юг<br>
                        @endif
                    </td>
                </tr>
                @if(session('sub_role') === null ||($apartmentInfo->apartmentRoles[0]->role_name === "отдава се" || $apartmentInfo->apartmentRoles[0]->role_name === "отдаден"))
                    <tr>
                        <td>Коментар по цената в левове</td>
                        <td>{{$apartmentInfo->price_comment}}</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
        <div class="col-sm-6">
            <div class="row">
                @foreach($apartmentInfo->photos as $photo)
                    <div class="col-sm-5 m-1">
                        <a href="{{asset('storage/apartments')."/".$photo->photo_path}}"  data-toggle="lightbox" data-gallery="example-gallery" class="img-fluid">
                            <img src="{{asset('storage/apartments')."/".$photo->photo_path}}" alt="Снимката липсва" class="img-fluid">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <table class="table table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Call Статус</th>
                    <th scope="col">Клиент</th>
                    <th scope="col">Коментар</th>
                    <th scope="col">Кой се обадил</th>
                    <th scope="col">Дата</th>
                </tr>
                </thead>
                <tbody>
                @foreach($apartmentInfo->calls as $key => $call)
                    <tr
                        @switch($call->call_state_id)
                        @case(1)
                        class="table-success"
                        @break
                        @case(2)
                        class="table-warning"
                        @case(3)
                        class="table-danger"
                        @break
                        @default
                        class="table-light"
                        @endswitch
                    >
                        <th scope="row">{{$key+1}}</th>
                        <td>{{$call->callStates->call_status}}</td>
                        <td><b>Client ID:</b>{{$call->clients->id}}<br>
                            {{$call->clients->first_name}} {{$call->clients->middle_name}} {{$call->clients->last_name}}
                        </td>
                        <td>{{$call->call_comment}}</td>
                        <td>{{$call->users->name}} ({{$call->users->email}})</td>
                        <td>{{$call->datetime}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkUOdZ5y7hMm0yrcCQoCvLwzdM6M8s5qk&callback=initMap">
    </script>

    <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>
@endsection
