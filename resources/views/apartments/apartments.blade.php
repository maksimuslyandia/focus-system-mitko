@extends('layouts.appSB')
@section('content')
    <link href="{{ asset('css/preloader.css') }}" rel="stylesheet" type="text/css">
    <script src="{{asset('lib/lightbox/ekko-lightbox.min.js')}}" defer></script>
    <link href="{{asset('lib/lightbox/ekko-lightbox.css')}}" rel="stylesheet" type="text/css">
    <script src="{{ asset('lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script>
        $(window).on('load', function () {
            var $preloader = $('#p_prldr'),
                $svg_anm = $preloader.find('.svg_anm');
            $svg_anm.fadeOut();
            $preloader.delay(500).fadeOut('slow');
        });
        $(function () {
            $("#success-alert").hide();
            $("#p_prldr").hide();

            $(".address").on("click", function () {
                // alert($(this).html());
                let id = this.id;
                let elem = this;
                $(elem).html($(elem).val());
                if ($(elem).val() === '') {
                    $(elem).html('Няма Адрес');
                }
                setTimeout(function () {
                    // $('#address' + id).hide();
                    $(elem).html('Покажи адрес')
                }, 4000);
            });
        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });

        function calls(idModal, clienId, idApartment, call_comment) {
            var call_status = $('#call_status' + idApartment).val();
            call_comment = $('#' + call_comment).val();
            $.ajax({
                beforeSend: function () {
                    $('#p_prldr').show().fadeIn;

                },
                complete: function () {
                    $('#p_prldr').hide();
                },
                method: "POST",
                url: "/updatecall",
                type: "POST",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "call_state_id": call_status,
                    "call_comment": call_comment,
                    "client_id": clienId,
                    "apartment_id": idApartment,
                },
                success: function (response) {
                    $("#max").append(response); //дебагер ответа dd();
                }
            })
                .done(function (msg) {
                    switch (call_status) {
                        case "1":
                            $("#row" + idApartment).attr('class', 'table-success');
                            break;
                        case "2":
                            $("#row" + idApartment).attr('class', 'table-warning');
                            break;
                        default:
                            $("#row" + idApartment).attr('class', 'table-danger');
                            break;
                    }
                    // $("#max").append(msg); //дебагер ответа dd();
                    $('#' + idModal).modal('hide');
                    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                        $("#success-alert").slideUp(500);
                    });
                });
        }

        function Preposition(idApartment, clientId) {
            $.ajax({
                beforeSend: function () {
                    $('#p_prldr').show();
                    var $preloader = $('#p_prldr'),
                        $svg_anm = $preloader.find('.svg_anm');
                    $svg_anm.fadeOut();
                    $preloader.delay(500).fadeOut('slow');
                },
                complete: function () {
                    $('#p_prldr').hide();
                },
                method: "POST",
                url: "/preposition",
                type: "POST",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "client_id": clientId,
                    "apartment_id": idApartment,
                },
                success: function (response) {
                    // $("#max").append(response); //дебагер ответа dd();
                }
            })
                .done(function (msg) {
                    $("#max").append(msg); //дебагер ответа dd();
                    switch (msg) {
                        case "deleted":
                            $("#preposition" + idApartment).attr('class', 'btn btn-sm btn-outline-primary');
                            break;
                        case "saved":
                            $("#preposition" + idApartment).attr('class', 'btn btn-sm btn-success');
                            break;
                        default:
                            $("#preposition" + idApartment).attr('class', 'btn btn-sm btn-outline-primary');
                            break;
                    }

                });
        }
    </script>


    @if(session('clientId')!=null)
        <div><h2> Търси подходящ за - ID: {{$client->id}} - {{$client->first_name}} {{$client->last_name}}</h2>
            <a href="/rent/admin/apartments/get" class="btn btn-sm btn-outline-success" role="button"
               aria-pressed="true">Приключи
                с избора на подходящи</a></div>
    @endif
    
    @if(session('ogled')!=null)
        <div><h2>Избери апартаменти за предварителен оглед</h2>
            <button data-toggle="modal" data-target="#ogledModal" href="/rent/admin/apartments/get" class="btn btn-sm btn-outline-success" role="button"
               aria-pressed="true">Приключи
                с избора</button></div>
    @endif
    
    <div class="modal fade " tabindex="-1" role="dialog" id="ogledModal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Избрани апартаменти за предварителене оглед</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table" id="ogledTable">
                            <thead>
                            <tr>
                                <th scope="col">ID на клиент</th>
                                <th scope="col">Имена на клиент</th>
                                <th scope="col">ID на имот</th>
                                <th scope="col">Адрес на имот</th>
                                <th scope="col">Дата</th>
                                <th scope="col">Час</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="save_ogled" onclick="ogled()" data-dismiss="modal">Запази</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Затвори</button>
                </div>
            </div>
        </div>
    </div>

    <div id='max'></div>

    <div id="searchPannel">
        @if(session('role') == "admin")
            <form
                action="{{URL::to('/rent/admin/apartments/search/')}}@if(session('clientId')!=null)/{{session('clientId')}} @endif"
                method="get" enctype="multipart/form-data">
                @else
                    <form
                        @if(session('ogled') != null)
                            action="{{URL::to('/rent/broker/apartments/ogled/')}}/{{session('ogled')}}" 
                        @else
                            action="{{URL::to('/rent/broker/apartments/search/')}}@if(session('clientId')!=null)/{{session('clientId')}}@endif"

                        @endif
                        method="get"
                        enctype="multipart/form-data">
                        @endif
                        @csrf
                        <div class="form-group form-check">
                            <div class="row">
                                <div class="col-sm-3 m-2">
                                    <label for="2">{{$apartmentParams[1]->param_name}}</label>
                                    <select name="region[]" multiple="multiple" size="10" class="form-control" id="2">
                                        <option value="">Без район</option>
                                        @foreach($apartmentParams[1]->subApartmentParams as $param)
                                            <option
                                                value="{{$param->sub_param_name}}">{{$param->sub_param_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{--                    Вид имот--}}
                                <div class="col-sm-3 m-2">
                                    <label for="6">{{$apartmentParams[5]->param_name}}</label>
                                    <select name="vid_imot[]" multiple="multiple" size="10" class="form-control" id="6">
                                        <option value="">Без район</option>
                                        @foreach($apartmentParams[5]->subApartmentParams as $param)
                                            <option
                                                value="{{$param->sub_param_name}}">{{$param->sub_param_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{--                Изложение  --}}
                                <div class="col-sm-3 m-2 border">
                                    <label for="3">{{$apartmentParams[2]->param_name}}</label>
                                    <table class="ml-4 mr-2">
                                        @foreach($apartmentParams[2]->subApartmentParams as $param)
                                            <tr>
                                                <td><input value="{{$param->sub_param_name}}" name="izlogenie[]" type="checkbox"
                                                           class="form-check-input" id="3"></td>
                                            </tr>
                                            <tr>
                                                <td><label class="form-check-label"
                                                           for="exampleCheck1">{{$param->sub_param_name}}</label></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="col-sm-3 m-2 border">
                                    <label for="1">{{$apartmentParams[0]->param_name}}</label>
                                    <table class="ml-4 mr-2">
                                        @foreach($apartmentParams[0]->subApartmentParams as $param)
                                            <tr>
                                                <td><input value="{{$param->sub_param_name}}" name="otoplenie[]" type="checkbox"
                                                           class="form-check-input" id="1"></td>
                                            </tr>
                                            <tr>
                                                <td><label class="form-check-label"
                                                           for="exampleCheck1">{{$param->sub_param_name}}</label></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                {{--                Вид строителство  --}}
                                <div class="col-sm-3 m-2 border">
                                    <label for="4">{{$apartmentParams[3]->param_name}}</label>
                                    <table class="ml-4 mr-2">
                                        @foreach($apartmentParams[3]->subApartmentParams as $param)
                                            <tr>
                                                <td><input value="{{$param->sub_param_name}}" name="vid_stroitelstvo[]" type="checkbox"
                                                           class="form-check-input" id="4"></td>
                                            </tr>
                                            <tr>

                                                <td><label class="form-check-label"
                                                           for="exampleCheck1">{{$param->sub_param_name}}</label></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                {{--                    Обзавеждане--}}
                                <div class="col-sm-3 m-2 border">
                                    <label for="5">{{$apartmentParams[4]->param_name}}</label>
                                    <table class="ml-4 mr-2">
                                        @foreach($apartmentParams[4]->subApartmentParams as $param)
                                            <tr>
                                                <td><input value="{{$param->sub_param_name}}" name="obzavegdane[]" type="checkbox"
                                                           class="form-check-input" id="5"></td>
                                            </tr>
                                            <tr>
                                                <td><label class="form-check-label"
                                                           for="exampleCheck1">{{$param->sub_param_name}}</label></td>
                                            </tr>

                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">ID апартамент</span>
                                        </div>
                                        <input type="text" name="idSearch" class="form-control" placeholder="11515"
                                               aria-label="Username" aria-describedby="basic-addon1"
                                               value="{{(isset($requestDataArr['idSearch']) && $requestDataArr['idSearch']!='')?$requestDataArr['idSearch']:''}}">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Прави думи</span>
                                        </div>
                                        <input disabled type="text" name="wordSearch" class="form-control"
                                               placeholder="Пиши нещо..." aria-label="Username"
                                               aria-describedby="basic-addon1"
                                               value="{{(isset($requestDataArr['wordSearch']) && $requestDataArr['wordSearch']!='')?$requestDataArr['wordSearch']:''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Цена от</span>
                                        </div>
                                        <input type="text" name="priceFrom" class="form-control" placeholder="100"
                                               aria-label="Username" aria-describedby="basic-addon1"
                                               value="{{(isset($requestDataArr['priceFrom']) && $requestDataArr['priceFrom']!='')?$requestDataArr['priceFrom']:0}}">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Цена до</span>
                                        </div>
                                        <input type="text" name="priceTo" class="form-control"
                                               placeholder="500" aria-label="Username"
                                               aria-describedby="basic-addon1"
                                               value="{{(isset($requestDataArr['priceTo']) && $requestDataArr['priceTo']!='')?$requestDataArr['priceTo']:50000000}}">
                                    </div>
                                </div>
                            </div>
                            @if(session('role') == "admin")
                                <div class="row">
                                    <div class="col-sm-3">
                                        <select id="inputState" name="status" class="form-control">
                                            <option value="">Всички имоти</option>
                                            @foreach($statuses as $status)
                                                @if(isset($requestDataArr['status']))
                                                    @if($status->id == $requestDataArr['status'])
                                                        <option selected
                                                                value="{{$status->id}}">{{$status->status_name}}</option>
                                                    @else
                                                        <option
                                                            value="{{$status->id}}">{{$status->status_name}}</option>
                                                    @endif
                                                @else
                                                    <option value="{{$status->id}}">{{$status->status_name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary ">Търси</button>
                        @if(session('ogled') != null)
                            <a class="btn btn-danger" href="{{URL::to('/rent/broker/apartments/ogled/')}}/{{session('ogled')}}" role="button">Изчисти
                                търсене</a>
                        @else
                            <a class="btn btn-danger" href="/rent/admin/apartments/get" role="button">Изчисти
                                търсене</a>
                        @endif
                    </form>
            </form>
    </div>

    <div class="row">
        <div class="col-sm-6">
            {{$apartmentsInfo->links()}}
        </div>
        <div class="col-sm-6">
            <h6> Общо имоти:{{$apartmentsInfo->total()}} </h6>
        </div>
    </div>

    <table class="table table-sm">
        <thead>
        <tr>
            <th scope="col">ID на ап-нт</th>
            <th scope="col">Инфо</th>
            <th scope="col">Вид имот</th>
            <th scope="col">Снимки</th>
            <th scope="col">Цена EUR</th>
            <th scope="col">Функции</th>

        </tr>
        </thead>
        <tbody>
        @foreach($apartmentsInfo as $id => $apartment)
            <tr id="row{{$apartment->id}}"
                @if(count($apartment->calls) != 0 && strtotime(date('Y-m-d 00:00:00'))<strtotime($apartment->calls[0]->datetime))
                @switch($apartment->calls[0]->call_state_id)
                @case(1)
                class="table-success"
                @break
                @case(2)
                class="table-warning"
                @break
                @case(3)
                class="table-danger"
                @break
                @endswitch
                @endif
            >
                <td scope="row">
                    {{$apartment->id}}<br>
                    @if($apartment->status_id ==1)
                        <span style=" color: green;"
                              data-toggle="tooltip" data-placement="top"
                              title="Активен имот">
                            <i class="fas fa-power-off fa-lg"></i>
                        </span>
                    @else
                        <span style=" color: red;"
                              data-toggle="tooltip" data-placement="top"
                              title="Неактивен имот">
                            <i class="fas fa-power-off fa-lg"></i>
                        </span>
                    @endif
                    @if($apartment->has_key === 1)
                        <span style=" color: orange;"
                              data-toggle="tooltip" data-placement="top"
                              @if(isset($apartment->key))
                                title="Ключ при  {{$apartment->key->username}} ({{$apartment->key->name}})">
                              @else
                                title="Ключът не е взет">
                              @endif
                                <i class="fas fa-key fa-lg"></i>
                        </span>
                    @endif
                    @if($apartment->has_exclusive_contract === 1)
                        <span style=" color: orange;"
                              data-toggle="tooltip" data-placement="top"
                              data-toggle="tooltip" data-placement="top"
                              @if(isset($apartment->exclusive_contract))
                                title="Ексклузивен договор взет от {{$apartment->exclusive_contract->username}} ({{$apartment->exclusive_contract->name}})">
                              @else
                                title="Екслклузивният договор не е взет от никой брокер">
                              @endif
                                <i class="fas fa fa-star fa-lg"></i>
                        </span>
                    @endif
                    @if($apartment->is_locked === 1)
                        <span style=" color: orange;"
                              data-toggle="tooltip" data-placement="top"
                              data-toggle="tooltip" data-placement="top"
                              @if(isset($apartment->lock_broker))
                                title="Апартаментът е заключен от {{$apartment->lock_broker->username}} ({{$apartment->lock_broker->name}})">
                              @else
                                title="Апартаментът е заключен">
                              @endif
                                <i class="fas fas fa fa-lock fa-lg"></i>
                        </span>
                    @endif
                    @if($apartment->top ==1)
                        <span style=" color: Tomato;" data-toggle="tooltip" data-placement="top"
                              title="Топ Оферта">
                                  <i class="fas fa-arrow-up fa-lg"></i>
                        </span>
                    @endif
                    @if($apartment->parking ==1)
                        <span style=" color: blue;" data-toggle="tooltip" data-placement="top"
                              title="Гараж">
                           <i class="fas fa-parking fa-lg"></i>
                        </span>
                    @endif
                    @if($apartment->air_conditioner ==1)
                        <span style=" color: deepskyblue;" data-toggle="tooltip" data-placement="top"
                              title="Климатик">
                            <i class="fas fa-fan fa-lg"></i>
                        </span>
                    @endif
                </td>   <!--Apartment ID-->
                <td scope="row">
                    @if(isset($client))
                        @foreach($apartment->clients as $client)
                            <b id="client{{$apartment->id}}" data-id="{{$client->id}}">{{$client->first_name}} {{$client->last_name}}</b>
                            <br> <!--Names-->
                        @endforeach
                    @endif
                    <b>Р-н:</b> {{($apartment->region==null)? '':$apartment->region}}<br>
                    <button type="button" id="{{$apartment->id}}" class="btn btn-sm btn-outline-primary address"
                            value="{{$apartment->address}}">Покажи адрес
                    </button>
                    <label id="address{{$apartment->id}}" style="display: none;">{{$apartment->address}}</label>
                </td>
                <td>
                    {{$apartment->vid_imot}}
                </td>
                <td scope="row"> 
                    @if(!$apartment->photos->isEmpty())
                        @foreach($apartment->photos as $key => $photo)
                            @if($key == 0)
                            <a href="{{asset('storage/apartments')."/".$photo->photo_path}}"  data-toggle="lightbox" data-gallery="example-gallery{{$id}}" class="img-fluid">
                                <img alt="Снимките липсват" width="150px"
                                    data-toggle="tooltip" data-placement="top" title="{{$apartment->photos->count()}}"
                                    src={{ asset('storage/apartments')."/". $photo->photo_path}}>
                            </a>
                            @else
                            <a href="{{asset('storage/apartments')."/".$photo->photo_path}}"  data-toggle="lightbox" data-gallery="example-gallery{{$id}}" class="img-fluid">
                                <img alt="Снимките липсват" 
                                        data-toggle="tooltip" data-placement="top" title="{{$apartment->photos->count()}}"
                                        src={{ asset('storage/apartments')."/". $photo->photo_path}} style="display: none;">
                            </a>
                            @endif
                        @endforeach
                        <br/>
                        Брой снимки {{$apartment->photos->count()}}
                    @endif
                </td>
                <!--Images-->
                <td scope="row">
                    {{$apartment->cena_eur}}
                </td>
                <td scope="row">
                    @if(session("ogled"))
                        <button class="btn btn-sm btn-outline-success ogled_select" data-id="{{$apartment->id}}" type="button" id="preposition{{$apartment->id}}">
                            Оглед
                        </button>
                    @endif
                    @if(session('clientId')!=null)
                        @if(!$apartment->clientsLike->isEmpty())
                            {{--                        @dd($apartment->clientsLike)--}}
                            @foreach($apartment->clientsLike as $clientLike)
                                @if(session('clientId')==$clientLike->id)
                                    <button type="button" id="preposition{{$apartment->id}}"
                                            class="btn btn-sm btn-sm btn-success"
                                            onclick="Preposition('{{$apartment->id}}',{{session('clientId')}})">Подходящ
                                    </button>@break; @continue;
                                @else
                                    <button type="button" id="preposition{{$apartment->id}}"
                                            class="btn btn-sm btn-outline-success"
                                            onclick="Preposition('{{$apartment->id}}',{{session('clientId')}})">Подходящ
                                    </button>
                                @endif
                            @endforeach
                        @else
                            <button type="button" id="preposition{{$apartment->id}}"
                                    class="btn btn-sm btn-outline-success"
                                    onclick="Preposition('{{$apartment->id}}',{{session('clientId')}})">Подходящ
                            </button>
                        @endif
                    @endif
                    <a href="/rent/{{session('role')}}/apartment/get/{{$apartment->id}}" role="button"
                       class="btn btn-primary btn-sm active" data-toggle="tooltip" data-placement="top"
                       title="Информация"><i class="fas fa-info"></i></a>
                    <a href="/rent/{{session('role')}}/apartments/update/{{$apartment->id}}" role="button"
                       class="btn btn-warning btn-sm active" data-toggle="tooltip" data-placement="top"
                       title="Редактирай"><i class="fas fa-edit"></i></a>
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" title="Обади ce"
                            data-target="#contact{{$apartment->id}}">
                        <i class="fas fa-mobile-alt"></i>
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="contact{{$apartment->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="contact{{$apartment->id}}" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="contact{{$apartment->id}}">
                                        @foreach($apartment->clients as $client)
                                            {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}<br>
                                        @endforeach
                                        <h5>ID апартамента: {{$apartment->id}}</h5>
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">Имена</th>
                                            <th scope="col">Имейли</th>
                                            <th scope="col">Телефони</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($apartment->clients) != 0)
                                            @foreach($apartment->clients as $client)
                                                <tr>
                                                    <td>
                                                        {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}
                                                    </td>
                                                    <td>
                                                        @if(count($client->emails) != 0)
                                                            @foreach($apartment->clients as $client)
                                                                {{$client->email}}<br>
                                                            @endforeach
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(($apartment->keys==null || $apartment->locked_broker == session("id")) || session("role") == "admin")
                                                            @if(count($client->phones) != 0)
                                                                @foreach($client->phones as $phone)
                                                                    {{ $phone->phone}}<br>
                                                                @endforeach
                                                            @endif
                                                        @else
                                                            {{--                                                            Ключ при {{$apartment->key->email}}--}}
                                                            {{--                                                            ({{$apartment->key->name}})--}}
                                                        @endif
                                                    </td>
                                                    @endforeach
                                                </tr>
                                                @endif
                                        </tbody>
                                    </table>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Оглед</label>
                                        <select class="form-control" id="call_status{{$apartment->id}}">
                                            <option selected value="">Избери</option>
                                            @foreach($callStates as $state)
                                                <option value="{{$state->id}}">{{$state->call_status}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Коментар към разговор</label>
                                        <textarea class="form-control" id="call_comment{{$apartment->id}}"
                                                  rows="3"></textarea>
                                    </div>
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">Статус на обаждане</th>
                                            <th scope="col">Коментар</th>
                                            <th scope="col">Дата</th>
                                            <th scope="col">Брокер</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($apartment->calls as $key => $call)
                                            @if($key<5)
                                                <tr
                                                    @switch($call->call_state_id)
                                                    @case(1)
                                                    class="table-success"
                                                    @break
                                                    @case(2)
                                                    class="table-warning"
                                                    @case(3)
                                                    class="table-danger"
                                                    @break
                                                    @default
                                                    class="table-light"
                                                    @endswitch
                                                >

                                                    <td>{{$call->callStates->call_status}}</td>
                                                    {{--                                                <td><b>Client ID:</b>{{$call->clients->id}}<br>--}}
                                                    {{--                                                    {{$call->clients->first_name}} {{$call->clients->middle_name}} {{$call->clients->last_name}}--}}
                                                    {{--                                                </td>--}}
                                                    <td>{{$call->call_comment}}</td>
                                                    <td>{{$call->datetime}}</td>
                                                    <td>{{$call->users->name}}<br>({{$call->users->email}})</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                    </button>

                                    <button type="button"
                                            onclick='calls("contact{{$apartment->id}}","{{$client->id}}","{{$apartment->id}}","call_comment{{$apartment->id}}",)'
                                            class="btn btn-primary">
                                        Save changes
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <script>
        let apartments = {};
        let dates = [];
        let times = [];
        function ogled() {
                $('.kek').get().forEach(function(element, index){
                    dates.push(element.value);
                })

                $('.eke').get().forEach(function(element, index){
                    times.push(element.value);
                });

            // if (ogledTime != '' && ogledDate != '' && new Date(ogledDate + ' ' + ogledTime) > Date.now()) {
                $.ajax({
                    method: "POST",
                    url: "/rent/broker/createOgled",
                    type: "POST",
                    data: {
                        "_token": $('meta[name="csrf-token"]').attr('content'),
                        "apartments": apartments,
                        "dates": dates,
                        "times": times
                    },
                    success: function (response) {
                        window.location.href = '/rent/broker/myclients/get';
                    }
                })
                    .done(function (msg) {
                        $("#max").append(msg); //дебагер ответа dd();
                    });
            // } else {
                // alert('Попълни данни и провери дали дата не е минала')
            // }
        }

        $(document).on('click', '.ogled_select', function(event) {
            let apartmentID = $(this).attr("data-id");
            let obj = {
                apartmentID: $(this).attr("data-id"),
                clientName: $("#client" + apartmentID).text(),
                clientID: $("#client" + apartmentID).attr("data-id"),
                address: $("#address" + apartmentID).text()
            };
            if($(this).hasClass('btn btn-sm btn-success')) {
                RemoveFromTable(apartmentID);
                delete apartments[apartmentID];
                $(this).attr('class', 'btn btn-sm btn-outline-success ogled_select');
            }else{
             
                $(this).attr('class', 'btn btn-sm btn-success ogled_select');
                apartments[apartmentID] = obj;
                AddToTable(obj);
                $('.kek').datepicker({
                    format: "dd/mm/yyyy"
                });
            }
        });


        function AddToTable(apart) {
            let markup =
                    "<tr " + "id=tableRow" + apart.apartmentID + ">" +
                        "<td>" +
                            apart.clientID +
                        "</td>" +

                        "<td>" +
                            apart.clientName +
                        "</td>" +

                        "<td>" +
                            apart.apartmentID +
                        "</td>" +

                        "<td>" +
                            apart.address +
                        "</td>" +
                        
                        "<td>" +
                            "<input  class='kek' maxlength='40' size='7'>" +
                        "</td>" +
                        
                        "<td>" +
                            "<input class='eke' maxlength='40' size='7'>" +
                        "</td>" +
                    "</tr>";

            $("#ogledTable").find('tbody')
                .append(markup);
        }

        function RemoveFromTable(id){
            $("#tableRow" + id).remove();
        }

        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>
    {{$apartmentsInfo->links()}}
@endsection
