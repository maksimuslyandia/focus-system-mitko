@extends('layouts.appSB')

@section('content')
    <div class="container-fluid">
        <h3>Брокери</h3>


        <table class="table table-sm">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Име</th>
                <th scope="col">Логин</th>
                <th scope="col">Вид профил</th>
                <th scope="col">Роля</th>
                <th scope="col">Функции</th>
            </tr>
            </thead>
            <tbody>
            @foreach($brokers as $broker)
                <tr>
                    <th scope="row">{{$broker->id}}</th>
                    <td>{{$broker->name}}</td>
                    
                    <td>{{$broker->username}}</td>
                  
                    <td>
                    @if($broker->sub_role === null)
                        Наеми
                    @elseif($broker->sub_role == "multi_admin")
                        Мулти Админ
                    @else
                        Продажби
                    @endif
                    </td>
                    <td>
                        {{$broker->roles[0]->role_name}}
                    </td>
                    <td>
                        <a href="/rent/admin/broker/{{$broker->id}}" role="button"
                        class="btn btn-warning btn-sm active" data-toggle="tooltip"
                        title="Редактирай"><i class="fas fa-edit"></i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
