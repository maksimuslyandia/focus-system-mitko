@extends('layouts.appSB')
@section('content')
    <script>
        // Shorthand for $( document ).ready()
        $(function () {
            console.log("ready!");

            $("#phoneBtn").click(function () {
                $("#addPhone").append("<input value=\"\"  name=\"phone[]\" type=\"text\"\n" +
                    "                       class=\" mt-2 form-control\" id=\"phone\" placeholder=\"Телефон\">");
            });
        });
    </script>
    <form action="{{URL::to('/rent/admin/broker/update/')}}" method="post" enctype="multipart/form-data">
        @csrf
        <h3 class="mt-5">За Брокер <b>{{$broker->name}}</b> с номер <b>{{$broker->username}}</b></h3>
        <hr>

        <input value="{{$broker->id}}" name="brokerID" hidden>
        <div class="row">
            <div class="col-sm-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Роля на брокера</span>
                </div>
                <select id="sub_role" name="sub_role" class="custom-select mr-sm-2">
                            <option value="sales" @if($broker->sub_role === null) selected @endif>Продажби</option>
                            <option @if($broker->sub_role === null) selected @endif>Наеми</option>
                            <option id="multi_admin" value="multi_admin" @if($broker->sub_role === "multi_admin") selected  @endif @if($broker->roles[0]->role_name !== "admin") style="display: none;"@endif>Мулти Админ</option>
                </select>
            </div>
            <div class="col-sm-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Вид профил</span>
                </div>
                <select id="role" name="role" class="custom-select mr-sm-2">
                            <option value="1" @if($broker->roles[0]->role_name === "admin") selected @endif>Админ</option>
                            <option value="2" @if($broker->roles[0]->role_name === "broker") selected @endif>Брокер</option>
                </select>
            </div>
            <div class="col-sm-3">
            <input value="{{$broker->username}}" name="user_name" hidden>
                <div class="input-group-prepend">
                    <span class="input-group-text">Потребителско име на брокера</span>
                </div>
                <div class="input-group-prepend">
                <input type="hidden" name="username" value="{{$broker->username}}"/>
                    <input name="username" class="form-control" value="{{$broker->username}}"/>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="input-group-prepend">
                    <span class="input-group-text">Име на брокер</span>
                </div>
                <div class="input-group-prepend">
                    <input name="name" class="form-control" value="{{$broker->name}}"/>
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Личен телефон</span>
                </div>
                    <input name="personal_phone" class="form-control" value="{{$broker->personal_phone}}" />
            </div>
            <div class="col-sm-6">
                <div class="input-group-prepend">
                    <span class="input-group-text">Служебен телефон</span>
                </div>
                <div class="input-group-prepend">
                    <input name="work_phone" class="form-control" value="{{$broker->work_phone}}" />
                </div>
            </div>        
        </div>

        <div class="row mt-4">
            <div class="col-sm-6">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Смяна на парола</span>
                    </div>
                        <input name="new_pass" id="new_pass" class="form-control" disabled/>
            </div>     
            <div class="col-sm-2 mt-4">
                <div class="custom-control custom-switch mt-3" style="">
                    <input type="hidden" name="change_pass" value="0"/>
                    <input type="checkbox" class="custom-control-input" id="change_pass" name="change_pass" value="1"/>
                    <label style=" onColor: warning;"  class="custom-control-label" for="change_pass"><i style=" color: orange;" class="fas fa fa-retweet"></i> Промяна?</label>
                </div>
            </div>
        </div>

        <button class="btn btn-primary mt-4" type="submit" >Запази</button>
    </form>
    <script>
        $('#role').change(function() {
            if ($(this).val() === '2') {
                if($("#sub_role").val() === 'multi_admin'){
                    $("#sub_role").val("sales")
                }
                $("#multi_admin").hide();
            }else {
                $("#multi_admin").show();
            }
        });

        $('#change_pass').click(function(){
            if($('#change_pass').is(":checked")){
                $("#new_pass").prop("disabled", false)
            }else{
                $("#new_pass").prop("disabled", true)
            }
        })
    </script>
@endsection
