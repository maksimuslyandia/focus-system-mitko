<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);


Route::get('/', function () {
    return view('welcome');
});


Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
//всички имат достъп до тези рутове

Route::middleware('auth')->group(function () {
    Route::middleware('setRole')->group(function () {
        Route::post('/checkphone', 'PhoneController@chekPhoneInfo'); //Phone checker
        Route::get('/file', 'ImageController@index');
        Route::post('/save', 'ImageController@save');
        Route::post('/updatecall', 'PhoneController@updateCall');
        Route::post('/preposition', 'ApartmentController@updateLikeApartment');
        Route::get('/preposition/stop', 'ApartmentController@stopLikeApartment');

        Route::group(['middleware' => ['roles:admin']], function () {/* Роуты, которые проходят проверку */
            /** администратори КЛИЕНТИ*/
            Route::get('/home', 'HomeController@index');
            Route::get('/rent/admin/clients/{status}/get', 'ClientController@getClients'); //all | active | passive
            Route::get('/rent/admin/clients/get/{clientId}', 'ClientController@getClient');
            Route::get('/rent/admin/myclients/get', 'ClientController@getUserClients');
            Route::get('/rent/admin/clients/create', 'ClientController@createClient');
            Route::post('/rent/admin/clients/create', 'ClientController@saveNewClient');
            Route::get('/rent/admin/clients/update/{clientId}', 'ClientController@getClient');
            Route::post('/rent/admin/clients/update', 'ClientController@saveClient');
            /** администратори Апартаменти */
            Route::get('/rent/admin/apartments/{status}/get', 'ApartmentController@getApartments'); //searchPannel Items: all | active | passive
            Route::get('/rent/admin/apartments/get/{apartmentId}', 'ApartmentController@getApartment');
            Route::get('/rent/admin/apartments/create', 'ApartmentController@createApartment'); //данни за фронт енд
            Route::post('/rent/admin/apartments/create', 'ApartmentController@saveNewApartment');
//            Route::get('/rent/admin/apartments/update/{apartmentId}', 'ApartmentController@getApartment');
//            Route::post('/rent/admin/apartments/update', 'ApartmentController@saveApartment');
            Route::get('/rent/admin/apartments/search', 'ApartmentController@searchApartmetns');

            /** администратори Заявки */
            Route::get('/rent/admin/requests/get', 'RequestController@requests');
            Route::get('/rent/admin/newrequest', 'RequestController@newRequest');
            Route::post('/rent/admin/setrequest', 'RequestController@setRequest');
            Route::post('/rent/admin/requests/changeStatus', 'RequestController@changeStatus');
        });
        Route::group(['middleware' => ['roles:broker']], function () {/* Роуты, которые проходят проверку */
            /** Брокери  */
            Route::get('/rent/broker/clients/{status}/get', 'ClientController@getClients'); //all | active | passive
            Route::get('/rent/broker/clients/get/{clientId}', 'ClientController@getClient');
            Route::get('/rent/broker/myclients/get', 'ClientController@getUserClients');
            Route::get('/rent/broker/myclients/apartaments/{clientId}/get', 'ApartmentController@getApartments');
            Route::get('/rent/admin/apartments/{status}/get/{clientId}', 'ApartmentController@getApartments');

            Route::get('/rent/broker/clients/create', 'ClientController@createClient');
            Route::post('/rent/broker/clients/set', 'ClientController@setClient');
            Route::get('/rent/broker/clients/update/{clientId}', 'ClientController@getClient');
            Route::post('/rent/broker/clients/update', 'ClientController@saveClient');
            Route::get('/rent/broker/clients/update/{clientId}', 'ClientController@getClient');
            Route::post('/rent/v/clients/update', 'ClientController@saveClient');

            /** Брокеры Апартаменти */
            Route::get('/rent/broker/apartments/{status}/get', 'ApartmentController@getApartments'); //searchPannel Items: all | active | passive
            Route::get('/rent/broker/apartments/get/{apartmentId}', 'ApartmentController@getApartment');
            Route::get('/rent/broker/apartments/create', 'ApartmentController@createApartment'); //данни за фронт енд
            Route::post('/rent/broker/apartments/set', 'ApartmentController@index');
//            Route::get('/rent/broker/apartments/update/{apartmentId}', 'ApartmentController@getApartment');
//            Route::post('/rent/broker/apartments/update', 'ApartmentController@saveApartment');
            Route::get('/rent/broker/apartments/search', 'ApartmentController@searchApartmetns');

            /** БрокерыЙ Заявки */
            Route::get('/rent/broker/requests/get', 'RequestController@requests');
            Route::post('/rent/broker/setrequest', 'RequestController@setRequest');
            Route::get('/rent/broker/newrequest', 'RequestController@newRequest');
        });

    });
});


